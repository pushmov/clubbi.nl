<?php
/* this file is used to upload banners */
class Banner_Management_Upload {
	
	//this function is used to upload banners
	public function image($fileObject, $uploadPath = '', $newFilename = '') {
		
		//check fileObject set
		if(!isset($fileObject) || empty($fileObject)) {
			return $response['error'] = 'fileObject not set';
		}
				
		//set allowed extensions
		$allowedExts = array('jpg', 'jpeg', 'png', 'gif');
		
		//explode filename
		if(isset($fileObject['name']) && !empty($fileObject['name'])) {
			$fileExplode = explode('.', $fileObject['name']);
		}
		
		//get file extension
		if(isset($fileExplode) && !empty($fileExplode)) {
			$extension = end($fileExplode);
		}
		
		//check file extension and filesize
		if (in_array($extension, $allowedExts)) {
			if ($fileObject['error'] > 0) {
				return $response['error'] = 'file showing error';
			} else {
				
				if(isset($uploadPath) && !empty($uploadPath) && isset($newFilename) && !empty($newFilename)) {
					
					$fullFilename = $_SERVER['DOCUMENT_ROOT'].'/'.$uploadPath.'/'.$newFilename;
					
					if(file_exists($fullFilename)) {
						unlink($fullFilename);
					}
					move_uploaded_file($fileObject['tmp_name'], $fullFilename);
					if(file_exists($fullFilename)) {
						return $response = array('data' => $uploadPath.'/'.$newFilename);
					} else {
						return $response = array('error' => 'file does not exists');
					}
				} else {
					return $response = array('error' => 'filepath or filename not set');
				}
			}
		} else {
			return $response = array('error' => 'filetype error');
		}
		
		return $response = array('error' => 'unknown error');
	}
}