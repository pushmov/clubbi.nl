<?php
/* this file is used to get the current url */
class Banner_Management_Url {
	
	//this gets the current url
	public function getCurrentUrl() {
		$url = 'http';
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
			$url .= 's';
		}		
		$url .= '://';
		
		if ($_SERVER['SERVER_PORT'] != '80') {
			$url .= $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
		} else {
			$url .= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		}
		return $url;
	}
	
	//this is used to breakdown url into array segments
	public function getUrlSegments() {
		$url = $this->getCurrentUrl();
		$url = str_replace('http://', '', $url);
		$url = str_replace('https://', '', $url);
		$exp = explode('/', $url);
		
		$data = array();
		if(isset($exp) && !empty($exp)) {
			foreach($exp as $segment) {
				if(isset($segment) && !empty($segment)) {
					$data[] = $segment;
				}	
			}
		}
		return $data;
	}
}
