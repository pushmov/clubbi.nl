<?php
/* this is a simple database class for simple queries across classes */
class Banner_Management_Database {
	
	public $host		= 'localhost';
	public $username	= 'online1q_clubbie';
	public $password	= '{95h#bVE$-gp';
	public $database	= 'online1q_clubbie';
	
	public function __construct() {		
		$con = mysql_connect($this->host, $this->username, $this->password);
		if (!$con) {
			die('Could not connect: ' . mysql_error());
		}	
		mysql_select_db($this->database, $con);
		unset($this->host);
		unset($this->username);
		unset($this->password);
		unset($this->database);
	}
	
	//this simply runs any mysql query used in all db classes
	public function runQuery($sql) {
		return mysql_query($sql);	
	}
	
	//this simply runs a queries and returns results in an array
	public function getResults($sql) {		
		$results = $this->runQuery($sql);
		$data 	 = array();

		if(isset($results) && !empty($results)) {
			while($row = mysql_fetch_assoc($results)) {
  				$data[] = $row;
  			}	
		}
		return $data;
	}
	
	//this is a simple dump function for easy debugging
	public function d($d) {
		print '<pre>';
		print_r($d);
		print '</pre>';
	}
}