<?php
/*this is used to handle placeholder crud operations */

class Banner_Management_Database_Placeholder extends Banner_Management_Database {
	
	public function __construct() {
		parent::__construct();
	}
	
	//this is to add a placeholder
	public function addItem($options) {
		
		//check title is set
		if(isset($options['title'])) {
			
			//get enabled based on key alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = '1';
			} else {
				$options['enabled'] = '0';
			}
			
			//insert new placeholder
			$sql = "
				insert into banner_manager_placeholder 
				(id, title, width, height, enabled, date_created, date_modified) values 
				(
					'',
					'".$options['title']."',
					'".$options['width']."',
					'".$options['height']."',
					'".$options['enabled']."',
					'".date('Y-m-d H:i:s')."',
					'".date('Y-m-d H:i:s')."'					
				);			
			";			
			return $this->runQuery($sql);
		}
	}
	
	//this is to edit an existing placeholder
	public function editItem($options) {
		
		//ensure title is set
		if(isset($options['title'])) {
			
			//get enabled based on key alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = 1;
			} else {
				$options['enabled'] = 0;
			}
			
			//update existing placeholder
			$sql = "
				update banner_manager_placeholder set  
				title 			= '".$options['title']."',
				width 			= '".$options['width']."',  
				height 			= '".$options['height']."',  
				enabled 		= '".$options['enabled']."',  
				date_modified 	= '".date('Y-m-d H:i:s')."' 
				where id = '".$options['id']."' limit 1
			";
			return $this->runQuery($sql);
		}
	}
	
	//delete placeholder by id
	public function deleteItem($options) {
		if(isset($options['id']) && !empty($options['id'])) {
			return $this->runQuery("delete from banner_manager_placeholder where id = '".$options['id']."' limit 1");
		}
	}
	
	//get placeholder by id
	public function getItem($id) {		
		$results = $this->getResults("select * from banner_manager_placeholder where id = '".$id."' limit 1");
		if(isset($results[0]) && !empty($results[0])) {
			return $results[0];
		}
	}
	
	//get all placeholders
	public function getItems() {
		return $this->getResults("select * from banner_manager_placeholder");
	}
}	