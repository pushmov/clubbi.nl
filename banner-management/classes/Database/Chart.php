<?php
/*this file is to collect analytics chart results */
class Banner_Management_Database_Chart extends Banner_Management_Database {
	
	//set default settings
	public $chartType = 'impressions'; //impressions, clicks
	public $bannerId  = 1;
	public $interval  = 'daily'; //daily, weekly, monthly
	public $dateFrom;
	public $dateTo;
	
	public function __construct() {
		parent::__construct();
		
		//set default date range
		$this->dateFrom  = date('Y-m-d', strtotime('-30 days')).' 00:00:00';
		$this->dateTo    = date('Y-m-d').' 23:59:59';
	}
	
	public function getData() {
		
		//get database table name
		$table = 'banner_manager_'.substr($this->chartType, 0, -1);
		
		//run daterange query - get result for each day in range
		$sql   = "
			select * from ".$table." where 
			banner_id = ".$this->bannerId." and 
			date_created >= '".$this->dateFrom."' and 
			date_created < '".$this->dateTo."'" 
		;
		$results  = $this->getResults($sql);
		
		//get interval value		
		$interval = 'get'.ucfirst($this->interval);
		
		//return results based on time interval setting		
		return $this->$interval($results);	
	}
		
	//get results formatted daily	
	public function getDaily($results = array()) {				
		$data 	 = array();		
		$incr	 = 0;		
		if(isset($results) && !empty($results)) {
			foreach($results as $result) {
				$data[$incr]['value']        = (int)$result['count_'.$this->chartType];
				$data[$incr]['date_created'] =  date('jS M',strtotime($result['date_created']));
				$incr++;
			}
		}
		return $data;
	}
	
	//get results formatted weekly
	public function getWeekly($results = array()) {				
		$data 	 = array();		
		$incr	 = 0;
		$day	 = 1;
		$total	 = 0;		
		if(isset($results) && !empty($results)) {
			foreach($results as $result) {
				$total = $total+(int)$result['count_'.$this->chartType];
				if($day == 1) {
					$data[$incr]['value'] 		 = $total;
					$data[$incr]['date_created'] = date('jS M',strtotime($result['date_created']));
					$total = 0;
				}
				if($day == 7) {
					$day = 0;
				}				
				$day++;
				$incr++;
			}
		}
		return $data;
	}
	
	//get results formatted monthly
	public function getMonthly($results = array()) {				
		$data 	 = array();		
		$incr	 = 0;
		$total	 = 0;		
		if(isset($results) && !empty($results)) {
			foreach($results as $result) {
				$total = $total+(int)$result['count_'.$this->chartType];
				$date  = str_replace(' 00:00:00', '', $result['date_created']);
				$exp   = explode('-', $date);
				$day   = $exp[2];
				
				if($day == '01') {
					$data[$incr]['value'] 		 = $total;
					$data[$incr]['date_created'] = date('jS M',strtotime($result['date_created']));
					$total = 0;
				}				
				$incr++;
			}
		}
		return $data;
	}
}