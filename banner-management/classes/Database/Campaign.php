<?php

/* this file is to handle campaign crud operations */
class Banner_Management_Database_Campaign extends Banner_Management_Database {
	
	public function __construct() {
		parent::__construct();
	}
	
	//add campaign
	public function addItem($options) {
		
		//check client id set
		if(isset($options['client_id'])) {
			
			//set enabled using alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = '1';
			} else {
				$options['enabled'] = '0';
			}
			
			//insert new client
			$sql = "
				insert into banner_manager_campaign 
				(id, client_id, title, description, enabled, date_created, date_modified) values 
				(
					'',
					'".$options['client_id']."',
					'".$options['title']."',
					'".$options['description']."',
					'".$options['enabled']."',
					'".date('Y-m-d H:i:s')."',
					'".date('Y-m-d H:i:s')."'					
				);			
			";			
			return $this->runQuery($sql);
		}
	}
	
	//edit client
	public function editItem($options) {
		
		//ensure client id set
		if(isset($options['client_id'])) {
			
			//set enabled based on alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = 1;
			} else {
				$options['enabled'] = 0;
			}
			
			//update campaign
			$sql = "
				update banner_manager_campaign set  
				client_id 		= '".$options['client_id']."',
				title 			= '".$options['title']."',  
				description 	= '".$options['description']."',  
				enabled 		= '".$options['enabled']."',  
				date_modified 	= '".date('Y-m-d H:i:s')."' 
				where id = '".$options['id']."' limit 1
			";
			return $this->runQuery($sql);
		}
	}
	
	//delete campaign by id
	public function deleteItem($options) {
		if(isset($options['id']) && !empty($options['id'])) {
			return $this->runQuery("delete from banner_manager_campaign where id = '".$options['id']."' limit 1");
		}
	}
	
	//get campaign by id
	public function getItem($id) {		
		$results = $this->getResults("
			select 
				cam.id,
				cam.client_id,
				cam.title,
				cam.description,
				cam.enabled,
				cam.date_created,
				cam.date_modified,
				cli.company_name
			from banner_manager_campaign as cam
			left join banner_manager_client as cli on 
			cam.client_id = cli.id 
			where cam.id = '".$id."' limit 1		
		");
		
		//return campaign
		if(isset($results[0]) && !empty($results[0])) {
			return $results[0];
		}
	}
	
	//get all campaigns
	public function getItems() {
			
		//return campaigns
		return $this->getResults("
			select 
				cam.id,
				cam.client_id,
				cam.title,
				cam.description,
				cam.enabled,
				cam.date_created,
				cam.date_modified,
				cli.company_name
			from banner_manager_campaign as cam
			left join banner_manager_client as cli on 
			cam.client_id = cli.id 
		");
	}
}	