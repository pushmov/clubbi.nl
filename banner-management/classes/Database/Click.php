<?php
/* this file is used to update click analytics */
class Banner_Management_Database_Click extends Banner_Management_Database {
	
	public function __construct() {
		parent::__construct();
	}
	
	//update clicks
	public function updateClicks($options = array()) {
			
		//check if entry exists		
		$results = $this->getResults("select * from banner_manager_click where banner_id = '".$options['banner_id']."' and date_created = '".date('Y-m-d')." 00:00:00' limit 1");
		if(isset($results[0]['id']) && !empty($results[0]['id'])) {
			
			//get click count
			$clicks = $results[0]['count_clicks']+1;
			
			//edit existing entry
			$this->editItem(array('banner_id' => $options['banner_id'], 'count_clicks' => $clicks));
		} else {
			
			//create a new entry
			$this->addItem(array('banner_id' => $options['banner_id'], 'count_clicks' => 1));
		}
	}
	
	//edit existing click count entry
	public function editItem($options = array()) {		
		$sql = "
			update banner_manager_click set			
			count_clicks 	= '".$options['count_clicks']."',  
			date_modified 	= '".date('Y-m-d H:i:s')."' 
			where 
				banner_id = '".$options['banner_id']."' and 
				date_created = '".date('Y-m-d')." 00:00:00' 
				limit 1
		";
		return $this->runQuery($sql);
	}
	
	//create new click count entry
	public function addItem($options = array()) {
		$sql = "
			insert into banner_manager_click 
			(id, banner_id, count_clicks, date_created, date_modified) values 
			(
				'',				 
				'".$options['banner_id']."', 
				'".$options['count_clicks']."', 
				'".date('Y-m-d')." 00:00:00', 
				'".date('Y-m-d H:i:s')."'
			)		
		";
		return $this->runQuery($sql); 	
	}
}