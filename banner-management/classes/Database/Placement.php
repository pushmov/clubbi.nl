<?php
/* this file is to handle all placement crud operations */

class Banner_Management_Database_Placement extends Banner_Management_Database {
	
	public function __construct() {
		parent::__construct();
	}
	
	//this is to add a placement
	public function addItem($options) {
		
		//ensure banner id set
		if(isset($options['banner_id'])) {
			
			//get enabled based on key alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = '1';
			} else {
				$options['enabled'] = '0';
			}
			
			//insert new banner placement
			$sql = "
				insert into banner_manager_placement 
				(id, banner_id, placeholder_id, date_start, date_end, enabled, date_created, date_modified) values 
				(
					'',
					'".$options['banner_id']."',
					'".$options['placeholder_id']."',
					'".$options['date_start']."',
					'".$options['date_end']."',
					'".$options['enabled']."',
					'".date('Y-m-d H:i:s')."',
					'".date('Y-m-d H:i:s')."'					
				);			
			";			
			return $this->runQuery($sql);
		}
	}
	
	//this is used to update an existing banner placement
	public function editItem($options) {
		
		//ensure banner id set
		if(isset($options['banner_id'])) {
			
			//get enabled base on key alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = 1;
			} else {
				$options['enabled'] = 0;
			}		
			
			//update existing banner placement
			$sql = "
				update banner_manager_placement set  
				banner_id 			= '".$options['banner_id']."',
				placeholder_id 		= '".$options['placeholder_id']."',				
				date_start 			= '".$options['date_start']."',
				date_end 			= '".$options['date_end']."',
				enabled 			= '".$options['enabled']."',
				date_modified 		= '".date('Y-m-d H:i:s')."' 
				where id = '".$options['id']."' limit 1
			";
			return $this->runQuery($sql);
		}
	}
	
	//this is to delete banner placement by id
	public function deleteItem($options) {
		if(isset($options['id']) && !empty($options['id'])) {
			return $this->runQuery("delete from banner_manager_placement where id = '".$options['id']."' limit 1");
		}
	}
	
	//this is to get banner placement by id
	public function getItem($id) {		
		$results = $this->getResults("select * from banner_manager_placement where id = '".$id."' limit 1");
		if(isset($results[0]) && !empty($results[0])) {
			return $results[0];
		}
	}
	
	//this is to get all banner placements
	public function getItems() {
		return $this->getResults("
			select 
				pl.id,
				pl.banner_id,
				pl.placeholder_id,				
				pl.date_start,
				pl.date_end,
				pl.enabled,
				pl.date_created,
				pl.date_modified,	
				ba.title as banner_title,	
				ph.title as placeholder_title,
				ph.width,
				ph.height,
				ph.enabled,
				ph.date_created,
				ph.date_modified
			from banner_manager_placement as pl 
			left join banner_manager_banner as ba on pl.banner_id = ba.id 
			left join banner_manager_placeholder as ph on pl.placeholder_id = ph.id
		");
	}
}
