<?php
/* this file is used to collect banners for placeholders and update analytics impressions */
class Banner_Management_Database_Impression extends Banner_Management_Database {
	
	public function __construct() {
		parent::__construct();
	}
	
	//get banners based on placeholder ids array
	public function loadBanners($placeholders = array()) {
		$data = array();
		if(isset($placeholders) && !empty($placeholders)) {
			foreach($placeholders as $placeholder) {
				
				//get banners matching placeholders
				$sql = "
					select
						ba.id as banner_id,								
						ba.src,
						ba.href,
						pl.id as placement_id						
					from banner_manager_placement as pl 
					left join banner_manager_banner as ba on pl.banner_id = ba.id 
					where 
						pl.placeholder_id = '".$placeholder."' and 
						pl.date_start <= '".date('Y-m-d H:i:s')."' and 
						pl.date_end >= '".date('Y-m-d H:i:s')."' 
						and pl.enabled = 1 order by rand()
				";
				$results = $this->getResults($sql);
				
				//check banner src is set
				if(isset($results[0]['src']) && !empty($results[0]['src'])) {
					
					//add banners to array					
					$data[$placeholder] = array('banner_id' => $results[0]['banner_id'], 'src' => $results[0]['src'], 'href' => $results[0]['href']);
					
					//update impressions
					$this->updateImpressions($results[0]);
				}				
			}
		}
		
		//return banners for display in placeholders
		return $data;	
	}
	
	//update banner impressions analytics
	public function updateImpressions($options = array()) {
			
		//check if entry exists
		$results = $this->getResults("select * from banner_manager_impression where banner_id = '".$options['banner_id']."' and date_created = '".date('Y-m-d')."' limit 1");
		if(isset($results[0]['id']) && !empty($results[0]['id'])) {
				
			//get existing impressions count
			$impressions = $results[0]['count_impressions']+1;
			
			//edit existing impressions count entry
			$this->editItem(array('banner_id' => $options['banner_id'], 'count_impressions' => $impressions));
		} else {
			
			//create new impressions count entry
			$this->addItem(array('banner_id' => $options['banner_id'], 'count_impressions' => 1));
		}
	}
	
	//edit existing impressions count entry
	public function editItem($options = array()) {
		$sql = "
			update banner_manager_impression set			
			count_impressions 	= '".$options['count_impressions']."',  
			date_modified 		= '".date('Y-m-d H:i:s')."' 
			where banner_id = '".$options['banner_id']."' and date_created = '".date('Y-m-d')." 00:00:00' limit 1
		";
		return $this->runQuery($sql);
	}
	
	//create new impressions count entry
	public function addItem($options = array()) {
		$sql = "
			insert into banner_manager_impression 
			(id, banner_id, count_impressions, date_created, date_modified) values 
			(
				'',				 
				'".$options['banner_id']."', 
				'".$options['count_impressions']."', 
				'".date('Y-m-d')." 00:00:00', 
				'".date('Y-m-d H:i:s')."'
			)		
		";
		return $this->runQuery($sql); 	
	}
}