<?php
/*this file is to handle client crud operations */
class Banner_Management_Database_Client extends Banner_Management_Database {
	
	public function __construct() {
		parent::__construct();
	}
	
	//add new client
	public function addItem($options) {
		
		//check first name is set
		if(isset($options['first_name'])) {
			
			//set enabled based on key alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = '1';
			} else {
				$options['enabled'] = '0';
			}
			
			//insert new client
			$sql = "
				insert into banner_manager_client 
				(id, first_name, last_name, company_name, company_url, company_email, company_telephone, enabled, date_created, date_modified) values 
				(
					'',
					'".$options['first_name']."',
					'".$options['last_name']."',
					'".$options['company_name']."',
					'".$options['company_url']."',
					'".$options['company_email']."',
					'".$options['company_telephone']."',
					'".$options['enabled']."',
					'".date('Y-m-d H:i:s')."',
					'".date('Y-m-d H:i:s')."'					
				);			
			";			
			return $this->runQuery($sql);
		}
	}
	
	//edit existing client
	public function editItem($options) {
		
		//check first name is set
		if(isset($options['first_name'])) {
			
			//set enabled based on key alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = 1;
			} else {
				$options['enabled'] = 0;
			}
			
			//update existing client
			$sql = "
				update banner_manager_client set  
				first_name 			= '".$options['first_name']."',
				last_name 			= '".$options['last_name']."',  
				company_name 		= '".$options['company_name']."',  
				company_url 		= '".$options['company_url']."',  
				company_email 		= '".$options['company_email']."',  
				company_telephone 	= '".$options['company_telephone']."',  
				enabled 			= '".$options['enabled']."',
				date_modified 		= '".date('Y-m-d H:i:s')."' 
				where id = '".$options['id']."' limit 1
			";
			return $this->runQuery($sql);
		}
	}
	
	//delete client by id
	public function deleteItem($options) {
		if(isset($options['id']) && !empty($options['id'])) {
			return $this->runQuery("delete from banner_manager_client where id = '".$options['id']."' limit 1");
		}
	}
	
	//get client by id
	public function getItem($id) {		
		$results = $this->getResults("select * from banner_manager_client where id = '".$id."' limit 1");
		if(isset($results[0]) && !empty($results[0])) {
			return $results[0];
		}
	}
	
	//get all clients
	public function getItems() {
		return $this->getResults("select * from banner_manager_client");
	}
}	