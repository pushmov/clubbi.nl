<?php
/* this file is to handle banner crud operations*/

//this file is to add file upload support
require_once BANNER_MANAGEMENT_PATH.'/classes/Upload.php';


class Banner_Management_Database_Banner extends Banner_Management_Database {
	
	public function __construct() {
		parent::__construct();
		
		//init file upload support
		$this->upload = new Banner_Management_Upload();
	}
	
	//this is to add a banner to the database
	public function addItem($options) {
		
		//ensure campaign id sent
		if(isset($options['campaign_id'])) {
			
			//set enabled from using alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = '1';
			} else {
				$options['enabled'] = '0';
			}
			
			//upload any files attached to form
			if(isset($_FILES['file']) && !empty($_FILES['file'])) {
				$filename = md5(time()).'.jpg';
				$this->upload->image($_FILES['file'], 'banner-management/storage', $filename);
			} else {
				$filename = '';
			}
			
			//insert new banner into database
			$sql = "
				insert into banner_manager_banner 
				(id, campaign_id, title, src, href, width, height, enabled, date_created, date_modified) values 
				(
					'',
					'".$options['campaign_id']."',
					'".$options['title']."',
					'".$filename."',
					'".$options['href']."',
					'".$options['width']."',
					'".$options['height']."',
					'".$options['enabled']."',
					'".date('Y-m-d H:i:s')."',
					'".date('Y-m-d H:i:s')."'					
				);			
			";			
			return $this->runQuery($sql);
		}
	}
	
	//this is the edit an existing banner
	public function editItem($options) {
		
		//ensure campaign id is set
		if(isset($options['campaign_id'])) {
			
			//set enabled based on field alias
			if($options['enabled'] == 'enabled') {
				$options['enabled'] = 1;
			} else {
				$options['enabled'] = 0;
			}
			
			//if file has been changes then upload new one
			if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
				$filename = md5(time()).'.jpg';
				$this->upload->image($_FILES['file'], 'banner-management/storage', $filename);
			}
			
			
			//check if file upload attached
			if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
						
				//update existing banner entry and image	
				$sql = "
					update banner_manager_banner set  
					campaign_id 	= '".$options['campaign_id']."',
					title 			= '".$options['title']."',  
					src 			= '".$filename."',
					href 			= '".$options['href']."',  
					width 			= '".$options['width']."',  
					height 			= '".$options['height']."',  
					enabled 		= '".$options['enabled']."',  
					date_modified 	= '".date('Y-m-d H:i:s')."' 
					where id = '".$options['id']."' limit 1
				";	
			} else {
				
				//update existing banner entry - no changes to image
				$sql = "
					update banner_manager_banner set  
					campaign_id 	= '".$options['campaign_id']."',
					title 			= '".$options['title']."',
					href 			= '".$options['href']."',					  
					width 			= '".$options['width']."',  
					height 			= '".$options['height']."',  
					enabled 		= '".$options['enabled']."',  
					date_modified 	= '".date('Y-m-d H:i:s')."' 
					where id = '".$options['id']."' limit 1
				";
			}			
	
			return $this->runQuery($sql);
		}
	}
	
	//this is used to delete a banner
	public function deleteItem($options) {
						
		//get banner id
		if(isset($options['id']) && !empty($options['id'])) {
			
			//delete banner by id
			return $this->runQuery("delete from banner_manager_banner where id = '".$options['id']."' limit 1");
		}
	}
	
	//get a banner by id
	public function getItem($id) {
			
		//get banner
		$results = $this->getResults("
			select 
				ban.id,
				ban.campaign_id,
				ban.title,
				ban.src,
				ban.href,
				ban.width,
				ban.height,
				ban.enabled,
				ban.date_created,
				ban.date_modified,
				cam.title as campaign_title,
				cli.company_name				
			from banner_manager_banner as ban 
			left join banner_manager_campaign as cam on ban.campaign_id = cam.id 
			left join banner_manager_client as cli on cam.client_id = cli.id		
			where ban.id = '".$id."' limit 1
		");
		
		//return result
		if(isset($results[0]) && !empty($results[0])) {
			return $results[0];
		}
	}
	
	//get list of banners
	public function getItems() {
		
		//return banners
		return $this->getResults("
			select 
				ban.id,
				ban.campaign_id,
				ban.title,
				ban.src,
				ban.href,
				ban.width,
				ban.height,
				ban.enabled,
				ban.date_created,
				ban.date_modified,
				cam.title as campaign_title,
				cli.company_name				
			from banner_manager_banner as ban 
			left join banner_manager_campaign as cam on ban.campaign_id = cam.id 
			left join banner_manager_client as cli on cam.client_id = cli.id 			
		");		
	}
}