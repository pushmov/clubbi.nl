<?php 
	/* this page is to display the banner admin panel */
	require_once 'config.php'; //this is the master config	
	require_once BANNER_MANAGEMENT_PATH.'/includes/header.php'; //this is the html header
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php'; //this is the master database class
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Banner.php'; //this is the banner crud class
	
	//get banners to display to page
	$item  = new Banner_Management_Database_Banner();
		
	if(isset($_POST['action']) && !empty($_POST['action'])) {
		switch($_POST['action']) {
			case 'add':
				
				//this will add a new banners 
				$item->addItem($_POST);
			break;
				
			case 'edit':
				//this will edit an existing banner	
				$item->editItem($_POST);
			break;
				
			case 'delete':
				//this will delete an existing banner
				$item->deleteItem($_POST);
			break;
		}
	}
	
	$results = $item->getItems(); 
?>
<section>
	<div class="page-header">
		<h1>3. Banners</h1>
	</div>
	<p class="lead">Here you can add new banners to campaigns the next step will be to add placeholders to position your banners on your website</p>
	<div class="row-fluid">
		<div class="span12">
			<p>	
				<!-- this button is used to add a banner -->
				<span id="btn-banners-add" class="btn btn-small btn-success btn-ajax-modal"><span class="icon-plus-sign"></span> Add New Banner</span>				
			</p>
			<table class="table table-bordered table-striped">				
				<thead>
					<tr>
						<th>ID</th>
						<th>Campaign</th>
						<th>Title</th>
						<th>Banner</th>
						<th>Link</th>
						<th>Width</th>
						<th>Height</th>
						<th>Enabled</th>
						<th>Date Created</th>
						<th>Date Modified</th>			
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php
						//this will display all banner listings
						if(isset($results) && !empty($results)) {
							foreach($results as $result) {
								?>
								<tr>
									<td><?php echo $result['id']; ?></td>
									<td><?php echo $result['company_name'].' - '.$result['campaign_title']; ?></td>
									<td><?php echo $result['title']; ?></td>
									<td><img src="storage/<?php echo $result['src']; ?>"></img></td>
									<td><?php echo $result['href']; ?></td>
									<td><?php echo $result['width']; ?></td>
									<td><?php echo $result['height']; ?></td>
									<td><?php echo $result['enabled']; ?></td>
									<td><?php echo $result['date_created']; ?></td>
									<td><?php echo $result['date_modified']; ?></td>
									
									<!-- these buttons are used to edit / delete banners -->										
									<td>
										<span id="btn-banners-edit-<?php echo $result['id']; ?>" class="btn btn-small btn-info btn-ajax-modal"><span class="icon-edit"></span> Edit</span>										
									</td>
									<td>
										<span id="btn-banners-delete-<?php echo $result['id']; ?>" class="btn btn-small btn-danger btn-ajax-modal"><span class="icon-trash"></span> Delete</span>
									</td>
								</tr>								
								<?php
							}
						}
					?>
				</tbody>
			</table>			
		</div>
	</div>
</section>

<!-- this will include the html footer -->            
<?php require_once BANNER_MANAGEMENT_PATH.'/includes/footer.php'; ?>