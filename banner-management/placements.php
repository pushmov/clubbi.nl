<?php 
	/* this file is used to display the placements crud admin panel */
	require_once 'config.php'; //this is used to include the master config file
	require_once BANNER_MANAGEMENT_PATH.'/includes/header.php'; //this is used to include the html header
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php'; //this is used to include the master database class
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Placement.php'; //this is used to include the placement crud class
	
	//this is used to get placement class
	$item  = new Banner_Management_Database_Placement();
		
	if(isset($_POST['action']) && !empty($_POST['action'])) {
		switch($_POST['action']) {
			case 'add':				
				//this is used to add placements
				$item->addItem($_POST);
			break;
				
			case 'edit':
				//this is used to edit placements
				$item->editItem($_POST);
			break;
				
			case 'delete':
				//this is used to delete placements
				$item->deleteItem($_POST);
			break;
		}
	}
	
	//this is used to get all placements
	$results = $item->getItems(); 
?>
<section>
	<div class="page-header">
		<h1>5. Placements</h1>
	</div>
	<p class="lead">
		Here you can assign any number of banners to each placeholder. You can also set display duration and impressions limits here.		
		Once you have created placements and added the placeholder embed code to your website, You can sit back and relax and view results on the analytics charts..
	</p>
	<div class="row-fluid">
		<div class="span12">
			<p>	
				<!-- this button is used to add a placement -->
				<span id="btn-placements-add" class="btn btn-small btn-success btn-ajax-modal"><span class="icon-plus-sign"></span> Add New Placement</span>				
			</p>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Banner</th>
						<th>Placeholder</th>						
						<th>Start</th>
						<th>End</th>
						<th>Enabled</th>
						<th>Date Created</th>
						<th>Date Modified</th>								
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>		
					<?php
						//this is where the placements are displayed */
						if(isset($results) && !empty($results)) {
							foreach($results as $result) {
								?>
								<tr>
									<td><?php echo $result['id']; ?></td>
									<td><?php echo $result['banner_title']; ?></td>
									<td><?php echo $result['placeholder_title']; ?></td>									
									<td><?php echo $result['date_start']; ?></td>
									<td><?php echo $result['date_end']; ?></td>
									<td><?php echo $result['enabled']; ?></td>
									<td><?php echo $result['date_created']; ?></td>
									<td><?php echo $result['date_modified']; ?></td>
									
									<!-- these buttons are used to edit / delete placements -->
									<td>
										<span id="btn-placements-edit-<?php echo $result['id']; ?>" class="btn btn-small btn-info btn-ajax-modal"><span class="icon-edit"></span> Edit</span>										
									</td>
									<td>
										<span id="btn-placements-delete-<?php echo $result['id']; ?>" class="btn btn-small btn-danger btn-ajax-modal"><span class="icon-trash"></span> Delete</span>
									</td>
								</tr>								
								<?php
							}
						}
					?>
				</tbody>
			</table>			
		</div>
	</div>
</section>

<!-- this is used to include the html footer -->           
<?php require_once BANNER_MANAGEMENT_PATH.'/includes/footer.php'; ?>