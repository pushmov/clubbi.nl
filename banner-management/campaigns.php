<?php 
	/* this page is to display the campaign admin panel */
	require_once 'config.php'; //this will include the master config
	require_once BANNER_MANAGEMENT_PATH.'/includes/header.php'; //this will include the html header
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php'; //this will include the master database class
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Campaign.php'; //this will include the campaign crud functions
	
	//get all campaigns
	$item  = new Banner_Management_Database_Campaign();
		
	if(isset($_POST['action']) && !empty($_POST['action'])) {
		switch($_POST['action']) {
			case 'add':				
				//this will add a campaign
				$item->addItem($_POST);
			break;
				
			case 'edit':
				//this will edit a campaign
				$item->editItem($_POST);
			break;
				
			case 'delete':
				//this will delete a campaign
				$item->deleteItem($_POST);
			break;
		}
	}
	
	$results = $item->getItems(); 
?>
<section>
	<div class="page-header">
		<h1>2. Campaigns</h1>
	</div>
	<p class="lead">
		Here you can add new campaigns for clients, For example 'summer campaign', 'winter campaign', or anything you like... 
		next you can add banners to your campaigns.
	</p>	
	<div class="row-fluid">
		<div class="span12">
			<p>	
				<!-- this button is used to add a campaign -->
				<span id="btn-campaigns-add" class="btn btn-small btn-success btn-ajax-modal"><span class="icon-plus-sign"></span> Add New Campaign</span>				
			</p>
			<table class="table table-bordered table-striped">				
				<thead>
					<tr>
						<th>ID</th>
						<th>Client</th>
						<th>Title</th>
						<th>Description</th>
						<th>Enabled</th>
						<th>Date Created</th>
						<th>Date Modified</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php
						//this is where campaigns are displayed
						if(isset($results) && !empty($results)) {
							foreach($results as $result) {
								?>
								<tr>
									<td><?php echo $result['id']; ?></td>
									<td><?php echo $result['company_name']; ?></td>
									<td><?php echo $result['title']; ?></td>
									<td><?php echo $result['description']; ?></td>
									<td><?php echo $result['enabled']; ?></td>
									<td><?php echo $result['date_created']; ?></td>
									<td><?php echo $result['date_modified']; ?></td>
									
									<!-- these buttons will allow for edit / delete campaigns -->										
									<td>
										<span id="btn-campaigns-edit-<?php echo $result['id']; ?>" class="btn btn-small btn-info btn-ajax-modal"><span class="icon-edit"></span> Edit</span>										
									</td>
									<td>
										<span id="btn-campaigns-delete-<?php echo $result['id']; ?>" class="btn btn-small btn-danger btn-ajax-modal"><span class="icon-trash"></span> Delete</span>
									</td>
								</tr>								
								<?php
							}
						}
					?>
				</tbody>
			</table>			
		</div>
	</div>
</section>

<!-- this will include the html footer -->
<?php require_once BANNER_MANAGEMENT_PATH.'/includes/footer.php'; ?>