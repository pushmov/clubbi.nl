<?php 
	/* this page is used to display the placeholders admin panel */
	require_once 'config.php'; //this is used to include the master config file
	require_once BANNER_MANAGEMENT_PATH.'/includes/header.php'; //this is used to include the html header
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php'; //this is used to include the master database class
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Placeholder.php'; //this is used to include the placeholder crud class
	
	//this is used to get the placeholder class
	$item  = new Banner_Management_Database_Placeholder();
		
	if(isset($_POST['action']) && !empty($_POST['action'])) {
		switch($_POST['action']) {
			case 'add':				
				//this is used to add placeholders
				$item->addItem($_POST);
			break;
				
			case 'edit':
				//this is used to edit placeholders
				$item->editItem($_POST);
			break;
				
			case 'delete':				
				//this is used to delete placeholders
				$item->deleteItem($_POST);
			break;
		}
	}
	
	//this is used to get all placeholders
	$results = $item->getItems(); 
?>
<section>
	<div class="page-header">
		<h1>4. Placeholders</h1>
	</div>
	<p class="lead">
		Here you can add new placeholders ready to embed into your website. once you add the embed code to your website the 
		final step will be to set banners to these placeholders.
	</p>
	<div class="row-fluid">
		<div class="span12">
			<p>	
				<!-- this button is used to add a placeholder -->
				<span id="btn-placeholders-add" class="btn btn-small btn-success btn-ajax-modal"><span class="icon-plus-sign"></span> Add New Placeholder</span>				
			</p>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Title</th>
						<th>Width</th>
						<th>Height</th>
						<th>Enabled</th>
						<th>Date Created</th>
						<th>Date Modified</th>
						<th>Embed Code</th>								
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php
						//this is where the placeholder results are displayed -->
						if(isset($results) && !empty($results)) {
							foreach($results as $result) {
								?>
								<tr>
									<td><?php echo $result['id']; ?></td>
									<td><?php echo $result['title']; ?></td>
									<td><?php echo $result['width']; ?></td>
									<td><?php echo $result['height']; ?></td>
									<td><?php echo $result['enabled']; ?></td>
									<td><?php echo $result['date_created']; ?></td>
									<td><?php echo $result['date_modified']; ?></td>
									
									<!-- these action buttons are used to get embed code + edit / delete placeholders -->
									<td>
										<span id="btn-placeholders-embed-<?php echo $result['id']; ?>" class="btn btn-small btn-success btn-ajax-modal"><span class="icon-cog"></span>Embed&nbsp;Code</span>										
									</td>
									<td>
										<span id="btn-placeholders-edit-<?php echo $result['id']; ?>" class="btn btn-small btn-info btn-ajax-modal"><span class="icon-edit"></span> Edit</span>										
									</td>
									<td>
										<span id="btn-placeholders-delete-<?php echo $result['id']; ?>" class="btn btn-small btn-danger btn-ajax-modal"><span class="icon-trash"></span> Delete</span>
									</td>
								</tr>								
								<?php
							}
						}
					?>
				</tbody>
			</table>			
		</div>
	</div>
</section>

<!-- this is to include the html footer -->            
<?php require_once BANNER_MANAGEMENT_PATH.'/includes/footer.php'; ?>