<?php
	/* this form is used to delete a placements*/
	
	//this is for select dropdown menus
	if(isset($_POST['id']) && !empty($_POST['id'])) {
		require_once '../../config.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Banner.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Placeholder.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Placement.php';
		
		$banner  	  = new Banner_Management_Database_Banner();
		$banners 	  = $banner->getItems();
		
		$placeholder  = new Banner_Management_Database_Placeholder();
		$placeholders = $placeholder->getItems();
		
		$item = new Banner_Management_Database_Placement();
		$result = $item->getItem($_POST['id']);
	}
	
	if(isset($result) && !empty($result)) {
?>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Delete Placement</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/placements.php">
		<input type="hidden" name="action" value="delete" />
		<input type="hidden" name="id" value="<?php echo $result['id']; ?>" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">Banner</label>
				<div class="controls">
					<select name="banner_id" readonly="readonly">
						<?php
							if(isset($banners) && !empty($banners)) {
								foreach($banners as $banner) {
									if($banner['id'] == $result['banner_id']) {
										?>		
										<option value="<?php echo $banner['id']; ?>" selected="selected">
											<?php echo $banner['title']; ?>
										</option>
										<?php									
									} else {
										?>
										<option value="<?php echo $banner['id']; ?>">
											<?php echo $banner['title']; ?>
										</option>
									<?php										
									}									
								}
							}
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Placeholder</label>
				<div class="controls">
					<select name="placeholder_id" readonly="readonly">
						<?php
							if(isset($placeholders) && !empty($placeholders)) {
								foreach($placeholders as $placeholder) {
									if($placeholder['id'] == $result['placeholder_id']) {
									?>
										<option value="<?php echo $placeholder['id']; ?>" selected="selected">
											<?php echo $placeholder['title']; ?>
										</option>
									<?php	
										} else {
									?>
										<option value="<?php echo $placeholder['id']; ?>">
											<?php echo $placeholder['title']; ?>
										</option>
									<?php	
									}
									
								}
							}
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Date Start</label>
				<div class="controls">
					<input type="text" name="date_start" placeholder="Date Start" value="<?php echo $result['date_start']; ?>" readonly="readonly" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Date End</label>
				<div class="controls">
					<input type="text" name="date_end" placeholder="Date End" value="<?php echo $result['date_end']; ?>" readonly="readonly" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select name="enabled" readonly="readonly">
						<?php
							if($result['enabled'] == '1') {
								?>
								<option value="enabled" selected="selected">Enabled</option>
								<option value="disabled">Disabled</option>		
								<?php
							} else {
								?>
								<option value="enabled">Enabled</option>
								<option value="disabled" selected="selected">Disabled</option>		
								<?php
							}
						?>									
					</select>
				</div>
			</div>				
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Delete Item</button>
		</div>
	</form>
</div>

<?php } ?>