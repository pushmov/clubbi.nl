<?php
	/* this form is used to add a placements*/
	
	//this is for select dropdown menus
	require_once '../../config.php';
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Banner.php';
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Placeholder.php';
	
	$banner  	  = new Banner_Management_Database_Banner();
	$banners 	  = $banner->getItems();
		
	$placeholder  = new Banner_Management_Database_Placeholder();
	$placeholders = $placeholder->getItems();
?>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add Placement</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/placements.php">
		<input type="hidden" name="action" value="add" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">Banner</label>
				<div class="controls">
					<select name="banner_id">
						<?php
							if(isset($banners) && !empty($banners)) {
								foreach($banners as $banner) {
									?>
									<option value="<?php echo $banner['id']; ?>">
										<?php echo $banner['title']; ?>
									</option>
									<?php
								}
							}
						?>
					</select>	
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Placeholder</label>
				<div class="controls">
					<select name="placeholder_id">
						<?php
							if(isset($placeholders) && !empty($placeholders)) {
								foreach($placeholders as $placeholder) {
									?>
									<option value="<?php echo $placeholder['id']; ?>">
										<?php echo $placeholder['title']; ?>
									</option>
									<?php
								}
							}
						?>
					</select>
				</div>
			</div>			
			<div class="control-group">
				<label class="control-label">Date Start</label>
				<div class="controls">
					<input type="text" name="date_start" placeholder="yyyy-mm-dd hh:mm:ss">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Date End</label>
				<div class="controls">
					<input type="text" name="date_end" placeholder="yyyy-mm-dd hh:mm:ss">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select name="enabled">
						<option value="enabled">Enabled</option>
						<option value="disabled">Disabled</option>					
					</select>
				</div>
			</div>			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>
