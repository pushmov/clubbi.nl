<?php
	/* this form is used to delete a placeholder*/
	
	//this is for select dropdown menus
	if(isset($_POST['id']) && !empty($_POST['id'])) {
		require_once '../../config.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Placeholder.php';
		
		$item = new Banner_Management_Database_Placeholder();
		$result = $item->getItem($_POST['id']);
	}
	
	if(isset($result) && !empty($result)) {
?>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Edit Placeholder</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/placeholders.php">
		<input type="hidden" name="action" value="delete" />
		<input type="hidden" name="id" value="<?php echo $result['id']; ?>" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">Title</label>
				<div class="controls">
					<input type="text" name="title" placeholder="Title" value="<?php echo $result['title']; ?>" readonly="readonly" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Width</label>
				<div class="controls">
					<input type="text" name="width" placeholder="Width" value="<?php echo $result['width']; ?>" readonly="readonly" /> px
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Height</label>
				<div class="controls">
					<input type="text" name="height" placeholder="Height" value="<?php echo $result['height']; ?>" readonly="readonly" /> px
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select name="enabled" readonly="readonly">
						<?php
							if($result['enabled'] == '1') {
								?>
								<option value="enabled" selected="selected">Enabled</option>
								<option value="disabled">Disabled</option>		
								<?php
							} else {
								?>
								<option value="enabled">Enabled</option>
								<option value="disabled" selected="selected">Disabled</option>		
								<?php
							}
						?>									
					</select>
				</div>
			</div>				
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Delete Item</button>
		</div>
	</form>
</div>

<?php } ?>