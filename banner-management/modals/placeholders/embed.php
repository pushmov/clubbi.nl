<?php
	/* this modal is to display banner placeholder embed code*/
	
	//this is collect placeholder details
	if(isset($_POST['id']) && !empty($_POST['id'])) {
		require_once '../../config.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Placeholder.php';
		
		$item = new Banner_Management_Database_Placeholder();
		$result = $item->getItem($_POST['id']);
	}
	
	if(isset($result) && !empty($result)) {
?>

<style type="text/css">
	.codebox {
		background-color: #F5F5F5;
    	border: 1px solid rgba(0, 0, 0, 0.15);
    	border-radius: 4px 4px 4px 4px;
    	display: block;
    	font-size: 13px;
    	line-height: 20px;
    	margin: 0 0 10px;
    	padding: 10px;
	}
</style>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Placeholder Embed Code</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/placeholders.php">
		<input type="hidden" name="action" value="add" />
		<div class="modal-body">
			<p>1. You need to add this line to your website before the end &lt;/body&gt;</p>		
			<div class="codebox">
				&lt;script type="text/javascript" src="/banner-management/scripts/js/banner-tracking.js"&gt;&lt;/script&gt;
			</div><br />
			
			<p>2. Place this code where you want the banner to appear</p>		
			<div class="codebox">
				&lt;div id="banner-placeholder-<?php echo $result['id']; ?>" class="banner-placeholder" style="width:<?php echo $result['width']; ?>px;height:<?php echo $result['height']; ?>px;border:1px solid grey;"&gt;&lt;/div&gt;
			</div>
			<br />
			<p>3. Create a placement and your banner and analytics will start working.</p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>

<?php } ?>
