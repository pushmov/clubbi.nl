<?php
	/* this form is used to add a placeholder */	
?>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add Placeholder</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/placeholders.php">
		<input type="hidden" name="action" value="add" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">Title</label>
				<div class="controls">
					<input type="text" name="title" placeholder="Title">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Width</label>
				<div class="controls">
					<input type="text" name="width" placeholder="Width"> px
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Height</label>
				<div class="controls">
					<input type="text" name="height" placeholder="Height"> px
				</div>
			</div>			
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select name="enabled">
						<option value="enabled">Enabled</option>
						<option value="disabled">Disabled</option>					
					</select>
				</div>
			</div>			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>
