<?php
	/* this form is used to edit a client */
	
	//this is for select dropdown menus
	if(isset($_POST['id']) && !empty($_POST['id'])) {
		require_once '../../config.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Client.php';
		
		$client = new Banner_Management_Database_Client();
		$result = $client->getItem($_POST['id']);
	}
	
	if(isset($result) && !empty($result)) {
?>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Edit Client</h3>
	</div>
		<form class="form-horizontal" method="post" action="/banner-management/clients.php">
		<input type="hidden" name="action" value="edit" />
		<input type="hidden" name="id" value="<?php echo $result['id']; ?>" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">First Name</label>
				<div class="controls">
					<input type="text" name="first_name" placeholder="First Name" value="<?php echo $result['first_name']; ?>">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Last Name</label>
				<div class="controls">
					<input type="text" name="last_name" placeholder="Last Name" value="<?php echo $result['last_name']; ?>">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Company</label>
				<div class="controls">
					<input type="text" name="company_name" placeholder="Company Name" value="<?php echo $result['company_name']; ?>">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Website</label>
				<div class="controls">
					<input type="text" name="company_url" placeholder="Website" value="<?php echo $result['company_url']; ?>">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Email</label>
				<div class="controls">
					<input type="text" name="company_email" placeholder="Email" value="<?php echo $result['company_email']; ?>">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Telephone</label>
				<div class="controls">
					<input type="text" name="company_telephone" placeholder="Telephone" value="<?php echo $result['company_telephone']; ?>">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select name="enabled">
						<?php
							if($result['enabled'] == '1') {
								?>
								<option value="enabled" selected="selected">Enabled</option>
								<option value="disabled">Disabled</option>		
								<?php
							} else {
								?>
								<option value="enabled">Enabled</option>
								<option value="disabled" selected="selected">Disabled</option>		
								<?php
							}
						?>									
					</select>
				</div>
			</div>			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>

<?php } ?>