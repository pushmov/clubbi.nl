<?php
	/* this form is used to delete a client*/
	
	//this is for select dropdown menus
	if(isset($_POST['id']) && !empty($_POST['id'])) {
		require_once '../../config.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
		require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Client.php';
		
		$client = new Banner_Management_Database_Client();
		$result = $client->getItem($_POST['id']);
	}
	
	if(isset($result) && !empty($result)) {	
?>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Delete Client</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/clients.php">
		<input type="hidden" name="action" value="delete" />
		<input type="hidden" name="id" value="<?php echo $result['id']; ?>" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">First Name</label>
				<div class="controls">
					<input type="text" name="first_name" placeholder="First Name" value="<?php echo $result['first_name']; ?>" readonly="readonly"">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Last Name</label>
				<div class="controls">
					<input type="text" name="last_name" placeholder="Last Name" value="<?php echo $result['last_name']; ?>" readonly="readonly"">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Company</label>
				<div class="controls">
					<input type="text" name="company" placeholder="Company Name" value="<?php echo $result['company_name']; ?>" readonly="readonly"">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Email</label>
				<div class="controls">
					<input type="text" name="email" placeholder="Email" value="<?php echo $result['company_email']; ?>" readonly="readonly"">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Telephone</label>
				<div class="controls">
					<input type="text" name="telephone" placeholder="Telephone" value="<?php echo $result['company_telephone']; ?>" readonly="readonly"">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select readonly="readonly">
						<?php
							if($result['enabled'] == '1') {
								?>
								<option value="enabled" selected="selected">Enabled</option>
								<option value="disabled">Disabled</option>		
								<?php
							} else {
								?>
								<option value="enabled">Enabled</option>
								<option value="disabled" selected="selected">Disabled</option>		
								<?php
							}
						?>									
					</select>
				</div>
			</div>			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Delete Item</button>
		</div>
	</form>
</div>

<?php } ?>