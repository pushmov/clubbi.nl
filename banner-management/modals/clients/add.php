<?php
	/* this form is used to add a new client */	
?>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add Client</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/clients.php">
		<input type="hidden" name="action" value="add" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">First Name</label>
				<div class="controls">
					<input type="text" name="first_name" placeholder="First Name">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Last Name</label>
				<div class="controls">
					<input type="text" name="last_name" placeholder="Last Name">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Company</label>
				<div class="controls">
					<input type="text" name="company_name" placeholder="Company Name">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Website</label>
				<div class="controls">
					<input type="text" name="company_url" placeholder="Website">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Email</label>
				<div class="controls">
					<input type="text" name="company_email" placeholder="Email">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Telephone</label>
				<div class="controls">
					<input type="text" name="company_telephone" placeholder="Telephone">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select name="enabled">
						<option value="enabled">Enabled</option>
						<option value="disabled">Disabled</option>					
					</select>
				</div>
			</div>			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>