<?php
	/* this form is used to add a banner*/
	
	//this is for select dropdown menus
	require_once '../../config.php';
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Campaign.php';
	
	$campaign  = new Banner_Management_Database_Campaign();
	$campaigns = $campaign->getItems();
?>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add Banner</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/banners.php" enctype="multipart/form-data">
		<input type="hidden" name="action" value="add" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">Campaign</label>
				<div class="controls">
					<select name="campaign_id" style="width:300px;">
						<?php
							if(isset($campaigns) && !empty($campaigns)) {
								foreach($campaigns as $campaign) {
									?>
									<option value="<?php echo $campaign['id']; ?>">
										<?php echo $campaign['company_name'].' - '.$campaign['title']; ?>
									</option>
									<?php
								}
							}
						?>
					</select>	
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Title</label>
				<div class="controls">
					<input type="text" name="title" placeholder="Title">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Banner</label>
				<div class="controls">
					<input type="file" name="file" id="file"><br>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Link</label>
				<div class="controls">
					<input type="text" name="href" placeholder="http://www.example.com">
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Width</label>
				<div class="controls">
					<input type="text" name="width" placeholder="width"> px
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Height</label>
				<div class="controls">
					<input type="text" name="height" placeholder="height"> px
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select name="enabled">
						<option value="enabled">Enabled</option>
						<option value="disabled">Disabled</option>					
					</select>
				</div>
			</div>			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>