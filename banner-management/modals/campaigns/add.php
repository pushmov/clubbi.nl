<?php
	/* this form is used to add a campaign */
	
	//this is for select dropdown menus
	require_once '../../config.php';
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Client.php';
	
	$client  = new Banner_Management_Database_Client();
	$clients = $client->getItems();
?>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add Campaign</h3>
	</div>
	<form class="form-horizontal" method="post" action="/banner-management/campaigns.php">
		<input type="hidden" name="action" value="add" />
		<div class="modal-body">		
			<div class="control-group">
				<label class="control-label">Client</label>
				<div class="controls">
					<select name="client_id">
						<?php
							if(isset($clients) && !empty($clients)) {
								foreach($clients as $client) {
									?>
									<option value="<?php echo $client['id']; ?>">
										<?php echo $client['company_name']; ?>
									</option>
									<?php
								}
							}
						?>
					</select>					
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Title</label>
				<div class="controls">
					<input type="text" name="title" placeholder="Title">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Description</label>
				<div class="controls">
					<input type="text" name="description" placeholder="Description">
				</div>
			</div>			
			<div class="control-group">
				<label class="control-label">Enable</label>
				<div class="controls">
					<select name="enabled">
						<option value="enabled">Enabled</option>
						<option value="disabled">Disabled</option>					
					</select>
				</div>
			</div>			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary">Save changes</button>
		</div>
	</form>
</div>