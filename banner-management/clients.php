<?php 
	/* this page is to display the clients admin panel */
	require_once 'config.php'; //this will include the master config
	require_once BANNER_MANAGEMENT_PATH.'/includes/header.php'; //this will include the header html
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php'; // this will include the master database class
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Client.php'; //this will include the client crud class
	
	//this will init the clients class
	$item  = new Banner_Management_Database_Client();
		
	if(isset($_POST['action']) && !empty($_POST['action'])) {
		switch($_POST['action']) {
			case 'add':
				//this will add a client
				$item->addItem($_POST);
			break;
				
			case 'edit':
				//this will edit a client
				$item->editItem($_POST);
			break;
				
			case 'delete':				
				//this will delete a client
				$item->deleteItem($_POST);
			break;
		}
	}
	
	//this will get the clients results
	$results = $item->getItems(); 
?>
<section>
	<div class="page-header">
		<h1>1. Clients</h1>
	</div>
	<p class="lead">This page is for adding new advertising clients to your system once you have added your clients you can start adding new banner campaigns</p>
	<div class="row-fluid">
		<div class="span12">
			<p>	
				<!-- this button is used to add a client -->
				<span id="btn-clients-add" class="btn btn-small btn-success btn-ajax-modal"><span class="icon-plus-sign"></span> Add New Client</span>				
			</p>
			<table class="table table-bordered table-striped">				
				<thead>
					<tr>						
						<th>ID</th>
						<th colspan="2">Client Name</th>
						<th>Company</th>
						<th>Url</th>
						<th>Email</th>
						<th>Telephone</th>
						<th>Enabled</th>
						<th>Created</th>
						<th>Modified</th>						
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php
						//this will display the clients listings
						if(isset($results) && !empty($results)) {
							foreach($results as $result) {
								?>
								<tr>
									<td><?php echo $result['id']; ?></td>
									<td><?php echo $result['first_name']; ?></td>
									<td><?php echo $result['last_name']; ?></td>
									<td><?php echo $result['company_name']; ?></td>
									<td><?php echo $result['company_url']; ?></td>
									<td><?php echo $result['company_email']; ?></td>
									<td><?php echo $result['company_telephone']; ?></td>
									<td><?php echo $result['enabled']; ?></td>
									<td><?php echo $result['date_created']; ?></td>
									<td><?php echo $result['date_modified']; ?></td>
									
									<!-- these buttons are used to edit / delete clients -->										
									<td>
										<span id="btn-clients-edit-<?php echo $result['id']; ?>" class="btn btn-small btn-info btn-ajax-modal"><span class="icon-edit"></span> Edit</span>										
									</td>
									<td>
										<span id="btn-clients-delete-<?php echo $result['id']; ?>" class="btn btn-small btn-danger btn-ajax-modal"><span class="icon-trash"></span> Delete</span>
									</td>
								</tr>								
								<?php
							}
						}
					?>
				</tbody>
			</table>			
		</div>
	</div>
</section>

<!-- this is used to include the html footer -->           
<?php require_once BANNER_MANAGEMENT_PATH.'/includes/footer.php'; ?>