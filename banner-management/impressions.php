<?php
	/* this page is used to display the impressions chart */
	require_once 'config.php'; //this is used to include master config file
	require_once BANNER_MANAGEMENT_PATH.'/includes/header.php'; //this is used to include the html header
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php'; //this is used to include the master database class
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Banner.php'; //this is used to include the banner crud class
	
	//this is used to get the banners
	$banner  = new Banner_Management_Database_Banner();
	$banners = $banner->getItems();
?>
<section>
	<div class="page-header">
		<h1>Impressions Chart</h1>
	</div>
	<p class="lead"></p>
	<div class="row-fluid">
		<div class="span12">
			<form id="chart_form" action="#" method="post">
				<input id="chart_type" name="chart_type" type="hidden" value="impressions">
				<div id="select_banner_holder">
					Select Banner
					
					<!-- this is used to select banners -->
					<select id="select_banner" name="banner">
						<?php 
							if(isset($banners) && !empty($banners)) {
								foreach($banners as $banner) {
									?>
									<option value="<?php echo $banner['id']; ?>">
										<?php echo $banner['title']; ?>
									</option>
									<?php
								}
							}
						?>
					</select>	
				</div>
				
				<!-- this is used to select the daterange -->
				<div id="daterange_holder">
					Universal Date Format (yyyy-mm-dd)<br />
					<select id="select_interval" name="interval">
						<option value="daily" selected="selected">Daily</option>
						<option value="weekly">Weekly</option>
						<option value="monthly">Monthly</option>
					</select>
					
					<input type="text" id="datepicker_from" placeholder="Date From" />
					<input type="text" id="datepicker_to" placeholder="Date To" />
				</div>
			</form>
			
			<div class="clear"></div>
			
			<br />
   			<div id="flotcontainer" style="width: 870px;height:350px; text-align: center; margin:0 auto;"></div>
				
		</div>
	</div>
</section>

<!-- this is used to include the html footer -->
<?php require_once BANNER_MANAGEMENT_PATH.'/includes/footer-charts.php'; ?>