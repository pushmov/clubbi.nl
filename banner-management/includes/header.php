<?php require_once BANNER_MANAGEMENT_PATH.'/classes/Url.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
  	<!-- this is the metadata area -->
    <meta charset="utf-8">
    <title>Clubbie Banner Management</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Manage Clients, Campaigns, Banners, Place Holders, Embed Codes, and Analytics Charts.">
    <meta name="author" content="Daniel Lee">

    <!-- these are the required stylesheets -->
    <link href="/banner-management/scripts/library/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/banner-management/scripts/library/bootstrap/css/docs.css" rel="stylesheet">
    <link href="/banner-management/scripts/library/jquery-ui/jquery-ui-1.10.0.custom/css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
    <link href="/banner-management/scripts/css/styles.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- this is the top menu -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">Bootstrap</a>
          <div class="nav-collapse collapse">
          	
          	<?php
          		//this is used to check which url is active
          		$url 		 		= new Banner_Management_Url();
				$urlSegments 		= $url->getUrlSegments();
				$activeHome			= !isset($urlSegments[2]) || empty($urlSegments[2]) ? 'active' : '';				
				$activeClients 		= isset($urlSegments[2]) && $urlSegments[2] == 'clients.php' ? 'active' : '';
				$activeCampaigns 	= isset($urlSegments[2]) && $urlSegments[2] == 'campaigns.php' ? 'active' : '';
				$activeBanners 		= isset($urlSegments[2]) && $urlSegments[2] == 'banners.php' ? 'active' : '';
				$activePlaceholders = isset($urlSegments[2]) && $urlSegments[2] == 'placeholders.php' ? 'active' : '';
				$activePlacements 	= isset($urlSegments[2]) && $urlSegments[2] == 'placements.php' ? 'active' : '';
				$activeImpressions 	= isset($urlSegments[2]) && $urlSegments[2] == 'impressions.php' ? 'active' : '';
				$activeClicks 		= isset($urlSegments[2]) && $urlSegments[2] == 'clicks.php' ? 'active' : '';
			?>          	
          	
          	<!-- these are the top menu links -->
            <ul class="nav">
              <li class="<?php echo $activeClients; ?>">              	
                <a href="/banner-management/clients.php">Clients</a>
              </li>
              <li class="<?php echo $activeCampaigns; ?>">
                <a href="/banner-management/campaigns.php">Campaigns</a>
              </li>
              <li class="<?php echo $activeBanners; ?>">
                <a href="/banner-management/banners.php">Banners</a>
              </li>
              <li class="<?php echo $activePlaceholders; ?>">
                <a href="/banner-management/placeholders.php">Placeholders</a>
              </li>
              <li class="<?php echo $activePlacements; ?>">
                <a href="/banner-management/placements.php">Placements</a>
              </li>
              <li class="<?php echo $activeImpressions; ?>">
                <a href="/banner-management/impressions.php">Impressions Chart</a>
              </li>
              <li class="<?php echo $activeClicks; ?>">
                <a href="/banner-management/clicks.php">Clicks Chart</a>
              </li>              
            </ul>
          </div>
        </div>       
      </div>      
    </div>

<!-- this is the main title area of the page -->
<header class="jumbotron subhead" id="overview">
  <div class="container">
    <h1>Clubbie Banner Management</h1>
    <p class="lead">Manage Clients, Campaigns, Banners, Place Holders, Embed Codes, and Analytics Charts.</p>
  </div>
</header>


  <div class="container">

   <!-- this is the left menu area of the template -->
    <div class="row">
      <div class="span3 bs-docs-sidebar">
        <ul class="nav nav-list bs-docs-sidenav">
          <li><a href="#" style="color:black;font-weight:bold;">Quick Menu</a></li>
          <li><a href="/banner-management/clients.php"><i class="icon-chevron-right"></i><span class="icon-user"></span> Add Client</a></li>
          <li><a href="/banner-management/campaigns.php"><i class="icon-chevron-right"></i><span class="icon-time"></span> Add Campaign</a></li>
          <li><a href="/banner-management/banners.php"><i class="icon-chevron-right"></i><span class="icon-picture"></span> Add Banner</a></li>
          <li><a href="/banner-management/placeholders.php"><i class="icon-chevron-right"></i><span class="icon-move"></span> Add Placeholder</a></li>
          <li><a href="/banner-management/placements.php"><i class="icon-chevron-right"></i><span class="icon-thumbs-up"></span> Add Placement</a></li>
          <li><a href="/banner-management/impressions.php"><i class="icon-chevron-right"></i><span class="icon-signal"></span> Impressions Chart</a></li>
          <li><a href="/banner-management/clicks.php"><i class="icon-chevron-right"></i><span class="icon-signal"></span> Clicks Chart</a></li>
        </ul>
        <br />
        <!--<p>&nbsp;&nbsp;Updating is disabled on this demo</p>-->
      </div>
      <div class="span9">