<!--
<div class="page-header">
	<h1>Demo Banners</h1>
</div>
<p class="lead">These banners below are live demo banners setup using this system.</p>
<p>
	<a href="/banner-management/impressions.php">
		<span class="btn btn-small btn-info">
			<span class="icon-signal"></span>
			View Analytics Charts
		</span>				
	</a>
</p>

<div id="banner-placeholder-1" class="banner-placeholder" style="width:250px;height:250px;border:1px solid grey;float:left;margin-right:20px;margin-bottom:20px;"></div>
<div id="banner-placeholder-2" class="banner-placeholder" style="width:250px;height:250px;border:1px solid grey;float:left;margin-right:20px;margin-bottom:20px;"></div>
<div id="banner-placeholder-3" class="banner-placeholder" style="width:250px;height:250px;border:1px solid grey;float:left;margin-right:20px;margin-bottom:20px;"></div>
-->
<!-- demo banner end -->


      </div>
    </div>
    
    
  </div>
<!-- this is the product license footer -->
<footer class="footer" style="text-align:center;height:50px;"></footer>

<!-- this is used to enabled all modals - its very important! -->
<div id="modal-holder"></div>
 

<!-- this are the required files for this script -->
<script type="text/javascript" src="/banner-management/scripts/library/general/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="/banner-management/scripts/library/jquery-ui/jquery-ui-1.10.0.custom/js/jquery-ui-1.10.0.custom.min.js"></script>
<script type="text/javascript" src="/banner-management/scripts/library/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/banner-management/scripts/js/modals.js"></script>
<script type="text/javascript" src="/banner-management/scripts/js/banner-tracking.js"></script>
<script type="text/javascript" src="/banner-management/scripts/js/general.js"></script>

  </body>
</html>