$(function() {
	
	//remove left quick links menu for smaller screens
	var width  = $(window).width();
  	var height = $(window).height();
	if(width < 1000) {
  		$('.bs-docs-sidebar').hide();	
  	} else {
  		$('.bs-docs-sidebar').show();
  	}
			
	$(window).resize(function() {
		var width  = $(window).width();
  		var height = $(window).height();
  		  		
  		if(width < 1000) {
  			$('.bs-docs-sidebar').hide();	
  		} else {
  			$('.bs-docs-sidebar').show();
  		}  		
	});	
	//end quick links menu fix
});
