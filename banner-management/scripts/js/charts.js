/* this file is used to collect / update analytics chart data */

//number formatter (123456789 to 123,456,789)
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$(function () {

    //create initial analytics chart with data
    updateChartForm();

    //this is used to setup datepicker for date from	
    $("#datepicker_from").datepicker({
        dateFormat: 'yy-mm-dd'
    });

    //this is used to setup datepicker for date to
    $("#datepicker_to").datepicker({
        dateFormat: 'yy-mm-dd'
    });

    //this will update analytics when a using changes banner	
    $('#chart_form #select_banner').change(function () {
        updateChartForm();
    });

    //this will update analytics when a using changes interval
    $('#chart_form #select_interval').change(function () {
        updateChartForm();
    });

    //this will update analytics when a using changes date from
    $('#chart_form #datepicker_from').change(function () {
        updateChartForm();
    });

    //this will update analytics when a using changes date to
    $('#chart_form #datepicker_to').change(function () {
        updateChartForm();
    });

    //update analytics results using form
    function updateChartForm() {
        chartForm = {};

        //get input fields
        chartForm.chartType = $('#chart_form #chart_type').val();
        chartForm.bannerId = $('#chart_form #select_banner').val();
        chartForm.Interval = $('#chart_form #select_interval').val();
        chartForm.dateFrom = $('#chart_form #datepicker_from').val();
        chartForm.dateTo = $('#chart_form #datepicker_to').val();
        
        $.ajax({
			url: '/banner-management/ajax/charts.php',
			data: {
				chart_type : chartForm.chartType,
				banner_id : chartForm.bannerId, 
				interval : chartForm.Interval,
				date_from : chartForm.dateFrom,
				date_to : chartForm.dateTo 
			},
			type: 'post',
			success: function(data) {
				
				//tooltip dates
				var toolTipDates = Array();				
				$.each(data, function(index, value) {
					toolTipDates.push(value.date_created);
				});
								
				//tooltip results
				var toolTipResults = Array();
				var i = 1;
				$.each(data, function(index, value) {
					toolTipResults.push(Array(i, value.value));
					i++;
				});	

		        var previousPoint = null;
		
		        $.fn.UseTooltip = function () {
		            $(this).bind("plothover", function (event, pos, item) {
		                if (item) {
		                    if (previousPoint != item.dataIndex) {
		                        previousPoint = item.dataIndex;
		
		                        $("#tooltip").remove();
		
		                        var x = item.datapoint[0];
		                        var y = item.datapoint[1];
		
		                        //console.log(x + "," + y)
		
		                        showTooltip(item.pageX, item.pageY, toolTipDates[x - 1] + "<br/>" + "<strong>" + numberWithCommas(y) + "</strong> ");
		                    }
		                } else {
		                    $("#tooltip").remove();
		                    previousPoint = null;
		                }
		            });
		        };
        
	        function showTooltip(x, y, contents) {
	            $('<div id="tooltip">' + contents + '</div>').css({
	                position: 'absolute',
	                display: 'none',
	                top: y + 5,
	                left: x + 20,
	                border: '2px solid #4572A7',
	                padding: '2px',
	                size: '10',
	                'border-radius': '6px 6px 6px 6px',
	                'background-color': '#fff',
	                opacity: 0.80
	            }).appendTo("body").fadeIn(200);
	        }
	
	        var plot;
	        
	        //xAxis Labels
			var xAxisLabels = Array();
			var i = 1;
			$.each(data, function(index, value) {
				xAxisLabels.push(Array(i, value.date_created));
				i++;
			});
	
	        $(function () {
	            plot = $.plot($("#flotcontainer"), [{
	                data: toolTipResults,
	                label: "",
	                lines: {
	                    show: true
	                },
	                points: {
	                    show: true
	                }
	            }], {
	                grid: {
	                    hoverable: true,
	                    clickable: false,
	                    //mouseActiveRadius: 30,
	                    backgroundColor: {
	                        colors: ["#D1D1D1", "#7A7A7A"]
	                    }
	                },
	                xaxis: {
	                    ticks: xAxisLabels,
	                    labelAngle: 90,
	                },
	                yaxis: {
	                	tickFormatter: function formatter(val, axis) {
           					return numberWithCommas(val);
        				}
	                }	                
	            });
	
	            $("#flotcontainer").UseTooltip();
	        });

			}
		});
    }

});