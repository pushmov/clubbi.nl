/* this file is used to trigger on screen modals */
$(document).ready(function() {
	
	//when a user clicks a modal button
	$('.btn-ajax-modal').click(function() {
		
		//get id		
		if(this.id) {
			
			//explode id
			exp = this.id.split('-');			
		}
		
		//set default mode (add/edit/delete)
		var mode = 'add';
		
		//check for id array
		if(exp) {
			
			//set action type
			if(exp[1]) {
				var type = exp[1];
			}			
			
			//check for itemId			
			if(exp[3]) {
				
				//update mode
				var mode = exp[2];
				
				//get itemId
				var itemId = exp[3];
			}		
		}
				
		if(type && mode && itemId) {
			
			//get any edit / delete item modals (required item id)
			$.ajax({
				url: '/banner-management/modals/'+type+'/'+mode+'.php',
				data: {id : itemId},
				type: 'post'
			}).done(function(html) {
				$('#modal-holder').html(html);
				$('#myModal').modal();
			});	
		} else if (type && mode) {
			
			//get any add item modals
			$.ajax({
				url: '/banner-management/modals/'+type+'/'+mode+'.php',
			}).done(function(html) {
				$('#modal-holder').html(html);
				$('#myModal').modal();
			});
		}	
		
	});
});