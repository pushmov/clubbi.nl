<?php
	/* this file is used to collect banners and track impressions */

	//this is to response json as output
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json');
	
	//this is the master config file
	require_once '../config.php';
	
	//this is the master database connection class
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
	
	//this is the class to update banner impressions
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Impression.php';	
	$banner = new Banner_Management_Database_Impression();
	
	//this collected a list of placeholder ids 1,2,3 etc...
	if(isset($_POST['placeholders']) && !empty($_POST['placeholders'])) {
		$placeholders 	= explode(',', $_POST['placeholders']);
		
		//this loads the banners and updates the impressions count
		$banners = $banner->loadBanners($placeholders);		
	}
	
	//if response is successful
	if(isset($banners) && !empty($banners)) {
		
		//return a list of banners and placeholder ids
		echo json_encode($banners);
	}
?>