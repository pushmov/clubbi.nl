<?php
	/*this page is used to get analytics charts data */
	
	//this is to send results in json format
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json');
	 
	//this is the master config file
	require_once '../config.php';
	
	//this is for database setup 
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
	
	//this is to collect analytics chart data
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Chart.php';	
	$chart = new Banner_Management_Database_Chart();
	
	if(isset($_POST['chart_type']) && !empty($_POST['chart_type'])) {
		$chart->chartType = $_POST['chart_type']; 
	}
	
	//this is to set banner id
	if(isset($_POST['banner_id']) && !empty($_POST['banner_id'])) {
		$chart->bannerId = $_POST['banner_id'];
	}
	
	//this is to set interval (daily, weekly. monthly)
	if(isset($_POST['interval']) && !empty($_POST['interval'])) {
		$chart->interval = $_POST['interval'];
	}
	
	//this is to set date range from (yyyy-mm-dd)
	if(isset($_POST['date_from']) && !empty($_POST['date_from'])) {
		$chart->dateFrom = $_POST['date_from'].' 00:00:00';
	}
	
	//this is to set date range to (yyyy-mm-dd)
	if(isset($_POST['date_to']) && !empty($_POST['date_to'])) {
		$chart->dateTo = $_POST['date_to'].' 23:59:59';
	}
	
	//this is to echo the final response
	echo json_encode($chart->getData($_POST));
?>