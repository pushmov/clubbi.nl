<?php
	/*this page is used to insert banner clicks */
	
	//this is to send results in json format
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json');
	
	//this includes the master config file
	require_once '../config.php';
	
	//this is the master database connection file
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php';
	
	//this class it to track banner clicks
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Click.php';		
	$click = new Banner_Management_Database_Click();
	
	//this is the set banner id
	if(isset($_POST['banner_id']) && !empty($_POST['banner_id'])) {
		
		//this is to update click value
		$click->updateClicks(array('banner_id' => $_POST['banner_id']));		
	}
	
	//return true to response to ajax as complete
	echo true;
?>