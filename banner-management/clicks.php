<?php
	/* this page is used to display an analytics graph for banner clicks */ 
	require_once 'config.php'; //this will include the main config
	require_once BANNER_MANAGEMENT_PATH.'/includes/header.php'; //this will include the html header
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database.php'; //this will include the main database class
	require_once BANNER_MANAGEMENT_PATH.'/classes/Database/Banner.php'; //this will include the banner crud class
	
	//this is used to get all banners
	$banner  = new Banner_Management_Database_Banner();
	$banners = $banner->getItems();
?>
<section>
	<div class="page-header">
		<h1>Clicks Chart</h1>
	</div>
	<p class="lead"></p>
	<div class="row-fluid">
		<div class="span12">
			<form id="chart_form" action="#" method="post">
				
				<!-- this will set the chart type -->
				<input id="chart_type" name="chart_type" type="hidden" value="clicks">
				
				
				<div id="select_banner_holder">
					Select Banner
					
					<!-- this will list the banner selections -->
					<select id="select_banner" name="banner">
						<?php 
							if(isset($banners) && !empty($banners)) {
								foreach($banners as $banner) {
									?>
									<option value="<?php echo $banner['id']; ?>">
										<?php echo $banner['title']; ?>
									</option>
									<?php
								}
							}
						?>
					</select>	
				</div>
				
				<!-- this will list the interval settings -->
				<div id="daterange_holder">
					Universal Date Format (yyyy-mm-dd)<br />
					<select id="select_interval" name="interval" style="width:80px;">
						<option value="daily" selected="selected">Daily</option>
						<option value="weekly">Weekly</option>
						<option value="monthly">Monthly</option>
					</select>
					
					<!-- this will display the date range fields -->
					<input type="text" id="datepicker_from" placeholder="Date From" />
					<input type="text" id="datepicker_to" placeholder="Date To" />
				</div>
			</form>
			
			<div class="clear"></div>
			
			<br />
   			<div id="flotcontainer" style="width: 870px;height:350px; text-align: center; margin:0 auto;"></div>	
		</div>
	</div>
</section>

<!-- this will display the html footer -->
<?php require_once BANNER_MANAGEMENT_PATH.'/includes/footer-charts.php'; ?>