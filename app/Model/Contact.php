<?php
/**
 * @copyright Copyright 2010 Evento
 */

class Contact extends AppModel {

	/**
	 * do not use any database table for this model
	 */

	var $useTable = false;

	/**
	 * validation
	 */

	var $validate = array(
		'email' => array(
			'rule' => 'email',
			'required' => true,
			'message' => 'Please enter a valid email address.'),
		'message' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Please write a message'),
		'recaptcha' => array(
			'notEmpty'  => array(
				'rule' => 'notEmpty',
				'message' => 'Incorrect captcha',
				'required' => true
			))
	);
}
?>