<?php
/**
 * @copyright Copyright 2008 Evento
 */

class Participant extends AppModel {

	var $name = 'Participant';
	var $useTable = 'participants';
	
	var $belongsTo = array(
		'User'=>array('className'=>'User',
			'fields' => array('User.username', 'User.slug', 'User.photo', 'User.active', 'User.first_name', 'User.last_name', 'User.birthdate', 'User.gender', 'User.email'))
	);
	
}