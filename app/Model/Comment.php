<?php
/**
 * @copyright Copyright 2008
 */

class Comment extends AppModel {

	var $name = 'Comment';
	var $useTable = 'comments';
	var $actsAs = array('Containable');
	var $use = array('Comment');
	var $recursive = -1;

	/*
	 * validation
	 */

	var $validate = array(
		'comment' => array(
			'rule' => array('minLength', 2),
			'message' => 'Comment must be at least 2 characters long',
		),
		'recaptcha' => array(
			'notEmpty'	=> array(
				'rule' => 'notEmpty',
				'on' => 'create',
				'message' => 'Incorrect captcha',
				'required' => true
			),
		));

	/*
	 * model associations
	 */

	var $belongsTo = array(
		'Event'=>array('className'=>'Event'),
		'User'=>array('className'=>'User',
			'fields' => array('User.username', 'User.slug', 'User.photo', 'User.active'))
	);

	/**
	 * Save user comment
	 *
	 * @param array $data
	 * @param int $eventId
	 * @param int $userId
	 * @return boolean
	 */

	public function saveComment($data, $eventId, $userId) {
		$data['Comment']['event_id'] = $eventId;
		$data['Comment']['user_id'] =  $userId;
		return $this->save($data);
	}


	/**
	 * bulk delete comments
	 *
	 * @param array $ids
	 * @return boolean
	 */

	public function bulkDelete($ids) {
		return $this->deleteAll(array('Comment.id'=>$ids));
	}
}
?>