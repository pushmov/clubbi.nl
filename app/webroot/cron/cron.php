<?php
//DATABASE CONNECTIE
$mysql_server = "localhost";
$mysql_database = "online1q_clubbie";
$mysql_login = "online1q_clubbie";
$mysql_wachtwoord = "{95h#bVE$-gp";
$mysql_conn = mysql_connect ("$mysql_server", "$mysql_login", "$mysql_wachtwoord");
mysql_select_db("$mysql_database");
//EINDE DATABASE CONNECTIE

//BEGIN FUNCTIES
setlocale(LC_ALL, 'en_US.UTF8');
function toAscii($str, $replace=array(), $delimiter='-') {
  if( !empty($replace) ) {
    $str = str_replace((array)$replace, ' ', $str);
  }

  $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
  $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
  $clean = strtolower(trim($clean, '-'));
  $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

  return $clean;
}

function xml2ary(&$string) {
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parse_into_struct($parser, $string, $vals, $index);
    xml_parser_free($parser);

    $mnary=array();
    $ary=&$mnary;
    foreach ($vals as $r) {
        $t=$r['tag'];
        if ($r['type']=='open') {
            if (isset($ary[$t])) {
                if (isset($ary[$t][0])) $ary[$t][]=array(); else $ary[$t]=array($ary[$t], array());
                $cv=&$ary[$t][count($ary[$t])-1];
            } else $cv=&$ary[$t];
            if (isset($r['attributes'])) {foreach ($r['attributes'] as $k=>$v) $cv['_a'][$k]=$v;}
            $cv['_c']=array();
            $cv['_c']['_p']=&$ary;
            $ary=&$cv['_c'];

        } elseif ($r['type']=='complete') {
            if (isset($ary[$t])) { // same as open
                if (isset($ary[$t][0])) $ary[$t][]=array(); else $ary[$t]=array($ary[$t], array());
                $cv=&$ary[$t][count($ary[$t])-1];
            } else $cv=&$ary[$t];
            if (isset($r['attributes'])) {foreach ($r['attributes'] as $k=>$v) $cv['_a'][$k]=$v;}
            $cv['_v']=(isset($r['value']) ? $r['value'] : '');

        } elseif ($r['type']=='close') {
            $ary=&$ary['_p'];
        }
    }    
    _del_p($mnary);
    return $mnary;
}
function _del_p(&$ary) {
    foreach ($ary as $k=>$v) {
        if ($k==='_p') unset($ary[$k]);
        elseif (is_array($ary[$k])) _del_p($ary[$k]);
    }
}
//EINDE FUNCTIES

date_default_timezone_set ('CET');



  $XMLURL = "http://feeds.clubjudge.com/events/upcoming.xml?country_code=nl";
  $xmldata = file_get_contents($XMLURL);
  $arr= xml2ary($xmldata);
  
   //print"<pre>";print_r($arr);exit;

  if(isset($arr['events']['_c']['event']) && is_array($arr['events']['_c']['event'])){
    
    
    foreach($arr['events']['_c']['event'] as $result){
    
      $event_title = mysql_real_escape_string($result['_c']['title']['_v']);
      $event_description = mysql_real_escape_string($result['_c']['description']['_v']);
      $event_guid = mysql_real_escape_string($result['_c']['guid']['_v']);
      $event_start = mysql_real_escape_string($result['_c']['start']['_v']);
      $event_end = mysql_real_escape_string($result['_c']['end']['_v']);

      $event_location = mysql_real_escape_string($result['_c']['location']['_c']['country']['_v']);
      $event_venue_name = mysql_real_escape_string($result['_c']['location']['_c']['venue_name']['_v']);
      $event_city = mysql_real_escape_string($result['_c']['location']['_c']['city']['_v']);
      $event_street = mysql_real_escape_string($result['_c']['location']['_c']['street']['_v']);
      $event_zip = preg_replace("/[^A-Za-z0-9]/",'',strtoupper($result['_c']['location']['_c']['zip']['_v']));
      $event_link = mysql_real_escape_string($result['_c']['location']['_c']['link']['_v']);
      $event_street_nummer = preg_replace("/[^\d]/", "", $event_street);

      $event_music_genres = mysql_real_escape_string($result['_c']['music_genres']['_v']);
      $event_line_up = mysql_real_escape_string($result['_c']['line_up']['_v']);
     $event_entrance_fees = mysql_real_escape_string($result['_c']['tickets']['_c']['ticket']['_c']['price']['_v']);
     $event_ticket = mysql_real_escape_string($result['_c']['tickets']['_c']['ticket']['_c']['link']['_v']);
     $event_flyers = mysql_real_escape_string($result['_c']['flyers']['_c']['flyer']['_c']['versions']['_c']['version'][0]['_c']['url']['_v']);
      
     $newTZ = new DateTimeZone("Europe/Amsterdam");
     $event_start = new DateTime($event_start);
     $event_start->setTimezone( $newTZ );
     $event_start = $event_start->format('Y-m-d H:i:s');
     $event_end = new DateTime($event_end);
     $event_end->setTimezone( $newTZ );
     $event_end = $event_end->format('Y-m-d H:i:s');
     

      
      if(mysql_real_escape_string($result['_c']['location']['_c']['country']['_v']) == "Netherlands"){

        ////STAD TOEVOEGEN
        $controleren_stad_betaat  = mysql_query ("SELECT name,id FROM cities WHERE name = '$event_city'");
        if (mysql_num_rows($controleren_stad_betaat) == 0) {

          $sql_stad = "
            INSERT INTO cities
                  SET 
            country_id = 155, 
            name = '".$event_city."', 
            slug = '".toAscii($event_city)."' 
            ";
          mysql_query($sql_stad) or die(mysql_error()); $stad_id = mysql_insert_id();

          
        }else{
          $row = mysql_fetch_array ($controleren_stad_betaat);
          $stad_id = $row['id']; 

        }
        /////EINDE STAD


        $controleren_club_betaat  = mysql_query ("SELECT name,id,postcode FROM venues WHERE name = '$event_venue_name'");
        if (mysql_num_rows($controleren_club_betaat) == 0) {

          $postcode_check = mysql_fetch_array (mysql_query ("SELECT pcode,xco,yco FROM locatie WHERE pcode = '$event_zip'"));
          $xco = $postcode_check['xco']; 
          $yco = $postcode_check['yco']; 
          $sql_event = "
            INSERT INTO venues
                  SET 
            city_id = '".$stad_id."', 
            name = '".utf8_decode($event_venue_name)."', 
            slug = '".toAscii($event_venue_name)."',
            lat = '".$yco."',
            lng = '".$xco."',
            created =  NOW(),
            modified = NOW(),
            postcode = '".$event_zip.$event_street_nummer."',
            address = '".utf8_decode ($event_street)."'
            ";
          mysql_query($sql_event) or die(mysql_error()); $event_id = mysql_insert_id();

        }else{
          $row = mysql_fetch_array ($controleren_club_betaat);
          $event_id = $row['id']; 

        }

echo "Event: " . $event_title;

$controleren_feest_betaat  = mysql_query ("SELECT guid FROM events WHERE guid = '$event_guid'");
if (mysql_num_rows($controleren_feest_betaat) == 0) {

  $naamfeest = toAscii($event_title);
  $controleren_naam_bestaat  = mysql_query ("SELECT slug FROM events WHERE slug = '$naamfeest'");
  if (mysql_num_rows($controleren_naam_bestaat) != 0) {
    $naamfeest = toAscii($event_title." ".rand(1, 1000));
  }


  $sql_party = "
    INSERT INTO events
          SET 

    user_id = 2,
    venue_id = '".$event_id."',
    name = '".utf8_decode ($event_title)."',
    logo = '".utf8_decode ($event_flyers)."',
    start_date = '". $event_start."',
    end_date = '". $event_end."',
    notes = '".utf8_decode ($event_description)."',
    web = '".utf8_decode ($event_link)."',
    lineup = '".utf8_decode ($event_line_up)."',
    presale = '".utf8_decode ($event_entrance_fees)."',
    ticket = '".utf8_decode ($event_ticket)."',
    slug = '".$naamfeest."',
    guid = '".$event_guid."',
    genre = '".utf8_decode($event_music_genres)."',
    
    created =  NOW(),
    modified = NOW()
    
    ";

  mysql_query($sql_party) or die(mysql_error()); $sql_party = mysql_insert_id();
echo " !!TOEGEVOEGD";
}

}
echo "<br>";
}}