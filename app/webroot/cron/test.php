<?php
$newTZ = new DateTimeZone("Europe/Amsterdam");
$date = new DateTime( "2014-06-28 11:00:00 UTC");
$date->setTimezone( $newTZ );
echo $date->format('Y-m-d H:i:s');
?>