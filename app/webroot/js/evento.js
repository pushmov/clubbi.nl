$(document).ready(function() {
  $('.image-link').magnificPopup({type:'image'});
	$('input.datepicker').Zebra_DatePicker();
	$('#dice_wrapper').hide();
});


if ($("#Win").is(':checked')){
    $("#giveawayz").show();
}

$(document).ready(function(){
    $('#Win').change(function(){
        if(this.checked)
            $('#giveawayz').fadeIn('slow');
        else
            $('#giveawayz').fadeOut('slow');

    });
});

(function($) {
	/***************************************************************
	 *** MAP ***
	 **************************************************************/
	if(typeof google != 'undefined') {
		var Map = {
			map: null,
			addressMarker: null,
			country: '',
			options: {
				scrollwheel: false,
				zoom: 15,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: new google.maps.LatLng(34.921631, -35.65625, true),
				mapTypeControl: false,
				streetViewControl: true
			},

			// start map
			init: function (container) {
				this.map = new google.maps.Map(document.getElementById(container), this.options);
			},

			// show a marker in the specified latitude and longitude
			showLatLng: function (lat, lng) {
				this.options.center = new google.maps.LatLng(lat, lng);
				if(!this.addressMarker) {
					this.addressMarker = new google.maps.Marker(
						{
							map: this.map,
							position: this.options.center
						});
				}
				else {
					this.addressMarker.setPosition(this.options.center);
				}
				this.map.panTo(this.options.center);
			},

			// show a marker in the specified address
			showAddress: function (address) {
				geocoder = new google.maps.Geocoder();
				geocoder.geocode( { 'address': address}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						Map.map.setCenter(results[0].geometry.location);
						if(!Map.addressMarker) {
							Map.addressMarker = new google.maps.Marker({
								map: Map.map,
								position: results[0].geometry.location
							});
						}
						else {
							Map.addressMarker.setPosition(results[0].geometry.location);
						}
					} else {
						//alert("Geocode was not successful for the following reason: " + status);
					}
				});
			},

			// allow to move the marker in the map
			mapEdit: function () {
				if(this.addressMarker === null) {
					this.addressMarker = new google.maps.Marker({
						map: this.map,
						draggable: true
					});
				}
				else {
					this.addressMarker.setOptions({draggable: true});
				}
				this.options.zoom = 2;
				this.options.streetViewControl = false;
				this.map.setOptions(this.options);

				google.maps.event.addListener(Map.addressMarker, "position_changed",
					function() {
						point = Map.addressMarker.getPosition();
						$("#VenueLat").val(point.lat());
						$("#VenueLng").val(point.lng());
					}
				);
			}
		};
	}

	/***************************************************************
	 *** FUNCTIONS ***
	 **************************************************************/
	// enable or disable an element
	function toggleEnabled(el) {
		if(el.is(':disabled')) el.removeAttr('disabled');
		else el.attr('disabled', 'disabled');
	}

	// enable or disable monthly repeat options
	function swithMonthlyOptions() {
		toggleEnabled($('#EventDirection'))
		toggleEnabled($('#EventMonthDay'));
		toggleEnabled($('#EventMonthlyWeekdays'));
	}

	// enable or disable general repeat options
	function switchGeneralOptions() {
		toggleEnabled($('#EventRepeatOccurrences'));
		toggleEnabled($('#EventRepeatUntilDay'));
		toggleEnabled($('#EventRepeatUntilMonth'));
		toggleEnabled($('#EventRepeatUntilYear'));
		toggleEnabled($('#EventRepeatUntilHour'));
		toggleEnabled($('#EventRepeatUntilMin'));
		toggleEnabled($('#EventRepeatUntilMeridian'));
	}

	// switch the end date checkbox
	var $end_date_input = $('#end_date_input').hide();
	var $start_date_input = $('#start_date_input');
	function switchEndDate() {
		var elements = $('.EndDate');
		if($('#end_date_check').is(':checked')) {
			elements.each(function(i, el)  { $(el).removeAttr('disabled'); });
	
		  $end_date_input.find('#EventEndDateMeridian').val($start_date_input.find('#EventStartDateMeridian').val());
			$end_date_input.show();
		}
		else {
			elements.each(function(i, el)  { $(el).attr('disabled', 'disabled'); });
			$end_date_input.hide();
		}
	}
	
	//add toggle form for giveaway
	function checkGiveAway(){
		var $el = $('#Win');
		if($el.is(':checked')) {
			$('#Win_').val('1');
			
			$('#Win_start_date').prop('disabled', false);
			$('#Win_end_date').prop('disabled', false);
			$('#Win_number_winners').prop('disabled', false);
			$('#Win_chance').prop('disabled', false);
			$('#Win_price_guestlist').prop('disabled', false);
			$('#Win_price_tickets').prop('disabled', false);
			$('#Win_guestlisttime').prop('disabled', false);
			
		}
		else {
			$('#Win_').val('0');
			
			$('#Win_start_date').prop('disabled', true);
			$('#Win_end_date').prop('disabled', true);
			$('#Win_number_winners').prop('disabled', true);
			$('#Win_chance').prop('disabled', true);
			$('#Win_price_guestlist').prop('disabled', true);
			$('#Win_price_tickets').prop('disabled', true);
			$('#Win_guestlisttime').prop('disabled', true);
		}
	}

	function checkPricetickets(){
		var $el = $('#Win_price_tickets');
		if($el.is(':checked')) {
			$('#Win_price_tickets').val('1');
			
		$('#Win_price_guestlist').prop('disabled', true);
			$('#Win_guestlisttime').prop('disabled', true);
			
		}
		else {
			$('#Win_price_tickets').val('0');
			
			$('#Win_price_guestlist').prop('disabled', false);
			$('#Win_guestlisttime').prop('disabled', false);
		}
	}
	function checkPriceguestlist(){
		var $el = $('#Win_price_guestlist');
		if($el.is(':checked')) {
			$('#Win_price_guestlist').val('1');
			
		$('#Win_price_tickets').prop('disabled', true);
			
		}
		else {
			$('#Win_price_guestlist').val('0');		
			$('#Win_price_tickets').prop('disabled', false);
		}
	}
	
	$('#tos').change(function(){
		if($(this).is(':checked')){
				$('#dice_wrapper').show('fast');
		}else {
			$('#dice_wrapper').hide();
		}	
	});

	
$('#Win_price_tickets').change(function() {
		if($(this).is(':checked')) {
			$('#Win_price_tickets').val('1');
			$('#Win_price_guestlist').prop('disabled', true);
			$('#Win_guestlisttime').prop('disabled', true);
			
		}
		else {
			$('#Win_price_tickets').val('0');
			$('#Win_price_guestlist').prop('disabled', false);
			$('#Win_price_tickets').prop('disabled', false);
			$('#Win_guestlisttime').prop('disabled', false);
		}
	});
$('#Win_price_guestlist').change(function() {
		if($(this).is(':checked')) {
			$('#Win_price_guestlist').val('1');
			$('#Win_price_tickets').prop('disabled', true);
			
		}
		else {
			$('#Win_price_guestlist').val('0');
			$('#Win_price_tickets').prop('disabled', false);
		}
	});




	$('#Win').change(function() {
		if($(this).is(':checked')) {
			$('#Win_').val('1');
			
			$('#Win_start_date').prop('disabled', false);
			$('#Win_end_date').prop('disabled', false);
			$('#Win_number_winners').prop('disabled', false);
			$('#Win_chance').prop('disabled', false);
			$('#Win_price_guestlist').prop('disabled', false);
			$('#Win_price_tickets').prop('disabled', false);
			$('#Win_guestlisttime').prop('disabled', false);
			
		}
		else {
			$('#Win_').val('0');
			$('#Win_start_date').prop('disabled', true);
			$('#Win_end_date').prop('disabled', true);
			$('#Win_number_winners').prop('disabled', true);
			$('#Win_chance').prop('disabled', true);
			$('#Win_price_guestlist').prop('disabled', true);
			$('#Win_price_tickets').prop('disabled', true);
			$('#Win_guestlisttime').prop('disabled', true);
		}
	});
	

	// hide or show the event repeat options blocks
	function hideFields(option) {
		var repeat_options = $('#repeat-options').hide();
		var repeat_daily = $('#repeat-daily-form').hide();
		var repeat_weekly = $('#repeat-weekly-form').hide();
		var repeat_monthly = $('#repeat-monthly-form').hide();
		switch(option) {
			case 'daily':
				repeat_daily.show();
				break;
			case 'weekly':
				repeat_weekly.show();
				break;
			case 'monthly':
				repeat_monthly.show();
				break;
		}
		if(option!='does_not_repeat') {
			repeat_options.show();
		}
	};

	/***************************************************************
	 *** DOCUMENT READY ***
	 **************************************************************/
	$(document).ready(function() {

    /**
     * write a comment
     */
		 checkGiveAway();
		 checkPriceguestlist();
		 checkPricetickets();
		 
		 if($('#tos').is(':checked')){
			$('#dice_wrapper').show();
		 } else {
			$('#dice_wrapper').hide();
		 }
    $('#CommentWrite').live('submit', function(e) {
      e.preventDefault();
      $this = $(e.currentTarget);
      $this.find('#submit-button').addClass('disabled-button').attr('disabled', 'disabled');
      $.post(
        $this.attr('action'),
        $this.serialize(),
        function(data, status, xhr) {
          $('.comments-form').remove();
          $('.block-comments').append(data);
        }, 'html'
      )
      .fail(function() {
        window.location.reload(true);
      });
    });

    var thumbsPaginator = $('#thumbnails-paginator');
    if(thumbsPaginator.length) {
      var thumbsNext = thumbsPaginator.find('#thumbs-next');
      var thumbsPrev = thumbsPaginator.find('#thumbs-prev');

      var paginate = function(e) {
        e.preventDefault();
        $.get(
          $(e.currentTarget).attr('href'),
          null,
          function(data, textStatus, jqXHR) {
            $('#thumbnails-column').replaceWith(data);
          }
        );
      };
          
      $('#thumbs-next').live('click', paginate);
      $('#thumbs-prev').live('click', paginate);

    }

    // image gallery in events view page
    $('.small-image').live('click', function(e) {
      e.preventDefault();
      $('#photo-loader').show();
      var $this = $(e.currentTarget);
      var pImg = new Image();
      pImg.src = $this.data('photo');
      pImg.onload = function() {
        $('#photo-loader').hide();
        $('#big-image').attr('src', $this.data('photo'));
      }
    });


		// start google map
		var map_container = $('div#map');
		if(map_container.length) {
			Map.init('map');
			if(map_container.data('lat')) {
				Map.showLatLng(map_container.data('lat'), map_container.data('lng'));
			}
			if(map_container.hasClass('map-edit')) {
				Map.mapEdit();
			}
		}

		// i'm going button
		var imgoing_container = $('#imgoing');
		if(imgoing_container.length) {
			imgoing_container.on('click', function(e) {
				$.ajax({
					url: imgoing_container.attr('href'),
					dataType: 'json',
					success: function(data) {
						imgoing_container.text(data.text);
						imgoing_container.attr('href', data.link);
						if(data.attendee == 0) {
							imgoing_container.addClass('imgoing-button');
							imgoing_container.removeClass('imgoing-cancel');
							$('#user-list #attendee-me').remove();
							if($('#user-list a').length < 1) {
								$('#user-list').prepend(data.empty);
							}
						}
						else {
							imgoing_container.removeClass('imgoing-button');
							imgoing_container.addClass('imgoing-cancel');
							$('#user-list').prepend(data.template);
							$('#user-list p.empty-message').remove();
						}
						//flashMessage(data.flash);
					},
					error: function(error) {
						//console.log(error);
					}
				});
				e.preventDefault();
			});
		}

		// disable submit button after a form has been submited
		var submit_button = $('#submit-button');
		if(submit_button.length) {
			var form = submit_button.parents('form');
			form.on('submit', function(e) {
				submit_button.attr('disabled', 'disabled');
			});
		}

		/***************************************************************
		 *** VENUES FORM ***
		 **************************************************************/
		var f = $('form.event-form');
		var v = $('form.venue-form');

		if(f.length || v.length) {

			var venue_block = $("#venue-name-block");
			if(venue_block.length) {venue_block.hide();}

			// reset venue link
			$('#reset-venue').on('click', function (e) {
				$("#EventVenueId").val('');
				$("#VenueName").val('');
				$("#VenueAddress").val('');
				$("#VenueLat").val('');
				$("#VenueLng").val('');
				$("#venue-name-block").toggle();
				$("#VenueName").toggle();
				$("#VenueName").focus();
				e.preventDefault();
			});

			// when a country is selected show it in the map
			var country_name = $('#CityCountryName');
			var country_id = $('#CityCountryId');
			var city_name = $('#CityName');

			country_id.on('change',
				function(e) {
					Map.country = country_id.find('option:selected').text();
					Map.map.setZoom(4);
					Map.showAddress(Map.country);
				})
				.find('option:selected').text();

			// handle autocomplete fields
			$('.auto_complete').prev('input').on('keyup', function(e) {
				var $this = $(this);
				var autocomplete = $this.next('div');
				if($this.val().length > 0 && e.keyCode !== 27) {
					var data = {};
					data[$this.attr('name')] = $this.val();
					if(autocomplete.data('rfield') == 'country' && country_id.length) {
						data[country_id.attr('name')] = country_id.find('option:selected').val();
					}
					$.ajax({
						url: autocomplete.data('url'),
						type: 'POST',
						data: data,
						success: function(data) {
							if($(data).children().length > 0) {
								autocomplete.css('top', autocomplete.prev('input').css('bottom'));
								autocomplete.html(data);
								autocomplete.fadeIn();
							}
							else {
								autocomplete.fadeOut();
							}
						}
					});
				}
				else {
					autocomplete.hide();
				}
			})
			.blur(function() {
				$('.auto_complete').delay(500).fadeOut();
			});

			// click event on the venues autocomplete list
			$('#VenueName_autoComplete ul li').live('click', function(e) {
				var $this = $(this)
				if($this.children().eq(0).attr('id')==0) {
					$("#venue-form").show();
					if(typeof google != 'undefined') {
						google.maps.event.trigger(Map.map, 'resize');
						Map.map.setOptions(Map.options);

						if(country_name.val()) {
							Map.country = country_name.val();
							if(city_name.val()) {
									Map.map.setZoom(15);
									Map.showAddress(city_name.val() + ', ' + Map.country);
							} else {
								Map.map.setZoom(4);
								Map.showAddress(Map.country);
							}
						}

					}
				}
				else {
					$("#VenueName").val($this.children().eq(0).text());
					$("#EventVenueId").val($this.children().eq(0).attr('id'));
					$("#venue-data").html($this.children());
					$("#VenueName").hide();
					$("#venue-name-block").show();
					$("#venue-form").hide();
				}
				$('#VenueName_autoComplete').hide();
			});

			// click a city in the cities autocomplete list
			$('#CityName_autoComplete ul li').live('click', function() {
				$('#CityName').val($(this).text()).trigger('change');
				$('#CityName_autoComplete').hide();
			});

			// update the map marker address
			$('#CityName').on("change keyup", function() {
				Map.showAddress($(this).val() + ', ' + Map.country);
			});

			// show address in the map as the user types it
			$("#VenueAddress").on('keyup', function() {
				Map.map.setZoom(15);
				Map.showAddress($("#VenueAddress").val() + ", " + $("#CityName").val() + ", " + Map.country);
			});
		}

		if($("#VenueAddress").val()) {
			Map.map.setZoom(15);
			Map.showAddress($("#VenueAddress").val() + ", " + $("#CityName").val() + ", " + Map.country);
		}

		/***************************************************************
		 *** EVENTS FORM ***
		 **************************************************************/
		if(f.length) {
			// if venue data exists show the venue box
			var event_venue_id = $('#EventVenueId');
			if(event_venue_id.length && event_venue_id.val()) {
				var element = "<p class=\"venue-name\">"
				+ $('#hiddenVenueName').val() + '</p><p class=\"venue-info\">'
				+ $('#hiddenVenueAddress').val() + ', '
				+ $('#hiddenCityName').val() + ', '
				+ $("#hiddenCountryName").val() + '.</p>';

				$("#venue-data").append(element);
				$("#venue-name-block").toggle();
				$("#VenueName").toggle();
			}

			// click an option in the tags autocomplete list
			var tags = $('#Tags');
			$('#Tags_autoComplete ul li').live('click', function(e) {
				var item = $(this).text();
				if(tags.val().lastIndexOf(",")>0) item = ', ' + item;
				tags.val(tags.val().substr(0, tags.val().lastIndexOf(',')) + item);
				tags.focus();
				$(this).closest('div.auto_complete').hide();
			});

			// enable or disable the end date fields
			var end_date_field = $('#end_date_check');
			if(end_date_field.is(':checked')) switchEndDate();
			end_date_field.on('change', function(e) {
				switchEndDate();
			});

			/***************************************************************
			 *** EVENT REPEAT OPTIONS ***
			 **************************************************************/
			// show the corresponding panel when repeat select input changes
			var event_repeat = $('#EventRepeat');
			event_repeat.on('change', function(e) {
				hideFields(event_repeat.find('option:selected').val());
			});
			event_repeat.trigger('change');

			// disable specific day fields in monthly repeat
			$('#EventDirection').attr('disabled', 'disabled');
			$('#EventMonthDay').attr('disabled', 'disabled');
			$('#EventRepeatOccurrences').attr('disabled', 'disabled');

			// disable or enable corresponding fields in monthly repeat
			$('#same_day').on('change', function(e) {
				swithMonthlyOptions();
			});

			// disable or enable corresponding fields in monthly repeat
			$('#specific_day').on('change', function(e) {
				swithMonthlyOptions();
			});

			// disable or enable corresponding repeat fields
			$('#repeat_occurences').on('change', function(e) {
				switchGeneralOptions();
			});

			// disable or enable corresponding repeat fields
			$('#repeat_until').on('change', function(e) {
				switchGeneralOptions();
			});
		}
	});
})(jQuery);

jQuery('#facebook').click(function(e){
	 console.log('halo');
 jQuery.oauthpopup({
path: 'facebooklogin',  
width:600,
height:300,
callback: function(){
//window.location.reload();
window.location = $('span#redir-page').attr('data-url');
}
});
e.preventDefault();
});

/*! Magnific Popup - v1.0.0 - 2015-01-03
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2015 Dmitry Semenov; */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):window.jQuery||window.Zepto)}(function(a){var b,c,d,e,f,g,h="Close",i="BeforeClose",j="AfterClose",k="BeforeAppend",l="MarkupParse",m="Open",n="Change",o="mfp",p="."+o,q="mfp-ready",r="mfp-removing",s="mfp-prevent-close",t=function(){},u=!!window.jQuery,v=a(window),w=function(a,c){b.ev.on(o+a+p,c)},x=function(b,c,d,e){var f=document.createElement("div");return f.className="mfp-"+b,d&&(f.innerHTML=d),e?c&&c.appendChild(f):(f=a(f),c&&f.appendTo(c)),f},y=function(c,d){b.ev.triggerHandler(o+c,d),b.st.callbacks&&(c=c.charAt(0).toLowerCase()+c.slice(1),b.st.callbacks[c]&&b.st.callbacks[c].apply(b,a.isArray(d)?d:[d]))},z=function(c){return c===g&&b.currTemplate.closeBtn||(b.currTemplate.closeBtn=a(b.st.closeMarkup.replace("%title%",b.st.tClose)),g=c),b.currTemplate.closeBtn},A=function(){a.magnificPopup.instance||(b=new t,b.init(),a.magnificPopup.instance=b)},B=function(){var a=document.createElement("p").style,b=["ms","O","Moz","Webkit"];if(void 0!==a.transition)return!0;for(;b.length;)if(b.pop()+"Transition"in a)return!0;return!1};t.prototype={constructor:t,init:function(){var c=navigator.appVersion;b.isIE7=-1!==c.indexOf("MSIE 7."),b.isIE8=-1!==c.indexOf("MSIE 8."),b.isLowIE=b.isIE7||b.isIE8,b.isAndroid=/android/gi.test(c),b.isIOS=/iphone|ipad|ipod/gi.test(c),b.supportsTransition=B(),b.probablyMobile=b.isAndroid||b.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),d=a(document),b.popupsCache={}},open:function(c){var e;if(c.isObj===!1){b.items=c.items.toArray(),b.index=0;var g,h=c.items;for(e=0;e<h.length;e++)if(g=h[e],g.parsed&&(g=g.el[0]),g===c.el[0]){b.index=e;break}}else b.items=a.isArray(c.items)?c.items:[c.items],b.index=c.index||0;if(b.isOpen)return void b.updateItemHTML();b.types=[],f="",b.ev=c.mainEl&&c.mainEl.length?c.mainEl.eq(0):d,c.key?(b.popupsCache[c.key]||(b.popupsCache[c.key]={}),b.currTemplate=b.popupsCache[c.key]):b.currTemplate={},b.st=a.extend(!0,{},a.magnificPopup.defaults,c),b.fixedContentPos="auto"===b.st.fixedContentPos?!b.probablyMobile:b.st.fixedContentPos,b.st.modal&&(b.st.closeOnContentClick=!1,b.st.closeOnBgClick=!1,b.st.showCloseBtn=!1,b.st.enableEscapeKey=!1),b.bgOverlay||(b.bgOverlay=x("bg").on("click"+p,function(){b.close()}),b.wrap=x("wrap").attr("tabindex",-1).on("click"+p,function(a){b._checkIfClose(a.target)&&b.close()}),b.container=x("container",b.wrap)),b.contentContainer=x("content"),b.st.preloader&&(b.preloader=x("preloader",b.container,b.st.tLoading));var i=a.magnificPopup.modules;for(e=0;e<i.length;e++){var j=i[e];j=j.charAt(0).toUpperCase()+j.slice(1),b["init"+j].call(b)}y("BeforeOpen"),b.st.showCloseBtn&&(b.st.closeBtnInside?(w(l,function(a,b,c,d){c.close_replaceWith=z(d.type)}),f+=" mfp-close-btn-in"):b.wrap.append(z())),b.st.alignTop&&(f+=" mfp-align-top"),b.wrap.css(b.fixedContentPos?{overflow:b.st.overflowY,overflowX:"hidden",overflowY:b.st.overflowY}:{top:v.scrollTop(),position:"absolute"}),(b.st.fixedBgPos===!1||"auto"===b.st.fixedBgPos&&!b.fixedContentPos)&&b.bgOverlay.css({height:d.height(),position:"absolute"}),b.st.enableEscapeKey&&d.on("keyup"+p,function(a){27===a.keyCode&&b.close()}),v.on("resize"+p,function(){b.updateSize()}),b.st.closeOnContentClick||(f+=" mfp-auto-cursor"),f&&b.wrap.addClass(f);var k=b.wH=v.height(),n={};if(b.fixedContentPos&&b._hasScrollBar(k)){var o=b._getScrollbarSize();o&&(n.marginRight=o)}b.fixedContentPos&&(b.isIE7?a("body, html").css("overflow","hidden"):n.overflow="hidden");var r=b.st.mainClass;return b.isIE7&&(r+=" mfp-ie7"),r&&b._addClassToMFP(r),b.updateItemHTML(),y("BuildControls"),a("html").css(n),b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo||a(document.body)),b._lastFocusedEl=document.activeElement,setTimeout(function(){b.content?(b._addClassToMFP(q),b._setFocus()):b.bgOverlay.addClass(q),d.on("focusin"+p,b._onFocusIn)},16),b.isOpen=!0,b.updateSize(k),y(m),c},close:function(){b.isOpen&&(y(i),b.isOpen=!1,b.st.removalDelay&&!b.isLowIE&&b.supportsTransition?(b._addClassToMFP(r),setTimeout(function(){b._close()},b.st.removalDelay)):b._close())},_close:function(){y(h);var c=r+" "+q+" ";if(b.bgOverlay.detach(),b.wrap.detach(),b.container.empty(),b.st.mainClass&&(c+=b.st.mainClass+" "),b._removeClassFromMFP(c),b.fixedContentPos){var e={marginRight:""};b.isIE7?a("body, html").css("overflow",""):e.overflow="",a("html").css(e)}d.off("keyup"+p+" focusin"+p),b.ev.off(p),b.wrap.attr("class","mfp-wrap").removeAttr("style"),b.bgOverlay.attr("class","mfp-bg"),b.container.attr("class","mfp-container"),!b.st.showCloseBtn||b.st.closeBtnInside&&b.currTemplate[b.currItem.type]!==!0||b.currTemplate.closeBtn&&b.currTemplate.closeBtn.detach(),b._lastFocusedEl&&a(b._lastFocusedEl).focus(),b.currItem=null,b.content=null,b.currTemplate=null,b.prevHeight=0,y(j)},updateSize:function(a){if(b.isIOS){var c=document.documentElement.clientWidth/window.innerWidth,d=window.innerHeight*c;b.wrap.css("height",d),b.wH=d}else b.wH=a||v.height();b.fixedContentPos||b.wrap.css("height",b.wH),y("Resize")},updateItemHTML:function(){var c=b.items[b.index];b.contentContainer.detach(),b.content&&b.content.detach(),c.parsed||(c=b.parseEl(b.index));var d=c.type;if(y("BeforeChange",[b.currItem?b.currItem.type:"",d]),b.currItem=c,!b.currTemplate[d]){var f=b.st[d]?b.st[d].markup:!1;y("FirstMarkupParse",f),b.currTemplate[d]=f?a(f):!0}e&&e!==c.type&&b.container.removeClass("mfp-"+e+"-holder");var g=b["get"+d.charAt(0).toUpperCase()+d.slice(1)](c,b.currTemplate[d]);b.appendContent(g,d),c.preloaded=!0,y(n,c),e=c.type,b.container.prepend(b.contentContainer),y("AfterChange")},appendContent:function(a,c){b.content=a,a?b.st.showCloseBtn&&b.st.closeBtnInside&&b.currTemplate[c]===!0?b.content.find(".mfp-close").length||b.content.append(z()):b.content=a:b.content="",y(k),b.container.addClass("mfp-"+c+"-holder"),b.contentContainer.append(b.content)},parseEl:function(c){var d,e=b.items[c];if(e.tagName?e={el:a(e)}:(d=e.type,e={data:e,src:e.src}),e.el){for(var f=b.types,g=0;g<f.length;g++)if(e.el.hasClass("mfp-"+f[g])){d=f[g];break}e.src=e.el.attr("data-mfp-src"),e.src||(e.src=e.el.attr("href"))}return e.type=d||b.st.type||"inline",e.index=c,e.parsed=!0,b.items[c]=e,y("ElementParse",e),b.items[c]},addGroup:function(a,c){var d=function(d){d.mfpEl=this,b._openClick(d,a,c)};c||(c={});var e="click.magnificPopup";c.mainEl=a,c.items?(c.isObj=!0,a.off(e).on(e,d)):(c.isObj=!1,c.delegate?a.off(e).on(e,c.delegate,d):(c.items=a,a.off(e).on(e,d)))},_openClick:function(c,d,e){var f=void 0!==e.midClick?e.midClick:a.magnificPopup.defaults.midClick;if(f||2!==c.which&&!c.ctrlKey&&!c.metaKey){var g=void 0!==e.disableOn?e.disableOn:a.magnificPopup.defaults.disableOn;if(g)if(a.isFunction(g)){if(!g.call(b))return!0}else if(v.width()<g)return!0;c.type&&(c.preventDefault(),b.isOpen&&c.stopPropagation()),e.el=a(c.mfpEl),e.delegate&&(e.items=d.find(e.delegate)),b.open(e)}},updateStatus:function(a,d){if(b.preloader){c!==a&&b.container.removeClass("mfp-s-"+c),d||"loading"!==a||(d=b.st.tLoading);var e={status:a,text:d};y("UpdateStatus",e),a=e.status,d=e.text,b.preloader.html(d),b.preloader.find("a").on("click",function(a){a.stopImmediatePropagation()}),b.container.addClass("mfp-s-"+a),c=a}},_checkIfClose:function(c){if(!a(c).hasClass(s)){var d=b.st.closeOnContentClick,e=b.st.closeOnBgClick;if(d&&e)return!0;if(!b.content||a(c).hasClass("mfp-close")||b.preloader&&c===b.preloader[0])return!0;if(c===b.content[0]||a.contains(b.content[0],c)){if(d)return!0}else if(e&&a.contains(document,c))return!0;return!1}},_addClassToMFP:function(a){b.bgOverlay.addClass(a),b.wrap.addClass(a)},_removeClassFromMFP:function(a){this.bgOverlay.removeClass(a),b.wrap.removeClass(a)},_hasScrollBar:function(a){return(b.isIE7?d.height():document.body.scrollHeight)>(a||v.height())},_setFocus:function(){(b.st.focus?b.content.find(b.st.focus).eq(0):b.wrap).focus()},_onFocusIn:function(c){return c.target===b.wrap[0]||a.contains(b.wrap[0],c.target)?void 0:(b._setFocus(),!1)},_parseMarkup:function(b,c,d){var e;d.data&&(c=a.extend(d.data,c)),y(l,[b,c,d]),a.each(c,function(a,c){if(void 0===c||c===!1)return!0;if(e=a.split("_"),e.length>1){var d=b.find(p+"-"+e[0]);if(d.length>0){var f=e[1];"replaceWith"===f?d[0]!==c[0]&&d.replaceWith(c):"img"===f?d.is("img")?d.attr("src",c):d.replaceWith('<img src="'+c+'" class="'+d.attr("class")+'" />'):d.attr(e[1],c)}}else b.find(p+"-"+a).html(c)})},_getScrollbarSize:function(){if(void 0===b.scrollbarSize){var a=document.createElement("div");a.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(a),b.scrollbarSize=a.offsetWidth-a.clientWidth,document.body.removeChild(a)}return b.scrollbarSize}},a.magnificPopup={instance:null,proto:t.prototype,modules:[],open:function(b,c){return A(),b=b?a.extend(!0,{},b):{},b.isObj=!0,b.index=c||0,this.instance.open(b)},close:function(){return a.magnificPopup.instance&&a.magnificPopup.instance.close()},registerModule:function(b,c){c.options&&(a.magnificPopup.defaults[b]=c.options),a.extend(this.proto,c.proto),this.modules.push(b)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&times;</button>',tClose:"Close (Esc)",tLoading:"Loading..."}},a.fn.magnificPopup=function(c){A();var d=a(this);if("string"==typeof c)if("open"===c){var e,f=u?d.data("magnificPopup"):d[0].magnificPopup,g=parseInt(arguments[1],10)||0;f.items?e=f.items[g]:(e=d,f.delegate&&(e=e.find(f.delegate)),e=e.eq(g)),b._openClick({mfpEl:e},d,f)}else b.isOpen&&b[c].apply(b,Array.prototype.slice.call(arguments,1));else c=a.extend(!0,{},c),u?d.data("magnificPopup",c):d[0].magnificPopup=c,b.addGroup(d,c);return d};var C,D,E,F="inline",G=function(){E&&(D.after(E.addClass(C)).detach(),E=null)};a.magnificPopup.registerModule(F,{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){b.types.push(F),w(h+"."+F,function(){G()})},getInline:function(c,d){if(G(),c.src){var e=b.st.inline,f=a(c.src);if(f.length){var g=f[0].parentNode;g&&g.tagName&&(D||(C=e.hiddenClass,D=x(C),C="mfp-"+C),E=f.after(D).detach().removeClass(C)),b.updateStatus("ready")}else b.updateStatus("error",e.tNotFound),f=a("<div>");return c.inlineElement=f,f}return b.updateStatus("ready"),b._parseMarkup(d,{},c),d}}});var H,I="ajax",J=function(){H&&a(document.body).removeClass(H)},K=function(){J(),b.req&&b.req.abort()};a.magnificPopup.registerModule(I,{options:{settings:null,cursor:"mfp-ajax-cur",tError:'<a href="%url%">The content</a> could not be loaded.'},proto:{initAjax:function(){b.types.push(I),H=b.st.ajax.cursor,w(h+"."+I,K),w("BeforeChange."+I,K)},getAjax:function(c){H&&a(document.body).addClass(H),b.updateStatus("loading");var d=a.extend({url:c.src,success:function(d,e,f){var g={data:d,xhr:f};y("ParseAjax",g),b.appendContent(a(g.data),I),c.finished=!0,J(),b._setFocus(),setTimeout(function(){b.wrap.addClass(q)},16),b.updateStatus("ready"),y("AjaxContentAdded")},error:function(){J(),c.finished=c.loadError=!0,b.updateStatus("error",b.st.ajax.tError.replace("%url%",c.src))}},b.st.ajax.settings);return b.req=a.ajax(d),""}}});var L,M=function(c){if(c.data&&void 0!==c.data.title)return c.data.title;var d=b.st.image.titleSrc;if(d){if(a.isFunction(d))return d.call(b,c);if(c.el)return c.el.attr(d)||""}return""};a.magnificPopup.registerModule("image",{options:{markup:'<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',cursor:"mfp-zoom-out-cur",titleSrc:"title",verticalFit:!0,tError:'<a href="%url%">The image</a> could not be loaded.'},proto:{initImage:function(){var c=b.st.image,d=".image";b.types.push("image"),w(m+d,function(){"image"===b.currItem.type&&c.cursor&&a(document.body).addClass(c.cursor)}),w(h+d,function(){c.cursor&&a(document.body).removeClass(c.cursor),v.off("resize"+p)}),w("Resize"+d,b.resizeImage),b.isLowIE&&w("AfterChange",b.resizeImage)},resizeImage:function(){var a=b.currItem;if(a&&a.img&&b.st.image.verticalFit){var c=0;b.isLowIE&&(c=parseInt(a.img.css("padding-top"),10)+parseInt(a.img.css("padding-bottom"),10)),a.img.css("max-height",b.wH-c)}},_onImageHasSize:function(a){a.img&&(a.hasSize=!0,L&&clearInterval(L),a.isCheckingImgSize=!1,y("ImageHasSize",a),a.imgHidden&&(b.content&&b.content.removeClass("mfp-loading"),a.imgHidden=!1))},findImageSize:function(a){var c=0,d=a.img[0],e=function(f){L&&clearInterval(L),L=setInterval(function(){return d.naturalWidth>0?void b._onImageHasSize(a):(c>200&&clearInterval(L),c++,void(3===c?e(10):40===c?e(50):100===c&&e(500)))},f)};e(1)},getImage:function(c,d){var e=0,f=function(){c&&(c.img[0].complete?(c.img.off(".mfploader"),c===b.currItem&&(b._onImageHasSize(c),b.updateStatus("ready")),c.hasSize=!0,c.loaded=!0,y("ImageLoadComplete")):(e++,200>e?setTimeout(f,100):g()))},g=function(){c&&(c.img.off(".mfploader"),c===b.currItem&&(b._onImageHasSize(c),b.updateStatus("error",h.tError.replace("%url%",c.src))),c.hasSize=!0,c.loaded=!0,c.loadError=!0)},h=b.st.image,i=d.find(".mfp-img");if(i.length){var j=document.createElement("img");j.className="mfp-img",c.el&&c.el.find("img").length&&(j.alt=c.el.find("img").attr("alt")),c.img=a(j).on("load.mfploader",f).on("error.mfploader",g),j.src=c.src,i.is("img")&&(c.img=c.img.clone()),j=c.img[0],j.naturalWidth>0?c.hasSize=!0:j.width||(c.hasSize=!1)}return b._parseMarkup(d,{title:M(c),img_replaceWith:c.img},c),b.resizeImage(),c.hasSize?(L&&clearInterval(L),c.loadError?(d.addClass("mfp-loading"),b.updateStatus("error",h.tError.replace("%url%",c.src))):(d.removeClass("mfp-loading"),b.updateStatus("ready")),d):(b.updateStatus("loading"),c.loading=!0,c.hasSize||(c.imgHidden=!0,d.addClass("mfp-loading"),b.findImageSize(c)),d)}}});var N,O=function(){return void 0===N&&(N=void 0!==document.createElement("p").style.MozTransform),N};a.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(a){return a.is("img")?a:a.find("img")}},proto:{initZoom:function(){var a,c=b.st.zoom,d=".zoom";if(c.enabled&&b.supportsTransition){var e,f,g=c.duration,j=function(a){var b=a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),d="all "+c.duration/1e3+"s "+c.easing,e={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},f="transition";return e["-webkit-"+f]=e["-moz-"+f]=e["-o-"+f]=e[f]=d,b.css(e),b},k=function(){b.content.css("visibility","visible")};w("BuildControls"+d,function(){if(b._allowZoom()){if(clearTimeout(e),b.content.css("visibility","hidden"),a=b._getItemToZoom(),!a)return void k();f=j(a),f.css(b._getOffset()),b.wrap.append(f),e=setTimeout(function(){f.css(b._getOffset(!0)),e=setTimeout(function(){k(),setTimeout(function(){f.remove(),a=f=null,y("ZoomAnimationEnded")},16)},g)},16)}}),w(i+d,function(){if(b._allowZoom()){if(clearTimeout(e),b.st.removalDelay=g,!a){if(a=b._getItemToZoom(),!a)return;f=j(a)}f.css(b._getOffset(!0)),b.wrap.append(f),b.content.css("visibility","hidden"),setTimeout(function(){f.css(b._getOffset())},16)}}),w(h+d,function(){b._allowZoom()&&(k(),f&&f.remove(),a=null)})}},_allowZoom:function(){return"image"===b.currItem.type},_getItemToZoom:function(){return b.currItem.hasSize?b.currItem.img:!1},_getOffset:function(c){var d;d=c?b.currItem.img:b.st.zoom.opener(b.currItem.el||b.currItem);var e=d.offset(),f=parseInt(d.css("padding-top"),10),g=parseInt(d.css("padding-bottom"),10);e.top-=a(window).scrollTop()-f;var h={width:d.width(),height:(u?d.innerHeight():d[0].offsetHeight)-g-f};return O()?h["-moz-transform"]=h.transform="translate("+e.left+"px,"+e.top+"px)":(h.left=e.left,h.top=e.top),h}}});var P="iframe",Q="//about:blank",R=function(a){if(b.currTemplate[P]){var c=b.currTemplate[P].find("iframe");c.length&&(a||(c[0].src=Q),b.isIE8&&c.css("display",a?"block":"none"))}};a.magnificPopup.registerModule(P,{options:{markup:'<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',srcAction:"iframe_src",patterns:{youtube:{index:"youtube.com",id:"v=",src:"//www.youtube.com/embed/%id%?autoplay=1"},vimeo:{index:"vimeo.com/",id:"/",src:"//player.vimeo.com/video/%id%?autoplay=1"},gmaps:{index:"//maps.google.",src:"%id%&output=embed"}}},proto:{initIframe:function(){b.types.push(P),w("BeforeChange",function(a,b,c){b!==c&&(b===P?R():c===P&&R(!0))}),w(h+"."+P,function(){R()})},getIframe:function(c,d){var e=c.src,f=b.st.iframe;a.each(f.patterns,function(){return e.indexOf(this.index)>-1?(this.id&&(e="string"==typeof this.id?e.substr(e.lastIndexOf(this.id)+this.id.length,e.length):this.id.call(this,e)),e=this.src.replace("%id%",e),!1):void 0});var g={};return f.srcAction&&(g[f.srcAction]=e),b._parseMarkup(d,g,c),b.updateStatus("ready"),d}}});var S=function(a){var c=b.items.length;return a>c-1?a-c:0>a?c+a:a},T=function(a,b,c){return a.replace(/%curr%/gi,b+1).replace(/%total%/gi,c)};a.magnificPopup.registerModule("gallery",{options:{enabled:!1,arrowMarkup:'<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',preload:[0,2],navigateByImgClick:!0,arrows:!0,tPrev:"Previous (Left arrow key)",tNext:"Next (Right arrow key)",tCounter:"%curr% of %total%"},proto:{initGallery:function(){var c=b.st.gallery,e=".mfp-gallery",g=Boolean(a.fn.mfpFastClick);return b.direction=!0,c&&c.enabled?(f+=" mfp-gallery",w(m+e,function(){c.navigateByImgClick&&b.wrap.on("click"+e,".mfp-img",function(){return b.items.length>1?(b.next(),!1):void 0}),d.on("keydown"+e,function(a){37===a.keyCode?b.prev():39===a.keyCode&&b.next()})}),w("UpdateStatus"+e,function(a,c){c.text&&(c.text=T(c.text,b.currItem.index,b.items.length))}),w(l+e,function(a,d,e,f){var g=b.items.length;e.counter=g>1?T(c.tCounter,f.index,g):""}),w("BuildControls"+e,function(){if(b.items.length>1&&c.arrows&&!b.arrowLeft){var d=c.arrowMarkup,e=b.arrowLeft=a(d.replace(/%title%/gi,c.tPrev).replace(/%dir%/gi,"left")).addClass(s),f=b.arrowRight=a(d.replace(/%title%/gi,c.tNext).replace(/%dir%/gi,"right")).addClass(s),h=g?"mfpFastClick":"click";e[h](function(){b.prev()}),f[h](function(){b.next()}),b.isIE7&&(x("b",e[0],!1,!0),x("a",e[0],!1,!0),x("b",f[0],!1,!0),x("a",f[0],!1,!0)),b.container.append(e.add(f))}}),w(n+e,function(){b._preloadTimeout&&clearTimeout(b._preloadTimeout),b._preloadTimeout=setTimeout(function(){b.preloadNearbyImages(),b._preloadTimeout=null},16)}),void w(h+e,function(){d.off(e),b.wrap.off("click"+e),b.arrowLeft&&g&&b.arrowLeft.add(b.arrowRight).destroyMfpFastClick(),b.arrowRight=b.arrowLeft=null})):!1},next:function(){b.direction=!0,b.index=S(b.index+1),b.updateItemHTML()},prev:function(){b.direction=!1,b.index=S(b.index-1),b.updateItemHTML()},goTo:function(a){b.direction=a>=b.index,b.index=a,b.updateItemHTML()},preloadNearbyImages:function(){var a,c=b.st.gallery.preload,d=Math.min(c[0],b.items.length),e=Math.min(c[1],b.items.length);for(a=1;a<=(b.direction?e:d);a++)b._preloadItem(b.index+a);for(a=1;a<=(b.direction?d:e);a++)b._preloadItem(b.index-a)},_preloadItem:function(c){if(c=S(c),!b.items[c].preloaded){var d=b.items[c];d.parsed||(d=b.parseEl(c)),y("LazyLoad",d),"image"===d.type&&(d.img=a('<img class="mfp-img" />').on("load.mfploader",function(){d.hasSize=!0}).on("error.mfploader",function(){d.hasSize=!0,d.loadError=!0,y("LazyLoadError",d)}).attr("src",d.src)),d.preloaded=!0}}}});var U="retina";a.magnificPopup.registerModule(U,{options:{replaceSrc:function(a){return a.src.replace(/\.\w+$/,function(a){return"@2x"+a})},ratio:1},proto:{initRetina:function(){if(window.devicePixelRatio>1){var a=b.st.retina,c=a.ratio;c=isNaN(c)?c():c,c>1&&(w("ImageHasSize."+U,function(a,b){b.img.css({"max-width":b.img[0].naturalWidth/c,width:"100%"})}),w("ElementParse."+U,function(b,d){d.src=a.replaceSrc(d,c)}))}}}}),function(){var b=1e3,c="ontouchstart"in window,d=function(){v.off("touchmove"+f+" touchend"+f)},e="mfpFastClick",f="."+e;a.fn.mfpFastClick=function(e){return a(this).each(function(){var g,h=a(this);if(c){var i,j,k,l,m,n;h.on("touchstart"+f,function(a){l=!1,n=1,m=a.originalEvent?a.originalEvent.touches[0]:a.touches[0],j=m.clientX,k=m.clientY,v.on("touchmove"+f,function(a){m=a.originalEvent?a.originalEvent.touches:a.touches,n=m.length,m=m[0],(Math.abs(m.clientX-j)>10||Math.abs(m.clientY-k)>10)&&(l=!0,d())}).on("touchend"+f,function(a){d(),l||n>1||(g=!0,a.preventDefault(),clearTimeout(i),i=setTimeout(function(){g=!1},b),e())})})}h.on("click"+f,function(){g||e()})})},a.fn.destroyMfpFastClick=function(){a(this).off("touchstart"+f+" click"+f),c&&v.off("touchmove"+f+" touchend"+f)}}(),A()});

 (function (jQuery) {
    jQuery.oauthpopup = function (options) {
    options.windowName = options.windowName || 'ConnectWithOAuth';
    options.windowOptions = options.windowOptions || 'location=0,status=0,width='+options.width+',height='+options.height+',scrollbars=1';
    options.callback = options.callback || function () {
   // window.location.reload();
    };
    var that = this;
    that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
    that._oauthInterval = window.setInterval(function () {
    if (that._oauthWindow.closed) {
    window.clearInterval(that._oauthInterval);
    options.callback();
    }
    }, 1000);
    };
    })(jQuery);

 jQuery(document).ready(function(){
  jQuery.oauthpopup = function (options) {
    options.windowName = options.windowName || 'ConnectWithOAuth';
    options.windowOptions = options.windowOptions || 'location=0,status=0,width='+options.width+',height='+options.height+',scrollbars=1';
    options.callback = options.callback || function () {
    window.location.reload();
    };
    var that = this;
    that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
    that._oauthInterval = window.setInterval(function () {
    if (that._oauthWindow.closed) {
    window.clearInterval(that._oauthInterval);
    options.callback();
    }
    }, 1000);
    };
 
  jQuery('#loginfb').click(function(e){
  jQuery.oauthpopup({
 path: '/users/facebooklogin',  
 width:600,
 height:300,
 callback: function(){
 //window.location.reload();
 window.location = $('span#redir').attr('alt');
 }
 });
 e.preventDefault();
 });
});
