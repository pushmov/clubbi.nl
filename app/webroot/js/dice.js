	(function($) {
	    $.fn.invisible = function() {
	        return this.each(function() {
	            $(this).css("visibility", "hidden");
	        });
	    };
	    $.fn.visible = function() {
	        return this.each(function() {
	            $(this).css("visibility", "visible");
	        });
	    };
	}(jQuery));

		$('#roll-button').click(function() {
			$('#dice').show();
			$('#roll-button').hide();
		
		
			
			$(this).prop('disabled', true);
			$(this).css('background','#e46666');
			var times = Math.floor(Math.random()*70) + 1;
			var eventId = $('#eventId').attr('data-val');
			var res = $('#res').attr('data-val');
			var i = 0;
			var timer = setInterval(function() {
				if (i == times) {
					setValue(res);
					clearInterval(timer);
					if(res == '2') {
						setTimeout(function(){
					$('.winnaar').slideToggle("fast");
				}, 1400);
				}else{
					setTimeout(function(){
					$('.loser').slideToggle("fast");
				}, 1400);
				}
				
				} else {
					roll();
				}
				
				i++;
			}, 120);
			
			//ajax
			$.get('/events/ajaxSubmit/' + eventId + '/' + res ,function(r){
				var j = $.parseJSON(r);
				if(j.status == 'OK') {
					if(j.number == '2') {
						setTimeout(function(){
						
						}, 4500);
					} else {
						setTimeout(function(){
						
						}, 4500);
					}
				}
			});
		});
		
		function roll() {
			var side = Math.floor(Math.random()*6) + 1;
			$('#dice .visible').removeClass('visible');
			$('#dice .side-' + side).addClass('visible');
		}
		
		function setValue(val)
		{
			$('#dice .visible').removeClass('visible');
			$('#dice .side-' + val).addClass('visible');
		}
		
		$(document).ready(function(){
			$('#dice').hide();
		});