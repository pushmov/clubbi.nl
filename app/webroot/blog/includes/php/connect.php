<?php
if(!defined('includeCheck')){ die(); }

error_reporting(E_ALL ^ E_NOTICE);

$mysqli = new mysqli($dbhost, $dbuser, $dbpassword, $dbname);
if ($mysqli->connect_errno) {
	echo "Have you set up the script using the <a href='installer.php'>installer</a>?<br />\n";
    die("Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
}