<?php
if(!defined('includeCheck')){ die(); }

function sanitiseString($string) {

	global $mysqli;
	$string = htmlspecialchars($string);
	$string = mysqli_real_escape_string($mysqli, $string);
	return $string;
}

class getRSS {
	
	public function addFeed($url, $category) {
	
		global $mysqli;
		
		$category = strtolower(sanitiseString($category));
		$url = mysqli_real_escape_string($mysqli, $url);
		$RSSURL = $this->getRSSURL($url);
		if ($RSSURL == FALSE) {
			return FALSE;
		}
		if (strlen($RSSURL)<7) {
			return FALSE;
		}
		$id = uniqid('', TRUE);
		$sql = "INSERT INTO `feeds` (`id`, `url`, `category`, `lastUpdated`) VALUES ('$id', '$RSSURL', '$category', '0');";
		if($mysqli->query($sql)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function getRSSURL($url) {
		$url = sanitiseString($url);
		//get the simplepie library
    	require_once('SimplePie.complied.php');
		//grab the feed
		$feed = new SimplePie();
		$feed->set_feed_url(array(
			$url,
		));
	  
		//disable caching
		$feed->enable_cache(false);
	    
		//provide the caching folder
		$feed->set_cache_location('cache');
	
		//set the amount of seconds you want to cache the feed
		$feed->set_cache_duration(1800);
	
		//init the process
		$feed->init();
		//let simplepie handle the content type (atom, RSS...)
		$feed->handle_content_type();
	
	
		if ($feed->error):
			return FALSE;
		endif;
	  
		foreach ($feed->get_items() as $item):
			if ($item->get_feed()) {
				$feed = $item->get_feed();
			}		
		endforeach;
		 
		foreach ($feed->get_all_discovered_feeds() as $link) {
			$rssurl = $link->url;
		}
	
		if (strlen($rssurl)>0) {
			$url = $rssurl;
		}
	
		return $url;
	}
	
	public function updateAllRSSFeeds() {
	
	global $mysqli;
	$id = sanitiseString($id);
	$timelimit = time()-(60*60);
	$query = "SELECT `id` FROM `feeds` WHERE lastUpdated<".$timelimit." ORDER by lastUpdated DESC";
	if(!$result = $mysqli->query($query)) {
		return FALSE;
	}
	while($row = $result->fetch_assoc()){
		$this->updateRSSFeed($row['id']);
	}
	
	
	}
	
	public function updateRSSFeed($feedid) {

	global $mysqli;
	$id = sanitiseString($feedid);
	
	$timeNow = time();
	$query = "UPDATE `feeds` SET lastUpdated='$timeNow' WHERE id='$id'";
	$updateFeedTime = $mysqli->query($query);
	
	$query = "SELECT `url`, `category` FROM `feeds` WHERE id='".$id."'";
	if(!$result = $mysqli->query($query)) {
		return FALSE;
	}
	while($row = $result->fetch_assoc()){
		$url = $row['url'];
		$category = $row['category'];
	}

	    //get the simplepie library
	    require_once('SimplePie.complied.php');
    
    //grab the feed
    $feed = new SimplePie();
    $feed->set_feed_url(array(
    	$url,
    ));

    
    //disable caching
    $feed->enable_cache(false);
    
    //init the process
    $feed->init();
    
    //let simplepie handle the content type (atom, RSS...)
    $feed->handle_content_type();

	 if ($feed->error):
	 	return FALSE;
	endif;
  
	foreach ($feed->get_items() as $item):

		global $showSource;
		
		$id = uniqid('', TRUE);
		$title = $item->get_title();
		$content = $item->get_content();
		$content = mysqli_real_escape_string($mysqli,$content);
		if ($showSource == TRUE) {
			$content .= '<br /><a href="'.$item->get_permalink().'">'.Source.'</a>';
		}
		//$feed = $item->get_feed();
		$time = $item->get_date('U');
		$query = "INSERT INTO `posts` (`id`, `title`, `post`, `time`, `category`) VALUES ('$id', '$title', '$content', $time, '$category');";
    		
		if($mysqli->query($query)) {
			// Updated!
		} else {
			// Something went wrong!
		}	     
	 
			endforeach;

		}

}

function addPost ($title, $content, $category) {

	global $mysqli;
	$title = sanitiseString($title);
	$content = mysqli_real_escape_string($mysqli,$content);
	$category = preg_replace("/[^A-Za-z0-9 ]/", '', $category);
	$category = strtolower(sanitiseString($category));
	if (!$time) {
		$time = time();
	}
	$id = uniqid('', TRUE);
	$query = "INSERT INTO `posts` (`id`, `title`, `post`, `time`, `category`) VALUES ('$id', '$title', '$content', $time, '$category');";
	if($mysqli->query($query)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function editPost ($id, $title, $content, $category) {

	global $mysqli;
	$title = sanitiseString($title);
	$content = mysqli_real_escape_string($mysqli,$content);
	$category = preg_replace("/[^A-Za-z0-9 ]/", '', $category);
	$category = strtolower(sanitiseString($category));
	$query = "UPDATE `posts` SET title='$title', post='$content', category='$category' WHERE id='$id'";
	if($mysqli->query($query)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function postTitle($id) {

	global $mysqli;
	$id = sanitiseString($id);
	$query = "SELECT `title` FROM `posts` WHERE id='".$id."'";
	if(!$result = $mysqli->query($query)) {
		return FALSE;
	}

	while($row = $result->fetch_assoc()){
		return $row['title'];
	}
}

function postCategory($id) {

	global $mysqli;
	$id = sanitiseString($id);
	$query = "SELECT `category` FROM `posts` WHERE id='".$id."'";
	if(!$result = $mysqli->query($query)) {
		return FALSE;
	}

	while($row = $result->fetch_assoc()){
		return $row['category'];
	}
}

function postContent($id) {

	global $mysqli;
	$id = sanitiseString($id);
	$query = "SELECT `post` FROM `posts` WHERE id='".$id."'";
	if(!$result = $mysqli->query($query)) {
		return FALSE;
	}
	while($row = $result->fetch_assoc()){
		return $row['post'];
	}
}

function deleteFeed ($id) {

	global $mysqli;
	$id = sanitiseString($id);
	$query = "DELETE FROM `feeds` WHERE id='".$id."'";
	if($mysqli->query($query)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function deletePost ($id) {

	global $mysqli;
	$id = sanitiseString($id);
	$query = "DELETE FROM `posts` WHERE id='".$id."'";
	if($mysqli->query($query)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function feedURL($id) {

	global $mysqli;
	$id = sanitiseString($id);
	$query = "SELECT `url` FROM `feeds` WHERE id='".$id."'";
	if(!$result = $mysqli->query($query)) {
		return FALSE;
	}

	while($row = $result->fetch_assoc()){
		return $row['url'];
	}
}

function feedCategory($id) {

	global $mysqli;
	$id = sanitiseString($id);
	$query = "SELECT `category` FROM `feeds` WHERE id='".$id."'";
	if(!$result = $mysqli->query($query)) {
		return FALSE;
	}

	while($row = $result->fetch_assoc()){
		return $row['category'];
	}
}