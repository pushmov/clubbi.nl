<?php
define('includeCheck','TRUE'); // Allow other files to be included

header('Content-Type: application/xml');

include_once('config.php'); // Config
include_once('includes/php/connect.php'); // Connect
include_once('includes/php/functions.php'); // Functions

$indexurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$indexurl = preg_replace('/rss.php$/', '', $indexurl);
//echo $indexurl;

$rssfeed = '<?xml version="1.0" encoding="ISO-8859-1"?>';
$rssfeed .= '<rss version="2.0">';
$rssfeed .= '<channel>';
$rssfeed .= '<title>'.$sitename.'</title>';
$rssfeed .= '<link>'.$indexurl.'</link>';
$rssfeed .= '<description></description>';
$rssfeed .= '<language>en-us</language>';
$rssfeed .= '<copyright>Copyright (C) '.date("Y").' '.$sitename.'</copyright>';
$sql = <<<SQL
    SELECT *
    FROM `posts`
	LIMIT 20
SQL;

if(!$result = $mysqli->query($sql)){
    die();
}

while($row = $result->fetch_assoc()){
    //echo $row['username'] . '<br />';
	$rssfeed .= '<item>';
	$rssfeed .= '<title>'.$row['title'].'</title>';
	$rssfeed .= '<description>' . $row['content'] . '</description>';
	$rssfeed .= '<link>'.$indexurl.'?id='.$row['id'] . '</link>';
	$rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", $row['time']) . '</pubDate>';
	$rssfeed .= '</item>';
}

	$rssfeed .= '</channel>';
	$rssfeed .= '</rss>';
 
    echo $rssfeed;
?>