<?php
if(!defined('includeCheck')){ die(); }

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8">

<title><?php echo $sitename; ?> - Admin Panel</title>


<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap-responsive.min.css"></link>
<style>
.container-fluid {
	max-width: 940px;
	margin: 0 auto;
}
</style>
</head>
<body>
<?php include_once('header.php'); ?>

<div class="container">
	<div class="">
		<h1 style="font-size:58px">Welcome <small>home.</small></h1>
		<div class="row">
			<div class="span4">
				<a href="index.php?p=feeds" class="btn btn-large btn-block btn-primary">Manage feeds.</a>
				<br />
			</div>
			<div class="span4">
				<a href="index.php?p=posts" class="btn btn-large btn-block btn-primary">Manage posts.</a>
				<br />
			</div>
			<div class="span4">
				<a href="index.php?p=edit" class="btn btn-large btn-block btn-primary">Create a new post.</a>
				<br />
			</div>
		</div>
		</div>
	</div>

<script src="../includes/jquery.js"></script>
<script src="../includes/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
