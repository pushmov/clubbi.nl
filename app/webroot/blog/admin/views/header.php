<?php if(!defined('includeCheck')){ die(); } ?>
<div class="navbar">
  <div class="navbar-inner">
    <div class="container">
 
      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
 
      <!-- Be sure to leave the brand out there if you want it shown -->
      <a class="brand" href="index.php"><?php echo $sitename; ?> Admin</a>
 
      <!-- Everything you want hidden at 940px or less, place within here -->
      <div class="nav-collapse collapse">
        <!-- .nav, .navbar-search, .navbar-form, etc -->
        <ul class="nav">
        	<li <?php if (strlen($p)<1) { ?>class="active"<?php } ?>><a href="index.php">Home</a></li>
        	<li <?php if ($p == "posts") { ?>class="active"<?php } ?>><a href="index.php?p=posts">Posts</a></li>
        	<li <?php if (($p == "edit") && (!isset($_GET['id']))) { ?>class="active"<?php } ?>><a href="index.php?p=edit">New</a></li>
        	<li <?php if ($p == "feeds") { ?>class="active"<?php } ?>><a href="index.php?p=feeds">Feeds</a></li>
        	<li><a href="index.php?p=logout">Logout</a></li>
        </ul>
      </div>
 
    </div>
  </div>
</div>