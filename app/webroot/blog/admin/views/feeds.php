<?php
if(!defined('includeCheck')){ die(); }

if (empty($_GET['action'])) {
	$_GET['action'] = "";
}

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$feed = new getRSS();
	if ($feed->addFeed($_POST['feedURL'],$_POST['category']) == FALSE) {
		$error = TRUE;
	}
}

if ($_GET['action'] == 'delete') {
	if (deleteFeed ($_GET['id']) == TRUE) {
		header('Location: index.php?p=feeds');
	}
}

if ($_GET['action'] == 'edit') {
	$url = feedURL($_GET['id']);
	$category = feedCategory($_GET['id']);
	deleteFeed ($_GET['id']);
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8">

<title><?php echo $sitename; ?> - Admin Panel</title>


<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap-responsive.min.css"></link>
<style>
.container-fluid {
	max-width: 940px;
	margin: 0 auto;
}
</style>
</head>
<body>
<?php include_once('header.php'); ?>

<div class="container">
<h1 style="font-size:58px">Manage <small>feeds.</small> <a href="./index.php?p=update" class="btn btn-success btn-large">Update All</a></h1>
<?php if ($error == TRUE) { ?>
<div class="alert alert-error">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Error!</strong> Please try again, make sure it's a valid feed and make sure you type in a category.
</div>
<?php } ?>
<form action="" method="post">
<div class="form-actions">
	<label>URL</label>
	<input name="feedURL" class="input-block-level" type="text" placeholder="Add a feed" value="<?php if (strlen($url)>0) {echo $url;} else {echo 'http://';} ?>">
	<label>Category</label>
	<input name="category" class="" type="text" placeholder="Category" <?php if (strlen($category)>0) {echo 'value="'.$category.'"';}?>>
	<br />
  <input type="submit" value="<?php if (strlen($url)>0) {echo 'Edit Feed';} else {echo 'Add Feed';} ?>" class="btn btn-primary" />
</div>
</form>

<table class="table table-condensed table-hover">
  <thead>
    <tr>
      <th class="span7">URL</th>
      <th class="span2">Category</th>
      <th class="span4">Last updated</th>
      <th class="span1">Update</th>
      <th class="span1">Edit</th>
      <th class="span1">Delete</th>
    </tr>
  </thead>
  <tbody>
    	<?php
		//lets not use your normal SELECT * FROM table query...just to show it still works
		$q = "SELECT * FROM  `feeds`";
		$paginate = new Paginate($mysqli, $q);
		$r = $paginate->get_results();
		if (@is_object($r)) {
			while ($message = $r->fetch_object()) {
			    //Echo out your page of info
			    //print_r($message);
		    ?>
    <tr>
    <?php 
    if ($message->lastUpdated>0) {
    	$dateMessage = date("Y/m/d H:i:s",$message->lastUpdated);
    } else {
    	$dateMessage = "Never updated.";
    }
    ?>
		<td><?php echo $message->url; ?></td>
		<td><?php echo $message->category; ?></td>
		<td><?php echo $dateMessage; ?></td>
    	<td><a class="btn btn-mini btn-inverse" href="index.php?p=update&feed=<?php echo $message->id; ?>"><i class="icon-refresh icon-white"></i></a></td>
    	<td><a class="btn btn-mini btn-inverse" href="index.php?p=feeds&action=edit&id=<?php echo $message->id; ?>"><i class="icon-edit icon-white"></i></a></td>
    	<td><a class="btn btn-mini btn-inverse" href="index.php?p=feeds&action=delete&id=<?php echo $message->id; ?>"><i class="icon-trash icon-white"></i></a></td>
    </tr>
<?php
		   
			} 
		}
?>
  </tbody>
</table>
<div class="pagination pagination-centered">
  <ul>
<?php 
echo $paginate->show_pages();  
?>
  </ul>
</div><!-- End pagination -->
</div>

<script src="../includes/jquery.js"></script>
<script src="../includes/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
