<?php
if(!defined('includeCheck')){ die(); }

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (isset($_GET['id'])) {
		editPost($_GET['id'], $_POST['title'], $_POST['content'], $_POST['category']);	
	} else {
		if (addPost ($_POST['title'], $_POST['content'], $_POST['category'], time()) == TRUE) {
			header('Location: index.php?p=posts');
			die();
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8">

<title><?php
if (isset($_GET['id'])) {
	echo "Edit";	
} else {
	echo "Add";
}
echo " - ";
echo $sitename;
?></title>


<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap-responsive.min.css"></link>
<link rel="stylesheet" type="text/css" href="../includes/bootstrap-wysihtml5/bootstrap-wysihtml5.css"></link>
<style>
textarea {
	width: 100%;
	//max-width: 940px;
	height: 200px;
	max-height: 100%;
}

.input-xxlarge {
	width: 100%;
}
.container-fluid {
//	width: 100%;
	max-width: 940px;
	margin: 0 auto;
}
</style>
</head>
<body>
<?php include_once('header.php'); ?>

<div class="container-fluid">
	<div class="">
		<h1 style="font-size:58px"><?php
if (isset($_GET['id'])) {
	echo "Edit";	
} else {
	echo "Add";
}?> <small>a post.</small></h1>
		<hr />
		<form method="post" action="">
		<p>Title:</p>
		<input class="input-xxlarge" name="title" type="text" <?php
if (isset($_GET['id'])) {
	echo 'value="'.postTitle($_GET['id']).'"';
} else {
	echo 'placeholder="Title"';
} ?> maxlength="255" autocomplete="off">
		<hr />
		<textarea class="textarea" name="content"<?php
if (!isset($_GET['id'])) {
	echo 'placeholder="Enter text ..."';
}?>><?php
if (isset($_GET['id'])) {
	echo postContent($_GET['id']);
}
?></textarea>
		<hr />
		<p>Category:</p>
		<input type="text" name="category" <?php
if (isset($_GET['id'])) {
	echo 'value="'.postCategory($_GET['id']).'"';
} else {
	echo 'placeholder="Category"';
} ?> maxlength="255" autocomplete="off">
		<hr />
		<input type="submit" name="submit" class="btn btn-large btn-primary" value="Submit" />
		</form>
	</div>

<script src="../includes/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script src="../includes/jquery.js"></script>
<script src="../includes/bootstrap/js/bootstrap.min.js"></script>
<script src="../includes/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<script>
	$('.textarea').wysihtml5();
</script>

</body>
</html>
