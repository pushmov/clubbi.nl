<?php
if(!defined('includeCheck')){ die(); }

if (empty($_GET['action'])) {
	$_GET['action'] = "";
	
}

if ($_GET['action'] == 'delete') {
	if (deletePost ($_GET['id']) == TRUE) {
		header('Location: index.php?p=posts');
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8">

<title><?php echo $sitename; ?> - Admin Panel</title>


<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap-responsive.min.css"></link>
<style>
.container-fluid {
	max-width: 940px;
	margin: 0 auto;
}
</style>
</head>
<body>
<?php include_once('header.php'); ?>

<div class="container">
<h1 style="font-size:58px">Edit <small>posts.</small></h1>
<table class="table table-condensed table-hover">
  <thead>
    <tr>
      <th class="span8">Name</th>
      <th class="span3">Category</th>
      <th class="span3">Last updated</th>
      <th class="span1">Edit</th>
      <th class="span1">Delete</th>
    </tr>
  </thead>
  <tbody>
    	<?php
		//lets not use your normal SELECT * FROM table query...just to show it still works
		$q = "SELECT * FROM  `posts` ORDER BY time DESC";
		$paginate = new Paginate($mysqli, $q);
		$r = $paginate->get_results();
		if (@is_object($r)) {
			while ($message = $r->fetch_object()) {
			    //Echo out your page of info
			    //print_r($message);
		    ?>
    <tr>
		<td><?php echo $message->title; ?></td>
		<td><?php echo $message->category; ?></td>
		<td><?php echo date("Y/m/d H:i:s",$message->time); ?></td>
    	<td><a class="btn btn-mini btn-inverse" href="index.php?p=edit&id=<?php echo $message->id; ?>"><i class="icon-edit icon-white"></i></a></td>
    	<td><a class="btn btn-mini btn-inverse" href="index.php?p=posts&action=delete&id=<?php echo $message->id; ?>"><i class="icon-trash icon-white"></i></a></td>
    </tr>
<?php
		   
			} 
		}
?>
  </tbody>
</table>
<div class="pagination pagination-centered">
  <ul>
<?php 
echo $paginate->show_pages();  
?>
  </ul>
</div><!-- End pagination -->
</div>

<script src="../includes/jquery.js"></script>
<script src="../includes/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
