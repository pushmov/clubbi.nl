<?php
if(!defined('includeCheck')){ die(); }
if (isset($_GET['feed'])) {
	$feed = sanitiseString($_GET['feed']);
} else {
	$feed = "";
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Updating &middot; <?php echo $sitename; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css" href="../includes/bootstrap/css/bootstrap-responsive.min.css"></link>
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .box {
        max-width: 500px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .box {
        margin-bottom: 10px;
      }
      
      img {
	      max-width: 100%;
	      max-height: auto;
      }

	iframe.hidden {
		display:none
	}

    </style>
    <meta http-equiv="refresh" content="30; url=./index.php?p=feeds">
  </head>

  <body>

    <div class="container box">

      <center><h2>Updating Feeds</h2></center>
      <div id="loaderIcon"><center><img width="100" height="100" src="../includes/img/loader.gif"></center></div>

<hr />
      <p class="small"><a href="../">Home</a></p>
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="../includes/jquery.js"></script>
	<script src="../includes/bootstrap/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
	<?php
	if (strlen($feed)>0) {
	?>
		var feed = encodeURI('<?php echo $feed;?>');
	<?php } else {?>
		var feed = "";
	<?php } ?>
		var url = "../updatediscrete.php?feed="+feed;
    function updateFeed()
    {
        $.get(url, function(data){
	        window.location.replace("./index.php?p=feeds");
        });
    };
updateFeed();
});
</script>
<iframe src="../updatediscrete.php?feed=<?php echo urlencode($feed); ?>" width="0" height="0" tabindex="-1" title="empty" class="hidden">
  </body>
</html>
