<?php
define('includeCheck','TRUE'); // Allow other files to be included

include_once('../config.php'); // Config
include_once('../includes/php/connect.php'); // Connect
include_once('../includes/php/functions.php'); // Functions
require_once('../includes/php/paginate.class.php'); // Paginate

session_start();

if (isset($_GET['p'])) {
	$p = $_GET['p'];
} else {
	$p = "";
}

if ($_SESSION['adminSignedIn'] != TRUE) {
	include_once('../admin/views/login.php');	
} else if ($p == "") {
	include_once("../admin/views/index.php");
} else if ($p == "edit") {
	include_once("../admin/views/edit.php");
} else if ($p == "feeds") {
	include_once("../admin/views/feeds.php");
} else if ($p == "posts") {
	include_once("../admin/views/posts.php");
} else if ($p == "update") {
	include_once("../admin/views/update.php");
} else if ($p == "logout") {
	$_SESSION['adminSignedIn'] = FALSE;
	session_destroy();
	header('Location: ../');
	echo '<meta http-equiv="refresh" content="0; url=../">';
} else {
	header("HTTP/1.0 404 Not Found");
	die('404. Page not found.');
}