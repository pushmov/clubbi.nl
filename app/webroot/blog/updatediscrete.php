<?php

// Keep loading after user leaves page.
@ignore_user_abort(true);
// Set time limit to 5 minutes. 
@set_time_limit(300);

define('includeCheck','TRUE'); // Allow other files to be included

include_once('config.php'); // Config
include_once('includes/php/connect.php'); // Connect
include_once('includes/php/functions.php'); // Functions

$feed = sanitiseString($_GET['feed']);

$rss = new getRSS();

if (strlen($feed)>0) {
	$rss->updateRSSFeed($feed);
} else {
	$rss->updateAllRSSFeeds();
}
?>