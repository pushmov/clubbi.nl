<?php
/*
	This is an old version of index.php that uses Bootstrap 2 instead of Bootstrap 3.
*/
define('includeCheck','TRUE'); // Allow other files to be included

include_once('config.php'); // Config
include_once('includes/php/connect.php'); // Connect
include_once('includes/php/functions.php'); // Functions
require_once('includes/php/paginate.class.php'); // Paginate

if (isset($_GET['category'])) {
	$query2 = "WHERE category = '".sanitiseString($_GET['category'])."'";
} else if (isset($_GET['id'])) {
	$query2 = "WHERE id = '".sanitiseString($_GET['id'])."'";
} else if (isset($_GET['search'])) {
	$query2 = "WHERE MATCH (title, post) 
AGAINST ('".sanitiseString($_GET['search'])."' IN BOOLEAN MODE)";
} else {
	$home = TRUE;
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Home &middot; <?php echo $sitename; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="rss.php" />

	
    <!-- Le styles -->
    <link href="./includes/bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 20px;
        padding-bottom: 40px;
      }

      /* Custom container */
      .container-narrow {
        margin: 0 auto;
        max-width: 700px;
      }
      .container-narrow > hr {
        margin: 30px 0;
      }

      /* Main marketing message and sign up button */
      .jumbotron {
        margin: 60px 0;
        text-align: center;
      }
      .jumbotron h1 {
        font-size: 72px;
        line-height: 1;
      }
      .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
      }

      /* Supporting marketing content */
      .marketing {
        margin: 60px 0;
      }
      .marketing p + h4 {
        margin-top: 28px;
      }
      
      /* Make label link white. */
	
		.label a, .label a:hover, .label a:active, .label a:visited { color: white; }
    </style>
    <link href="./includes/bootstrap/bootstrap-responsive.css" rel="stylesheet">
  </head>

  <body>

    <div class="container-narrow">

      <div class="masthead">
        <div class="input-append pull-right">
        <form action="index.php" method="GET">
  <input class="span2" id="appendedInputButtons" type="text" name="search" value="<?php echo htmlspecialchars($_GET['search']); ?>">
  <input type="submit" class="btn btn-primary" value="Search">
	</form>
</div>
        <ul class="nav nav-pills pull-right">
          <li <?php if ($home == TRUE) { ?>class="active"<?php } ?>><a href="./">Home</a></li>
          <li><a href="rss.php">RSS</a></li>
        </ul>
        <h3 class="muted"><?php echo $sitename; ?></h3>
      </div>

<div class="">
    	<?php
		//lets not use your normal SELECT * FROM table query...just to show it still works
		$q = "SELECT * FROM  `posts` ".$query2." ORDER BY time DESC ";
		$paginate = new Paginate($mysqli, $q);
		$r = $paginate->get_results();
		while ($message = $r->fetch_object()) {
		    //Echo out your page of info
		    //print_r($message);
		    ?>
		<hr />
		<?php
		if (isset($_GET['id'])) {
		?>
		<?php
		if (empty($_GET['t'])) {
			echo '<meta http-equiv="refresh" content="0;url=./?id='.$message->id.'&t='.urlencode($message->title).'">';
		}
		?>
		<h1><?php echo $message->title; ?></h1>
		<?php
		} else {
		?>
		<h1><a href="./?id=<?php echo $message->id; ?>&t=<?php echo urlencode($message->title); ?>"><?php echo $message->title; ?></a></h1>
		<?php 
		} ?>
		<p><?php echo $message->post; ?></p>
	    <div>
	        <span class="badge badge-success">Posted <?php echo date("Y/m/d H:i:s",$message->time); ?></span>
	        <div class="pull-right"><span class="label"><a href="./?category=<?php echo $message->category; ?>"><?php echo $message->category; ?></a></span></div>
	    </div>    

<?php
		   
		} 
?>
<?php if (isset($_GET['id'])) { ?>
<!-- Insert comment code below this line. -->
<!-- begin htmlcommentbox.com -->
 <div id="HCB_comment_box"><a href="http://www.htmlcommentbox.com">HTML Comment Box</a> is loading comments...</div>
 <link rel="stylesheet" type="text/css" href="//www.htmlcommentbox.com/static/skins/bootstrap/twitter-bootstrap.css?v=0" />
 <script type="text/javascript" id="hcb"> /*<!--*/ if(!window.hcb_user){hcb_user={};} (function(){var s=document.createElement("script"), l=(""+window.location || hcb_user.PAGE), h="//www.htmlcommentbox.com";s.setAttribute("type","text/javascript");s.setAttribute("src", h+"/jread?page="+encodeURIComponent(l).replace("+","%2B")+"&opts=16862&num=10");if (typeof s!="undefined") document.getElementsByTagName("head")[0].appendChild(s);})(); /*-->*/ </script>
<!-- end htmlcommentbox.com -->
<!-- Insert comment code above this line. -->
<?php } else { ?>
<hr />
<div class="pagination pagination-centered pagination-large">
  <ul>
<?php 
echo $paginate->show_pages();  
?>
  </ul>
</div><!-- End pagination -->
    
<?php } ?>
</div>
<hr />
      <div class="footer">
        <p>&copy; <?php echo $sitename.' '.date("Y"); ?></p>
      </div>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="./includes/jquery.js"></script>
<script src="./includes/bootstrap/js/bootstrap.min.js"></script>

<?php 
$rand = rand(1,5);
if ($rand == 1) {
?>
<script>
$(document).ready(function(){
		var url = "./updatediscrete.php";
    function updateFeed()
    {
        $.get(url, function(data){
	        // Complete loading.
        });
    };
updateFeed();
});
</script>
<iframe src="./updatediscrete.php" width="0" height="0" tabindex="-1" title="empty" class="hidden">
<?php
}
?>

  </body>
</html>