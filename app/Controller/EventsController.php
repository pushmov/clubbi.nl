<?php
/**
 * @copyright Copyright 2008 Evento
 */

App::uses('CakeEmail', 'Network/Email');
$dirname = dirname(dirname(__FILE__));
require_once("$dirname/vendors/Facebook/facebook.php");

class EventsController extends AppController {

	var $name = 'Events';
	var $uses = array('Event', 'Tag', 'EventsTag', 'Country', 'City', 'User', 'Settings', 'Category', 'Participant', 'Roll', 'Credit');
	var $helpers = array('Html', 'Form', 'Calendar', 'Time', 'Text', 'Timeformat',
		'Bookmark', 'Js');
	var $components = array('RequestHandler', 'Calendar');
	var $paginate = array('limit' => 100, 'order' => array('Event.start_date' => 'ASC',
		'Event.id'=>'DESC'));

	/**
	 * Overload beforeFilter to set some permissions
	 */

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index','view', 'search', 'ical', 'ajaxSubmit', 'festivals','giveaway', 'techno', 'house', 'hardstyle', 'urban', 'trance');
	}
	

	/**
	* display a list of events depending on city, tag or date.
	*
	* @param string country
	* @param string $city
	* @param string $venue
	* @param string $category
	* @param string $tag
	* @param int $year
	* @param string $month
	* @param int $day
	**/

	public function index($country='all-countries', $city='all-cities', $venue = 'all-venues',
		$category='all-categories', $tag='all-tags', $year = null, $month = null,$day = null) {

		$feed = 'events';
		$data = null;
		$cityId = null;
		$venueId = null;
		$categoryId = null;
		$countryId = null;
		$tagEvents = null;

		/*
		 * set basic conditions to query events.
		 * we don't want to show events marked as unpublished
		 */

		$conditions = array();
		$conditions['Event.published'] = 1;
		/*
		 *	if country is set and is valid set variables and conditions to query events
		 */

		if($country!='all-countries') {
			$currentCountry = $this->Country->find('first', array('conditions'=>array('Country.slug'=>$country)),
				array('Country.name', 'Country.id'), null, -1);
			if(empty($currentCountry)) throw new NotFoundException();
			$countryId = $currentCountry['Country']['id'];
			$this->set('country_name', $currentCountry['Country']['name']);
			$conditions['Country.id'] = $countryId;

			// set title for events in this country
			$this->set('title_for_layout', sprintf(__('Events in %s'),
				__d('countries', $currentCountry['Country']['name'])));

			/*
			 * if a city is set in the url get it from the database
			 * set basic variables and conditions for events query.
			 */

			if($city!='all-cities') {
				$cityConditions['City.slug'] = $city;
				if($countryId) $cityConditions['City.country_id'] = $countryId;
				$currentCity = $this->City->find('first', array('conditions'=>array($cityConditions),
				'fields'=>array('City.id', 'City.name')));
				if($currentCity) {
					$cityId = $currentCity['City']['id'];
					$feed = $city;
					$conditions['Venue.city_id'] = $cityId;
					$this->set('city_name', $currentCity['City']['name']);

					// set title for events in this city
					$this->set('title_for_layout', sprintf(__('Events in %s'),
						$currentCity['City']['name'] . ', ' . __d('countries', $currentCountry['Country']['name'])));

					/*
					 * if a venue is set in the url get it from the database
					 * to set variables and conditions for the events query
					 */
					if($venue!='all-venues') {
						$venueConditions['Venue.slug'] = $venue;
						$currentVenue = $this->Event->Venue->find('first', array('conditions'=>$venueConditions,
							'fields'=>array('Venue.id', 'Venue.name')));
						if(!empty($currentVenue)) {
							$venueId = $currentVenue['Venue']['id'];
							$conditions['Venue.id'] = $venueId;
							$this->set('venue_name', $currentVenue['Venue']['name']);
						}
						else throw new NotFoundException();
					}
				}
				else throw new NotFoundException();
			}
		}
		else {
			if($city!='all-cities' || $venue!='all-venues') throw new NotFoundException();;
		}

		/*
		 * if category is set check if it exists and set variables and conditions for the events query
		 */

		if(($categories = Cache::read('evento_categories')) === false) {
			$categories = $this->Category->find('all', array('order'=>'name ASC'));
			Cache::write('evento_categories', $categories);
		}

		if($category!='all-categories') {
			$currentCategory = Set::extract('/Category[slug='. $category .']', $categories);
			if(empty($currentCategory)) throw new NotFoundException();
			$currentCategory = array_shift($currentCategory);
			$categoryId = $currentCategory['Category']['id'];
			$conditions['Event.category_id'] = $categoryId;
			$this->set('category_name', $currentCategory['Category']['name']);

			// set title for layout
			if(isset($currentCountry['Country']['name'])) {
				$countryTitle = __d('countries', $currentCountry['Country']['name']);
				if(isset($currentCity['City']['name'])) $countryTitle = $currentCity['City']['name']  . ', '
				. $countryTitle;

				$this->set('title_for_layout', sprintf(__('%1$s events in %2$s'),
					$currentCategory['Category']['name'], $countryTitle));
			}
			else {
				$this->set('title_for_layout', sprintf(__('%s events'), $currentCategory['Category']['name']));
			}
		}

		/*
		 * if a tag is set in the url look for it in the database and set basic variables
		 * and conditions to query events
		 */

		if($tag!='all-tags') {
			$currentTag = $this->Tag->find('first', array('conditions'=>array('Tag.slug'=>$tag)));
			if(empty($currentTag)) throw new NotFoundException();
			$conditions['Event.id'] = $this->Tag->getTagEventIds($tag);
			if(!$conditions['Event.id']) throw new NotFoundException();
			$tagEvents = $conditions['Event.id'];
			$this->set('tag_name', $currentTag['Tag']['name']);
		}

		/*
		 * if year is not null check if it is a valid year and trowh error 404 if it isn't
		 */

		if($year!==null) {
			if(!($year = $this->Calendar->getYear($year))) throw new NotFoundException();
			else {
				$conditions['YEAR(end_date) >='] = $year;
				$conditions['YEAR(start_date) <='] = $year;
			}

			/*
			 * if month is not null check if it is valid and set conditions to query events
			 */
			if($month!==null) {
				if(!($monthNum = $this->Calendar->getMonth($month))) throw new NotFoundException();

				/*
				 * if day is set get conditions to query for events
				 */
				if($day!==null) {
					if(!($dayConditions = $this->Calendar->getDayConditions($day))) throw new NotFoundException();
					else {
						$conditions = array_merge($conditions, $dayConditions);

						// set title for layout
						$monthTitle =  __d('cake', date('F', strtotime($day .'-'.$monthNum.'-'.$year)));
						$dayTitle = date('j', strtotime($day .'-'.$monthNum.'-'.$year));

						if(isset($currentCountry['Country']['name'])) {
							$countryTitle = __d('countries', $currentCountry['Country']['name']);
							if(isset($currentCity['City']['name'])) {
								$countryTitle = $currentCity['City']['name']
								. ', ' . $countryTitle;
							}
							if(isset($currentCategory['Category']['name'])) {
								$this->set('title_for_layout', sprintf(__('%1$s events in %2$s on %3$s %4$s'),
									$currentCategory['Category']['name'], $countryTitle, $monthTitle, $dayTitle));
							}
							else {
								$this->set('title_for_layout', sprintf(__('events in %1$s on %2$s %3$s'),
								$countryTitle, $monthTitle, $dayTitle));
							}
						}
						else {
							if(isset($currentCategory['Category']['name'])) {
								$this->set('title_for_layout', sprintf(__('%1$s events on %2$s %3$s'),
								$currentCategory['Category']['name'], $monthTitle, $dayTitle));
							}
							else {
								$this->set('title_for_layout', sprintf(__('events on %1$s %2$s'), $monthTitle, $dayTitle));
							}
						}
					}
				}
				else {
					$lastDay = date('t', strtotime($year . '-' . sprintf('%02d', $monthNum)));
					$conditions['OR'] = array(
						'DATE_FORMAT(end_date, "%Y-%m")' => $year . '-' . sprintf('%02d', $monthNum),
						'DATE_FORMAT(start_date, "%Y-%m")' => $year . '-' . sprintf('%02d', $monthNum),
						'AND' => array(
							'start_date <=' =>
								date('Y-m-d', strtotime($year . '-' . sprintf('%02d', $monthNum) . '-01')),
							'end_date >=' =>
								date('Y-m-d', strtotime($year . '-' . sprintf('%02d', $monthNum) . '-' . $lastDay))
						)
					);
				}
			}
		}
		else {
			$conditions['end_date >='] = date('Y-m-d H:i');
		}

		/*
		 * paginate events using according to conditions
		 */
		$this->paginate['contain'] = array('Tag');
		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.lineup', 'Event.start_date',
			'Event.notes', 'Event.promoted', 'Event.ticketswap', 'Event.logo', 'Event.ticket', 'Event.win_guestlisttime', 'Event.win_price_tickets', 'Event.win_price_guestlist', 'Event.win', 'Event.presale', 'Event.doorsale', 'Event.facebook', 'Venue.name', 'Venue.slug', 'City.name', 'City.slug',
			'Country.name', 'Country.slug', 'Category.name',
			'Category.slug');

			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = Venue.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				),
				array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'left',
					'conditions' => array('Category.id = Event.category_id')
				)
			);

		if(($promoted = Cache::read('evento_promoted')) === false) {
			$this->Event->contain(array('Tag'));
			$promoted = $this->Event->find('all',
				array('conditions'=>array('Event.promoted'=>1, 'Event.published'=>1),
				'joins' => $this->paginate['joins'], 'fields' => $this->paginate['fields']));
			Cache::write('evento_promoted', $promoted);
		}

		if(Configure::read('evento_settings.city_name')) {
			$activeCities = false;
		}
		elseif(($activeCities = Cache::read('evento_active_cities')) === false) {
			$activeCities = $this->City->find('active');
			Cache::write('evento_active_cities', $activeCities);
		}

		if($categoryId) $conditions['category_id'] = $categoryId;
		if($cityId) $conditions['Venue.city_id'] = $cityId;
		$toptags = $this->Tag->find('top', array('conditions'=>$conditions, 'limit'=>30));
		$this->paginate['order'] = 'date(Event.start_date) ASC, City.name ASC';

		$this->set('events',$this->paginate('Event', $conditions));

		$this->set('weekStart', Configure::read('evento_settings.weekStart'));
		$this->set('feed',$feed);
		$this->set('year', $year);
		$this->set('month', $month);
		$this->set('day', $day);
		$this->set('city',$city);
		$this->set('venue', $venue);
		$this->set('country', $country);
		$this->set('current_tag',$tag);
		$this->set('category', $category);
		$this->set('data', $this->Calendar->getCalendarData($year, $month,
			$countryId, $cityId, $categoryId, $tagEvents));
		$this->set('countries', $activeCities);
		$this->set('toptags', $toptags);
		$this->set('categories', $categories);
		$this->set('promoted', $promoted);


	}

	/**
	 * Display an Event with all it's data.
	 *
	 * @access public
	 * @param string $city
	 * @param string $slug
	 */

	public function view($country = null, $city = null, $venue = null, $slug = null) {
		if(!$country || !$city || !$venue || !$slug) throw new NotFoundException();
		if(!($event = $this->Event->viewEvent($slug))) throw new NotFoundException();
		if(!$event['Event']['published'] && !$this->Auth->user('admin')) {
			throw new NotFoundException();
		}

		$this->set('title_for_layout',
			h($event['Event']['name'].'. '.' '.$event['Venue']['name'].', '.$event['City']['name']));
		$isAttendee = Set::extract('/Attendees/User[id='.$this->Auth->user('id').']/id',$event);

		if($event['Event']['repeat_parent']!=null) {
			$this->Event->contain(array('Venue'=>array('City'=>array('Country'))));
			$repeatEvents = $this->Event->find('all',
				array('conditions'=>array('Event.repeat_parent'=>$event['Event']['repeat_parent'],
				'Event.start_date >='=>date('Y-m-d'), 'Event.id not'=>$event['Event']['id'])));
			$this->set('repeatEvents', $repeatEvents);
		}
		$this->Event->Comment->recursive = -1;
		$recentComments = $this->Event->Comment->find('count',	array(
			'conditions'=>array('user_id'=>$this->Auth->user('id'),
			'created >='=>date('Y-m-d H:i', strtotime(date('Y-m-d H:i') . ' -5 minutes')))));
		$useRecaptcha = false;

		if(Configure::read('evento_settings.recaptchaPublicKey')
		&& Configure::read('evento_settings.recaptchaPrivateKey')
		&& ($recentComments >= 5)) {
			$recaptcha = $this->Components->load('Recaptcha.Recaptcha');
			$recaptcha->enabled = true;
			$recaptcha->initialize($this);
			$recaptcha->startup($this);
			$useRecaptcha = true;
		}

		$photoPagination = false;
		if(count($event['Photo']) > 20) {
			$event['Photo'] = array_slice($event['Photo'], 0, 20);
			$photoPagination = true;
		}
		$this->set('photoPagination', $photoPagination);
		$this->set('useRecaptcha', $useRecaptcha);
		$this->set('me', $this->Auth->user('id'));
		$this->set('event', $event);
		$this->set('isAttendee',(!$this->Auth->user('id')) ? FALSE : !empty($isAttendee));
		
		$isUpComing = FALSE;
		$isExpired = FALSE;
		$isWin = TRUE;
		
		if($event['Event']['win'] == '1') {
			$isUpComing = (time() >= strtotime($event['Event']['win_start_date'])) ? FALSE : TRUE;
			$isExpired  = (time() >= strtotime($event['Event']['win_end_date'])) ? TRUE : FALSE;
			
			if($isExpired) $isWin = FALSE;
			if($isUpComing) $isWin = FALSE;
		}
		else {
			$isWin = FALSE;
		}
		
		$this->set('isWin', $isWin);
		$this->set('winURL', $country .'/'. $city .'/'. $venue .'/'. $slug);
		
		//related events
		//related events parameters : same date and same city, otherwise same venue and otherwise same date
		$relatedMerged = array();
		$rel['date'] = $event['Event']['start_date'];
		
		$rel['venue'] = $event['Venue']['id'];
		$rel['city'] = $event['City']['id'];
		
		$joins = array(
			array(
				'table' => 'venues',
				'alias' => 'Venue',
				'type' => 'left',
				'conditions' => array('Venue.id = Event.venue_id')
			),
			array(
				'table' => 'cities',
				'alias' => 'City',
				'type' => 'left',
				'conditions' => array('City.id = Venue.city_id')
			),
			array(
				'table' => 'countries',
				'alias' => 'Country',
				'type' => 'left',
				'conditions' => array('Country.id = City.country_id')
			)
		);
		
		$fields = array('Event.*', 'City.*', 'Venue.*', 'Country.*');
		
		$relatedEvents = $this->Event->find('all', array('conditions'=>array(
			'Event.id <> ' => $event['Event']['id'],
			'Event.published' => '1',
			'City.id'=>$rel['city'], 
			'date(start_date) >= ' => date('Y-m-d', strtotime($rel['date']))
			// 'OR'=>array(
				// array('Event.venue_id' => $rel['venue'], 'date(start_date) > ' => date('Y-m-d', strtotime($rel['date']))),
				// array('Event.venue_id' => $rel['venue'], )
			// )
		), 
			'limit'=>6,
			'order'=>'Event.start_date ASC',
			'joins'=>$joins,
			'fields'=>$fields));
		
		$relatedMerged = $relatedEvents;
		$eventIds = array();
		
		if(sizeof($relatedMerged) < 6) {
			
			$eventIds = $this->__extractEventId($relatedMerged);
			
			$c = array(
				'Event.id <> ' => $event['Event']['id'],
				'Event.published' => '1',
				'OR'=>array(
					// array('Event.venue_id' => $rel['venue'], 'date(start_date) > ' => date('Y-m-d', strtotime($rel['date']))),
					array('City.id'=>$rel['city'], 'date(start_date) >= ' => date('Y-m-d'))
				)
			);
			
			if(!empty($eventIds))
			{
				if(sizeof($eventIds) == 1) 
					$c['Event.id <> ']=$eventIds[0];
				else
					$c['NOT']=array('Event.id IN'=>$eventIds);
			}
			
			$futureEvents = $this->Event->find('all', array('conditions'=>$c, 
				'limit'=>(6 - sizeof($relatedMerged)),
				'joins'=>$joins,
				'fields'=>$fields));
			
			$relatedMerged = array_merge($relatedMerged, $futureEvents);
		}
		
		if(sizeof($relatedMerged) < 6) {
			
			$eventIds = $this->__extractEventId($relatedMerged);
			
			$c = array(
				'Event.id <> ' => $event['Event']['id'],
				'Event.promoted' => '1',
				'Event.published' => '1',
				'date(start_date) >= ' => date('Y-m-d')
					//, strtotime($rel['date']))
				// 'date(end_date) < ' => date('Y-m-d')
			);
			
			if(!empty($eventIds))
			{
				if(sizeof($eventIds) == 1) 
					$c['Event.id <> ']=$eventIds[0];
				else
					$c['NOT']=array('Event.id IN'=>$eventIds);
			}
				
			$promotedEvent = $this->Event->find('all', array('conditions'=>$c, 
				'limit'=>(6 - sizeof($relatedMerged)),
				'order'=>array('Event.start_date ASC'),
				'joins'=>$joins,
				'fields'=>$fields));
			
			
			$relatedMerged = array_merge($relatedMerged, $promotedEvent);
		}
		
		$relatedIds = $this->__extractEventId($relatedMerged);
		
		$relatedEvents = array();
		
		if(!empty($relatedIds))
		{
			if(sizeof($relatedIds) == 1)
			{
				
				$relatedEvents = $this->Event->find('all', array('conditions'=>array('Event.id'=>$relatedIds[0]), 
					'limit'=>6,
					'order'=>array('Event.start_date ASC'),
					'joins'=>$joins,
					'fields'=>$fields)
				);
				
			} else {
				
				$relatedEvents = $this->Event->find('all', array('conditions'=>array('Event.id IN'=>$relatedIds), 
					'limit'=>6,
					'order'=>array('Event.start_date ASC'),
					'joins'=>$joins,
					'fields'=>$fields)
				);
			}
			
		}
		
		$this->set('relatedEvents', $relatedEvents);
	}
	
	private function __extractEventId($resources) {
		
		if(empty($resources))
			return array();
		
		$ret = array();
		foreach($resources as $row)	{
			$ret[] = $row['Event']['id'];
		}
		
		return $ret;
	}

	/**
	 * Add a new Event. If the Event's city and tags do not exist insert
	 * it all in the database and get a short url from bit.ly if enabled
	 *
	 * @access public
	 */

	public function add() {
		if(Configure::read('evento_settings.adminEvents')==1 && !$this->Auth->user('admin')) {
			throw new NotFoundException();
		}
		$this->set('title_for_layout', __('Add an event'));

		$recentEvents = $this->Event->find('count',	 array(
			'conditions'=>array('user_id'=>$this->Auth->user('id'),
			'Event.created >='=>date('Y-m-d H:i', strtotime(date('Y-m-d H:i') . ' -5 minutes'))),
			'recursive' => -1));
		$useRecaptcha = false;

		if(Configure::read('evento_settings.recaptchaPublicKey')
		&& Configure::read('evento_settings.recaptchaPrivateKey')
		&& ($recentEvents >= 5)) {
			$recaptcha = $this->Components->load('Recaptcha.Recaptcha');
			$recaptcha->enabled = true;
			$recaptcha->initialize($this);
			$recaptcha->startup($this);
			$useRecaptcha = true;
		}

		if (!empty($this->request->data)) {
			if(!$useRecaptcha || ($useRecaptcha && $recaptcha->verify())) {
				$this->request->data['Event']['recaptcha'] = 'correct';
			}
			$this->request->data['Event']['user_id'] = $this->Auth->user('id');
			if(Configure::read('evento_settings.moderateEvents')) {
				$this->request->data['Event']['published'] = 0;
			}
			else {
				$this->request->data['Event']['published'] = 1;
			}
			
			if($this->Event->save($this->request->data)) {
				$eventId = $this->Event->getInsertID();
				$this->Event->contain(array('Venue'=>array('City'=>array('Country'))));
				$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId)));
				$this->request->data['Event']['venue_id'] = $event['Event']['venue_id'];
				// if it is a repeat event create all needed entries
				if($this->request->data['Event']['repeat']!='does_not_repeat') {
					$event['Event']['repeat_parent'] = $eventId;
					$this->Event->save($event);
					$this->request->data['Event']['repeat_parent'] = $eventId;
					$this->request->data['Event']['logo'] = $event['Event']['logo'];
					$repeat = $this->Event->saveRepeat($this->request->data);
				}
				if(Configure::read('evento_settings.moderateEvents')) {
					$this->set('moderation', true);

					$cachedDate = Cache::read('evento_event_moderation');
					if(!$cachedDate || $cachedDate > date('Y-m-d')) {
						Cache::write('evento_event_moderation', date('Y-m-d'));
						$email = new CakeEmail();
						$email->from(Configure::read('evento_settings.systemEmail'));
						$email->to(Configure::read('evento_settings.adminEmail'));
						$email->subject(__('There are events awaiting moderation'));
						$email->template('event_moderation');
						$email->emailFormat('both');
						$email->send();
					}
					return $this->render();
				}
				else {
					$this->redirect(array('controller'=>'events','action'=>'view',
						$event['Venue']['City']['Country']['slug'],
						$event['Venue']['City']['slug'], $event['Venue']['slug'], $event['Event']['slug'] ));
				}
			}
			if(isset($this->request->data['Event']['venue_id'])
			&& $this->request->data['Event']['venue_id']) {
				$this->Event->Venue->contain(array('City'=>array('Country')));
				$venue = $this->Event->Venue->find('first',
					array('conditions'=>array('Venue.id'=>$this->request->data['Event']['venue_id'])));

				$this->request->data['Venue'] = $venue['Venue'];
				$this->request->data['Venue']['City'] = $venue['City'];
			}
			if(isset($this->request->data['Event']['end_date'])
				&& $this->request->data['Event']['start_date'] != $this->request->data['Event']['end_date']) {
					$this->set('end_date_checked', true);
			}
		}
		if(Configure::read('evento_settings.country_id')) {
			$this->set('country_id', Configure::read('evento_settings.country_id'));
			$this->set('country_name', $this->Country->field('name',
				array('Country.id'=>Configure::read('evento_settings.country_id'))));
		}
		else {
			if(($countryList = Cache::read('evento_countrylist')) === false) {
				$countryList = $this->Country->find('countrylist');
				Cache::write('evento_countrylist', $countryList);
			}
			$this->set('countries', $countryList);
		}

		if(Configure::read('evento_settings.city_name')) {
			$this->set('default_city', Configure::read('evento_settings.city_name'));
		}

		if(Configure::read('evento_settings.adminVenues')) {
			$this->set('venues', $this->Event->Venue->find('list'));
		}

		if(($categoryList = Cache::read('evento_categorylist')) === false) {
			$categoryList =	 $this->Category->find('list', array('fields'=>array('Category.name'),
				'order'=>'name'));
			Cache::write('evento_categorylist', $categoryList);
		}
		$this->set('useRecaptcha', $useRecaptcha);
		$this->set('categories', $categoryList);
	}

	/**
	 * Edit an existing event. Only the user who submited the event can edit it.
	 *
	 * @access public
	 * @param int $eventId
	 **/

	public function edit($eventId = null) {
		if(!$eventId && !($eventId = $this->request->data['Event']['id'])) throw new NotFoundException();
		$this->set('title_for_layout', __('edit event'));

		$this->Event->contain(array('Tag', 'Venue'=>array('City'=>array('Country'))));
		$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId,
			'Event.user_id'=>$this->Auth->user('id'), 'Event.published'=>true)));

		if(empty($event)) throw new NotFoundException();
		unset($this->request->data['Event']['published']);
		if($this->__editEvent($event)) {
			unset($event);
			$this->Event->contain(array('Venue'=>array('City'=>array('Country'))));
			$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId)));

			$this->redirect(array('controller'=>'events','action'=>'view',
				$event['Venue']['City']['Country']['slug'], $event['Venue']['City']['slug'],
				$event['Venue']['slug'], $event['Event']['slug'] ));
		}
	}

	/**
	 * add attendees to the event.
	 * throw 404 error if attendees is disabled in admin panel
	 *
	 * @access public
	 * @param int $eventId
	 * @param boolean $isGoing
	 */

	public function attendee($eventId = null, $isGoing = null) {
		if(Configure::read('evento_settings.disableAttendees')	== 1
		|| $eventId === null || $isGoing === null) throw new NotFoundException();
		$this->Event->contain(array('Venue'=>array('City'=>array('Country'))));
		$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId)));
		if(empty($event)) throw new NotFoundException();
		if($this->Event->attendee($eventId, $isGoing, $this->Auth->user('id'))) {
			$this->User->contain(array('City'=>array('Country')));
			$user = $this->User->find('first',
				array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
				
			if($event['Event']['facebook'] != '') {
				
				$status = ($isGoing == '1') ? "attending" : "declined";
				$this->__setRsvpEvent($status, $event['Event']['facebook']);
				
			}
			
			if($this->RequestHandler->isAjax()) {
				$this->response->type('json');
				$event['Event']['id'] = $eventId;
				$this->set('isAttendee', $isGoing);
				$this->set('event', $event);
				$this->set('attendee', $user);
				$this->layout = 'ajax';
				return;
			}
		}

		$this->redirect(array('controller'=>'events', 'action'=>'view',
			$event['Venue']['City']['Country']['slug'], $event['Venue']['City']['slug'],
			$event['Venue']['slug'], $event['Event']['slug']));
	}

	/**
	 * export event as ical file
	 *
	 * @param int $eventId
	 */

	public function ical($eventId = null) {
		if(!$eventId) throw new NotFoundException();
		$this->Event->contain(array('User', 'Category', 'Venue'=>array('City'=>array('Country'))));
		$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId)));
		if(empty($event)) throw new NotFoundException();
		$this->layout = 'ical';
		$this->set('event', $event);
		$this->response->header("Cache-Control: public");
		$this->response->header("Content-Description: File Transfer");
		$this->response->header('Content-Disposition', 'inline; filename=' . $event['Event']['slug'] . '.ics');
		$this->RequestHandler->respondAs('text/calendar');
		Configure::write('debug', 0);
	}

	/**
	 * search events looks at search term for a comma and if it does exist tryes to extract
	 * the city name from the string.
	 */

	public function search() {
		$data = $this->Session->read('Search.term');
		if(!empty($this->request->data) || !empty($data)) {
			if($this->request->data) {
				$this->Session->write('Search.term', $this->request->data);
			}
			else {
				$this->request->data = $data;
			}

			if(($n = strrpos($this->request->data['Search']['term'], ','))!==false) {
				$searchCity = trim(str_replace(',','',substr($this->request->data['Search']['term'], $n)));
				$searchTerm = trim(substr($this->request->data['Search']['term'], 0, $n));
				$city = $this->City->find('first', array('conditions'=>array('City.name'=>$searchCity)));
				if(!empty($city)) {
					$this->request->data['Search']['term'] = $searchTerm;
					$this->request->data['Search']['city_id'] = $city['City']['id'];
				}
			}

			$conditions = array(
				'or'=>array('Event.name like'=>'%'.$this->request->data['Search']['term'].'%',
					'Event.notes like '=> '%'.$this->request->data['Search']['term'].'%', 
					'Event.lineup like '=> '%'.$this->request->data['Search']['term'].'%',
					'Venue.name like '=> '%'.$this->request->data['Search']['term'].'%',
					'City.name like '=> '%'.$this->request->data['Search']['term'].'%'),
					'Event.start_date >='=>date('Y-m-d'),
				'Event.published' => true
			);

			if(isset($this->request->data['Search']['city_id'])
			&& $this->request->data['Search']['city_id']) {
				$conditions['Venue.city_id'] = $this->request->data['Search']['city_id'];
			}

			$this->paginate['recursive'] = -1;
			$this->paginate['order'] = 'Event.start_date ASC';
			$this->paginate['fields'] = array('Event.id', 'Event.win', 'Event.name', 'Event.slug', 'Event.start_date',
				'Event.notes', 'Event.logo', 'City.name', 'City.slug', 'User.username', 'User.slug',
				'User.photo', 'Country.name', 'Country.slug', 'Venue.slug', 'Venue.name');

			$this->paginate['joins'] = array(
					array(
						'table' => 'venues',
						'alias' => 'Venue',
						'type' => 'left',
						'conditions' => array('Venue.id = Event.venue_id')
					),
					array(
						'table' => 'cities',
						'alias' => 'City',
						'type' => 'left',
						'conditions' => array('City.id = Venue.city_id')
					),
					array(
						'table' => 'countries',
						'alias' => 'Country',
						'type' => 'left',
						'conditions' => array('Country.id = City.country_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'left',
						'conditions' => array('User.id = Event.user_id')
					));

			$this->set('events', $this->paginate('Event', $conditions));
			if(isset($searchCity) && isset($searchTerm)) {
				$this->request->data['Search']['term'] = $searchTerm . ', ' . $searchCity;
			}
		}
		if(($categories = Cache::read('evento_categories')) === false) {
			$categories = $this->Category->find('all');
			Cache::write('evento_categories', $categories);
		}
		$this->set('categories', $categories);
		$this->set('title_for_layout', __('Search'));
	}

	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////// ADMIN
	//////////////////////////////////////////////////////////////////////
	/**
	 * Giveaway page.
	 *
	 * @param int $eventId
	 */

public function admin_giveaway($eventId = null) {

		
		$conditions['Event.win'] = '1';
		$conditions['end_date >='] = date('Y-m-d H:i');
		$this->paginate['order'] = 'Event.win_end_date ASC';
		$this->paginate['recursive'] = -1;

		$this->paginate['joins'] = array(
			array(
				'table' => 'venues',
				'alias' => 'Venue',
				'type' => 'left',
				'conditions' => array('Venue.id = Event.venue_id')
			),
			array(
				'table' => 'cities',
				'alias' => 'City',
				'type' => 'left',
				'conditions' => array('City.id = Venue.city_id')
			),
			array(
				'table' => 'countries',
				'alias' => 'Country',
				'type' => 'left',
				'conditions' => array('Country.id = City.country_id')
			));

		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.ticketswap', 'Event.promoted',
			'Event.published','Event.win','Event.win_start_date','Event.win_number_winners', 'Event.win_end_date', 'Event.start_date', 'City.name', 'City.slug', 'Country.slug',
			'Venue.slug');
$this->set('title_for_layout', __('Manage giveaways'));



//$participants = $this->Participant->find('all', array(
//			'conditions' => array('Participant.event_id' => $eventId),
//			'order' => array('Participant.status ASC', 'Participant.part_date DESC')
//		));
//$partWon = $this->Participant->find('all', array(
		//	'conditions' => array('Participant.event_id' => $eventId,'Participant.status' => 'win')));
		//$this->set('participants', $participants);
		//$this->set('partWon', $partWon);
		
		$events = $this->paginate('Event', $conditions);
		if(!empty($events)) {
			$c = 0;
			foreach($events as $e) {
				
				$events[$c]['Count']['participant'] = $this->Participant->find('count', array(
					'conditions' => array(
						'event_id' => $e['Event']['id']
					),
					'group' => 'user_id',
				));
				$events[$c]['Count']['winner'] = $this->Participant->find('count', array(
					'conditions' => array(
						'event_id' => $e['Event']['id'],
						'status' => 'win'
					),
					'group' => 'user_id',
				));
				
				$c++;
			}
		}
		
		//this page displaying all list of giveaway events
		$this->set('promoted', $promoted);
		$this->set('events', $events);
		//$this->set('event', $event));
		
		
		 //print_r($this->paginate('Event', $conditions));
		 //exit();
		
	}
		/**
	 * Show main admin page with a list of events.
	 *
	 * @access public
	 */
	public function admin_index() {
		$this->Session->delete('Search.term');
		$this->set('title_for_layout', __('Manage events'));
		$this->paginate['recursive'] = -1;

		$this->paginate['joins'] = array(
			array(
				'table' => 'venues',
				'alias' => 'Venue',
				'type' => 'left',
				'conditions' => array('Venue.id = Event.venue_id')
			),
			array(
				'table' => 'cities',
				'alias' => 'City',
				'type' => 'left',
				'conditions' => array('City.id = Venue.city_id')
			),
			array(
				'table' => 'countries',
				'alias' => 'Country',
				'type' => 'left',
				'conditions' => array('Country.id = City.country_id')
			));

		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.ticketswap', 'Event.promoted',
			'Event.published', 'Event.start_date', 'City.name', 'City.slug', 'Country.slug',
			'Venue.slug');
		$this->paginate['order'] = 'Event.created DESC';
		$countevents = $this->Event->find('all', array(
			'conditions' => array('Event.published' => '1','Event.start_date >=' => date('Y-m-d H:i'))));
		$this->set('countevents', $countevents);
		$this->set('events',$this->paginate('Event'));
		
	}

	/**
	 * Delete event using its id.
	 *
	 * @access public
	 * @param int $id
	 * @param int $repeat
	 */

	public function admin_delete($id = null, $repeat = 1) {
		if(!$id) throw new NotFoundException();
		$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$id), 'recursive'=>-1));
		if(empty($event)) throw new NotFoundException();

		$page = isset($this->request->params['named']['page'])? $this->request->params['named']['page']: null;
		$conditions = null;
		$action = 'index';
		if($this->Session->read('Search.term')) {
			$action = 'search';
			$term = $this->Session->read('Search.term');
			$conditions = array('Event.name like'=>'%'.$term['Search']['term'].'%');
		}

		$deleted = false;
		if($event['Event']['repeat_parent'] === null || $repeat == 1){
			$this->Event->delete($id);
			$deleted = true;
		}
		else if($event['Event']['repeat_parent'] !== null && $repeat == 2) {
			$this->Event->deleteAll(array('Event.repeat_parent' => $event['Event']['repeat_parent']), true, true);
			$deleted = true;
		}
		if($deleted) {
			if($page) {
				$count = $this->Event->find('count', array('conditions'=>$conditions));
				$lastPage = ceil($count / $this->paginate['limit']);
				if($page > $lastPage) $page = $lastPage;
			}
			$this->redirect(array('action'=>$action, 'page' => $page));
		}
		$this->set('page', $page);
		$this->set('eventId', $id);
	}

	/**
	 * admin can add events in admin panel
	 */

	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->request->data['Event']['recaptcha'] = 'correct';
			$this->request->data['Event']['user_id'] = $this->Auth->user('id');
			
			$request = $this->request->data;
			if($_POST['data']['Event']['win'] == '1') {
				$request['Event']['win'] = '1';
			}
			
			if($this->Event->save($request)) {
				$eventId = $this->Event->getInsertID();
				$this->Event->recursive = -1;
				$this->Event->contain(array('Venue' => array('City'=>array('Country'))));
				$event = $this->Event->find('first', array('conditions' => array('Event.id'=>$eventId)));
				$this->request->data['Event']['venue_id'] = $event['Event']['venue_id'];
				// if it is a repeat event create all needed entries
				if($this->request->data['Event']['repeat']!='does_not_repeat') {
					$event['Event']['repeat_parent'] = $eventId;
					$this->Event->save($event);
					$this->request->data['Event']['repeat_parent'] = $eventId;
					$this->request->data['Event']['logo'] = $event['Event']['logo'];
					$repeat = $this->Event->saveRepeat($this->request->data);
				}
				$this->redirect(array('action'=>'index'));
			}
			if($this->request->data['Event']['venue_id']) {
				$this->Event->Venue->contain(array('City'=>array('Country')));
				$venue = $this->Event->Venue->find('first',
					array('conditions'=>array('Venue.id'=>$this->request->data['Event']['venue_id'])));

				$this->request->data['Venue'] = $venue['Venue'];
				$this->request->data['Venue']['City'] = $venue['City'];
			}
			if(isset($this->request->data['Event']['end_date'])
				&& $this->request->data['Event']['start_date'] != $this->request->data['Event']['end_date']) {
					$this->set('end_date_checked', true);
			}
		}
		$this->set('title_for_layout', __('Add an event'));
		if(Configure::read('evento_settings.country_id')) {
			$this->set('country_id', Configure::read('evento_settings.country_id'));
			$this->set('country_name', $this->Country->field('name',
				array('Country.id'=>Configure::read('evento_settings.country_id'))));
		}
		else {
			if(($countryList = Cache::read('evento_countrylist')) === false) {
				$countryList = $this->Country->find('countrylist');
				Cache::write('evento_countrylist', $countryList);
			}
			$this->set('countries', $countryList);
		}

		if(Configure::read('evento_settings.city_name')) {
			$this->set('default_city', Configure::read('evento_settings.city_name'));
		}

		if(Configure::read('evento_settings.adminVenues')) {
			$this->set('venues', $this->Event->Venue->find('list'));
		}

		if(($categoryList = Cache::read('evento_categorylist')) === false) {
			$categoryList =	 $this->Category->find('list', array('fields'=>array('Category.name'),
				'order'=>'name'));
			Cache::write('evento_categorylist', $categoryList);
		}
		$this->set('categories', $categoryList);
		$this->set('useRecaptcha', false);
		$this->request->data['Event']['published'] = true;
	}

	/**
	 * Edit an existing event.
	 *
	 * @access public
	 * @param int $eventId
	 */

	public function admin_edit($eventId = null) {
		if(!$eventId && !($eventId = $this->request->data['Event']['id'])) throw new NotFoundException();
		$this->set('title_for_layout', __('edit event'));
		$this->Event->contain(array('Tag', 'Venue'=>array('City'=>array('Country'))));
		$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId)));

		if(empty($event))throw new NotFoundException();
		$page = isset($this->request->params['named']['page'])? $this->request->params['named']['page']: null;

		if($this->__editEvent($event)) {
			unset($event);
			$action = 'index';
			if($this->Session->read('Search.term')) {
				$action = 'search';
			}
			$this->redirect(array('action'=>$action, 'page' => $page));
		}
		$this->set('page', $page);
		
		
		$participants = $this->Participant->find('all', array(
			'conditions' => array('Participant.event_id' => $eventId),
			'order' => array('Participant.status ASC', 'Participant.part_date DESC')
		));
$partWon = $this->Participant->find('all', array(
			'conditions' => array('Participant.event_id' => $eventId,'Participant.status' => 'win')));
		$this->set('participants', $participants);
		$this->set('partWon', $partWon);
	}

	/**
	 * promote event to make it appear on top
	 *
	 * @param $eventId int
	 * @param $status int
	 */

	public function admin_promote($eventId = null, $status = null) {
		if($eventId === null || $status === null) throw new NotFoundException();
		$this->Event->setPromote($eventId, $status);
		$this->redirect(array('action'=>'index'));
	}

	/**
	 * admin search events
	 */

	public function admin_search() {
		$data = $this->Session->read('Search.term');
		if(!empty($this->request->data) || !empty($data)) {
			if($this->request->data) {
				$this->Session->write('Search.term', $this->request->data);
			}
			else {
				$this->request->data = $data;
			}

			$conditions = array('Event.name like'=>'%'.$this->request->data['Search']['term'].'%');
			$this->paginate['recursive'] = -1;
			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('Venue.city_id = City.id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				));
			$this->paginate['order'] = 'Event.created DESC';
			$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug',
				'Event.published', 'Event.promoted', 'Event.ticketswap', 'City.slug', 'Country.slug', 'Venue.slug');
			$this->set('events', $this->paginate('Event', $conditions));
		}
		$this->set('title_for_layout', __('Search'));
		$this->render('admin_index');
	}

	/**
	 * merge events
	 *
	 * @param int $eventId
	 * @param int $eventOriginalId
	 */

	public function admin_merge($eventId = null, $eventOriginalId = null) {
		if(!$eventId) throw new NotFoundException();
		$this->Event->recursive = -1;
		$eventMerge = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId)));
		if(empty($eventMerge)) throw new NotFoundException();

		if($eventOriginalId===null) {
			$conditions = array();
			if(!empty($this->request->data)){
				$conditions['Event.name like'] = '%'.$this->request->data['Search']['term'].'%';
			}
			$conditions['Event.id <>'] = $eventMerge['Event']['id'];
			$this->paginate['limit'] = 50;
			$this->paginate['recursive'] = -1;
			$this->paginate['order'] = 'Event.created DESC';
			$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug',
				'Event.published', 'Event.promoted');
			$this->set('events', $this->paginate('Event', $conditions));
			$this->set('event_merge', $eventMerge);
		}
		else {
			$this->Event->recursive = -1;
			$eventOriginal = $this->Event->find('first',
				array('conditions'=>array('Event.id'=>$eventOriginalId)));
			if(empty($eventOriginal)) throw new NotFoundException();

			// merge comments
			$this->Event->Comment->updateAll(array('Comment.event_id'=>$eventOriginal['Event']['id']),
				array('Comment.event_id'=>$eventMerge['Event']['id']));

			// merge photos
			$this->Event->Photo->updateAll(array('Photo.event_id'=>$eventOriginal['Event']['id']),
				array('Photo.event_id'=>$eventMerge['Event']['id']));

			// merge tags
			$originalTags = $this->Tag->EventsTag->find('all',
				array('conditions'=>array('EventsTag.event_id'=>$eventOriginal['Event']['id']),
				'fields'=>array('tag_id')));
			if(!empty($originalTags)) {
				$originalTags = Set::extract('/EventsTag/tag_id', $originalTags);
				$this->Tag->EventsTag->deleteAll(array('EventsTag.event_id'=>$eventMerge['Event']['id'],
					'EventsTag.tag_id'=>$originalTags));
			}
			$this->Tag->EventsTag->updateAll(array('EventsTag.event_id'=>$eventOriginal['Event']['id']),
				array('EventsTag.event_id'=>$eventMerge['Event']['id']));

			// merge users
			$originalUsers = $this->User->EventsUser->find('all',
				array('conditions'=>array('EventsUser.event_id'=>$eventOriginal['Event']['id']),
				'fields'=>array('user_id')));
			if(!empty($originalUsers)) {
				$originalUsers = Set::extract('/EventsUser/user_id', $originalUsers);
				$this->User->EventsUser->deleteAll(array('EventsUser.event_id'=>$eventMerge['Event']['id'],
					'EventsUser.user_id'=>$originalUsers));
			}
			$this->User->EventsUser->updateAll(array('EventsUser.event_id'=>$eventOriginal['Event']['id']),
				array('EventsUser.event_id'=>$eventMerge['Event']['id']));

			// delete merge event
			$this->Event->delete($eventMerge['Event']['id']);
			$this->redirect(array('action'=>'index'));
		}
	}

	/**
	 * show event attendees for admin
	 *
	 * @param int $eventId
	 */

	public function admin_attendees($eventId = null) {
		if(!$eventId) throw new NotFoundException();
		$this->Event->contain(array('Attendees'));
		$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId)));
		$this->set('event', $event);
	}

	/**
	 * export event attendees as csv
	 *
	 * @param int $eventId
	 */

	public function admin_export_attendees($eventId = null) {
		if(!$eventId) throw new NotFoundException();
		Configure::write('debug', 0);
		$this->layout = 'export';
		$this->response->header("Cache-Control: private");
		$this->response->header("Content-Description: File Transfer");
		$this->response->header("Content-Disposition: attachment; filename=users.csv");
		$this->RequestHandler->respondAs('csv');

		$this->Event->contain(array('Attendees'=>array('fields'=>array('id'))));
		$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$eventId)));
		$userIds = Set::extract('/Attendees/id', $event);

		$joins = array(
			array(
				'table' => 'cities',
				'alias' => 'City',
				'type' => 'left',
				'conditions' => array('City.id = User.city_id')
			),
			array(
				'table' => 'countries',
				'alias' => 'Country',
				'type' => 'left',
				'conditions' => array('Country.id = City.country_id')
			));
		$users = $this->User->find('all', array('conditions'=>array('User.id'=>$userIds),
			 'fields'=>array('User.username', 'User.email', 'User.web', 'City.name', 'Country.name'),
			'joins'=>$joins));
		$this->set('users', $users);
	}

	/**
	 * bulk manage events
	 */

	public function admin_bulk() {
		if(empty($this->request->data)) throw new NotFoundException();
		$ids = array();
		foreach($this->request->data['Event']['id'] as $key => $value) {
			if($value != 0) {
				$ids[] = $key;
			}
		}
		switch($this->request->data['Event']['option']) {
			case 'publish':
				$this->Event->bulkPublish($ids, 1);
				break;
			case 'unpublish':
				$this->Event->bulkPublish($ids, 0);
				break;
			case 'delete':
				$this->Event->bulkDelete($ids);
				break;
		}
		$this->redirect(array('action'=>'index'));
	}

	//////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////// Private Methods
	//////////////////////////////////////////////////////////////////////

	/**
	 * We use this method to edit events in user mode and admin mode
	 *
	 * @param array $event
	 */

	private function __editEvent($event) {
		if(!empty($this->request->data)) {
			if($this->Event->save($this->request->data)) {
				return true;
			}
			if(!empty($this->request->data['Event']['venue_id'])) {
				// if event could not be saved check if venue changed and retrive its data from database
				if($this->request->data['Event']['venue_id'] != $event['Event']['venue_id']) {
					$this->Event->Venue->contain(array('City'=>array('Country')));
					$tmpVenue = $this->Event->Venue->find('first',
						array('conditions'=>array('Venue.id'=>$this->request->data['Event']['venue_id'])));
					$venue['Venue'] = $tmpVenue['Venue'];
					$venue['Venue']['City'] = $tmpVenue['City'];
					$this->request->data['Venue'] = $venue['Venue'];
				}
				else {
					// if venue didn't change use the original data
					$this->request->data['Venue'] = $event['Venue'];
				}
			}
		}
		else {
			$tags = Set::extract('/Tag/name', $event);
			$event['Event']['tags'] = implode(', ', $tags);
			$this->request->data = $event;
		}

		// load needed data for the event form
		if(Configure::read('evento_settings.country_id')) {
			$this->set('country_id', Configure::read('evento_settings.country_id'));
			$this->set('country_name', $this->Country->field('name',
				array('Country.id'=>Configure::read('evento_settings.country_id'))));
		}
		else {
			if(($countryList = Cache::read('evento_countrylist')) === false) {
				$countryList = $this->Country->find('countrylist');
				Cache::write('evento_countrylist', $countryList);
			}
			$this->set('countries', $countryList);
		}

		if(Configure::read('evento_settings.city_name')) {
			$this->set('default_city', Configure::read('evento_settings.city_name'));
		}

		if(Configure::read('evento_settings.adminVenues')) {
			$this->set('venues', $this->Event->Venue->find('list'));
		}

		if(($categoryList = Cache::read('evento_categorylist')) === false) {
			$categoryList =	 $this->Category->find('list', array('fields'=>array('Category.name'),
				'order'=>'name'));
			Cache::write('evento_categorylist', $categoryList);
		}
		if($event['Event']['start_date'] != $event['Event']['end_date']) {
			$this->set('end_date_checked', true);
		}
		$this->set('categories', $categoryList);
		$this->set('event', $event);
	}
	
	private function __runGameValidation($event){
		
		$evtSlug = $event['Country']['slug'].'/'.$event['City']['slug'].'/'.$event['Venue']['slug'].'/'.$event['Event']['slug'];
		
		if($event['Event']['win'] == '1') {
			$isUpComing = (time() >= strtotime($event['Event']['win_start_date'])) ? FALSE : TRUE;
			$isExpired  = (time() >= strtotime($event['Event']['win_end_date'])) ? TRUE : FALSE;
			
			if($isExpired) {
				$this->Session->setFlash('Win Ticket event expired');
				$this->redirect(array('controller'=>'event', 'action'=>$evtSlug));
			}
			
			if($isUpComing) {
				$this->Session->setFlash('Win Ticket event upcoming');
				$this->redirect(array('controller'=>'event', 'action'=>$evtSlug));
			}
		}
		
		//to do :
		//participant only have 24hrs. from they last participation
		// $lastParticipation = $this->Participant->find('first', array(
			// 'conditions' => array('user_id' => $this->Auth->user('id'), 'event_id' => $event['Event']['id']),
			// 'order' => array('Participant.part_date DESC'),
			// 'limit' => 1
		// ));
		
		//to do :
		//participant do not participate more than 5 times for this eventId
		$userParticipation = $this->Participant->find('count', array(
			'conditions' => array('user_id' => $this->Auth->user('id'), 'event_id' => $event['Event']['id'])
		));
		
		//user has never won the ticket
		$checkAlreadyWon = $this->Participant->find('first', array(
			'conditions' => array('user_id' => $this->Auth->user('id'), 'event_id' => $event['Event']['id'], 'status' => 'win')
		));
		
		if(!empty($checkAlreadyWon)) {
			$this->Session->setFlash(' <div id="hide" class="alerts">
  <div class="alert-message success">
    <a class="close" href="#">×</a>
    <p><center><a href="/win"><strong>Gefeliciteerd!</strong> Jij hebt al tickets voor dit evenement gewonnen. Klik hier voor meer winacties.</p>
  </center></a></div>
</div>');
			$this->redirect(array('controller'=>'event', 'action'=>$evtSlug));
		}
		
		if($userParticipation >= 5)
		{
			// $partDate = $lastParticipation['Participant']['part_date'];
			// $hrsLeft = $this->__getDiffInHours($partDate, date('Y-m-d H:i:s'));
			
			// if($hrsLeft < 24) {
				// $this->Session->setFlash('
					// <div id="hide" class="alerts">
  // <div class="alert-message error">
    // <a class="close" href="#">×</a>
    // <p><center><a href="/win"><strong>Helaas, je kunt maar 1 keer in de 24 uur meespelen met deze winactie.</strong> Probeer het later nogmaals. Klik hier voor meer winacties.</p></a>
  // </center></div>
// </div>');
				// $this->redirect(array('controller'=>'event', 'action'=>$evtSlug));
			// }
			
			$this->Session->setFlash('
					<div id="hide" class="alerts">
  <div class="alert-message error">
    <a class="close" href="#">×</a>
    <p><center><a href="/win"><strong>Sorry</strong> you already played 5 times for this event.</p></a>
  </center></div>
</div>');
				$this->redirect(array('controller'=>'event', 'action'=>$evtSlug));
		}
		
		
		//check if user has entering following field :
		//first name, second name, birthdate and gender
		$valid = TRUE;
		$user = $this->User->find('first', array('conditions'=>array(
			'id'=>$this->Auth->user('id'))));
		
		if($user['User']['first_name'] == '') {
			$valid = FALSE;
		}
		if($user['User']['email'] == '') {
			$valid = FALSE;
		}
		if($user['User']['last_name'] == '') {
			$valid = FALSE;
		}
		if($user['User']['zipcode'] == '') {
			$valid = FALSE;
		}
		if($user['User']['birthdate'] == '') {
			$valid = FALSE;
		}
		
		if($user['User']['gender'] == '') {
			$valid = FALSE;
		}
		
		if(!$valid) {
			$this->Session->setFlash('<div class="alerts">
  <div class="alert-message warning">
    <p><center>Vul onderstaande velden naar waarheid in om mee te spelen met de winactie.</p>
  </center></a></div>
</div>');
			$this->redirect(array('controller'=>'users','action'=>'edit/quick/' . $event['Event']['id']));
		}
		if($this->__myAge($user['User']['birthdate']) < 18)
		{
			$this->Session->setFlash('<div id="hide" class="alerts">
  <div class="alert-message warning">
    <center><a class="close" href="#">×</a>
    <p>Helaas, je moet <strong>minimaal 18 jaar</strong> zijn om mee te doen met deze winactie.</p>
  </center></div>
</div>');
			$this->redirect(array('controller'=>'event', 'action'=>$evtSlug));
		}
		
		return;
	}
	
	
	public function win($country = null, $city = null, $venue = null, $slug = null){
		
		if(!$country || !$city || !$venue || !$slug) throw new NotFoundException();
		if(!($event = $this->Event->viewEvent($slug))) throw new NotFoundException();
		
		if($event['Event']['win'] != '1') throw new NotFoundException();
		
		//check game validation
		$this->__runGameValidation($event);
		
		$this->set('eventId', $event['Event']['id']);
		$this->set('eventDetail', $event['Event']);
		
		//show tos page, and dice roll
	}
	
	public function play($eventId=NULL){
		
		if(!isset($eventId) || !isset($_POST['tos'])) throw new NotFoundException();
		
		$event = $this->Event->find('first', array(
			'conditions'=>array('Event.id' => $eventId)
		));
		
		$fullEvt = $this->Event->viewEvent($event['Event']['slug']);
		$evtSlug = 'event/'.$fullEvt['Country']['slug'].'/'.$fullEvt['City']['slug'].'/'.$fullEvt['Venue']['slug'].'/'.$fullEvt['Event']['slug'];
		
		if(empty($event))  {
			$this->redirect('/');
		}
		//check game validation
		$this->__runGameValidation($this->Event->viewEvent($event['Event']['slug']));
		
		//reduce credits
		$userCredit = $this->Credit->find('first', array(
			'conditions' => array('user_id' => $this->Auth->user('id'))
		));
		
		if($userCredit['Credit']['current_credits'] < 1 || empty($userCredit)) {
			$this->Session->setFlash('<div id="hide" class="alerts">
				<div class="alert-message error">
					<a class="close" href="#">×</a>
					<p><center><a href="#"><strong>Sorry</strong> You do not have enough credits to play</p>
				</center></a></div>
			</div>');
			$this->redirect(array('controller'=>'event', 'action'=>$fullEvt['Country']['slug'].'/'.$fullEvt['City']['slug'].'/'.$fullEvt['Venue']['slug'].'/'.$fullEvt['Event']['slug']));
		}
		
		$currCredits = ($userCredit['Credit']['current_credits'] < 1) ? 0 : $userCredit['Credit']['current_credits'] - 1;
		
		$this->Credit->id = $userCredit['Credit']['id'];
		$this->Credit->save(array('Credit' => array(
			'user_id' => $this->Auth->user('id'),
			'current_credits' => $currCredits
		)));
		
		if($event['Event']['win'] != '1') {
			$this->redirect('/');
		}
		
		//game data
		$chance = $event['Event']['win_chance'];
		$winNumber = $event['Event']['win_number_winners'];
		
		//fetch all game participant of this event
		$participants = $this->Participant->find('all', array(
			'conditions' => array('Participant.event_id' => $event['Event']['id'])
		));
		
		$countParticipant = sizeof($participants);
		$personNumber = $countParticipant + 1;
		
		$winner = $this->Participant->find('all', array(
			'conditions' => array('Participant.event_id' => $event['Event']['id'], 'Participant.status' => 'win')
		));
		
		$winnerNumber = sizeof($winner);
		
		if($winnerNumber * $chance > $countParticipant) {
			$prob = 0;
		} 
		else {
			
			if($winnerNumber < $winNumber && $personNumber % $chance == 0 )
			{
				$prob = 1;
			} else {
				
				if($winNumber >= $winnerNumber)
				{
					$prob = 0;
				}
				else
					$prob = rand(0,1);
			}
			
			
		}
		
		$status = ($prob == 1) ? 'win' : 'lose';
		
		if($status == 'win') {
			
			/** - check if number 2 is already given in other place for this eventId -- and 1 hour after recorded */
			$timestamp = date('Y-m-d H:i:s', strtotime("+1 hour"));
			
			$numbersRecord = $this->Roll->find('all',array(
				'conditions' => array(
					'Roll.event_id' => $event['Event']['id'],
					'Roll.date_expired > ' => $timestamp
				)
			));
			
			if(sizeof($numbersRecord) >= $event['Event']['win_number_winners']){
				
				$diceArray = array(1,3,4,5,6);
				$keyNum = array_rand($diceArray);
				$num = $diceArray[$keyNum];
				
				if($event['Event']['facebook'] != '')
					$this->__setRsvpEvent("maybe", $event['Event']['facebook']);
				
			} else {
				
				$num = 2;
				$timestamp = date('Y-m-d H:i:s', strtotime("+1 hour"));
				
				/** write the latest record given dice number */
				$rollData = array(
					'event_id' => $event['Event']['id'],
					'user_id' => $this->Auth->user('id'),
					'number_of_dice' => $num,
					'date_expired' => $timestamp
				);
				$this->Roll->save($rollData);
			}
			
		}
		else {
			$diceArray = array(1,3,4,5,6);
			$keyNum = array_rand($diceArray);
			$num = $diceArray[$keyNum];
			
			if($event['Event']['facebook'] != '')
				$this->__setRsvpEvent("maybe", $event['Event']['facebook']);
			
		}
		
	

		$this->set('event', $event);
		$this->set('slug', $evtSlug);
		$this->set('eventId', $event['Event']['id']);
		$this->set('number', $num); //dice number given
		
	}
	
	private function __countProbability()
	{
		
		$probability = 0.25;
		$length = 4;
		
		//return bool : 1 = win, 0 = lose
		$test = mt_rand(1, $length);
		return $test<=$probability*$length;
		
	}
	
	private function __myAge($dob){
		$seg = explode(' ', $dob);
		$birthDate = explode('-', $seg[0]);
		$age = (date('md', date('U', mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date('md')
			? ((date('Y') - $birthDate[0]) - 1)
			: (date('Y') - $birthDate[0]));
		return $age;
	}
	
	
	private function __getDiffInHours($from, $to)
	{
		$total      = strtotime($to) - strtotime($from);
		$hours      = abs($total / (60 * 60));
		
		return $hours;
	}
	
	public function ajaxSubmit($eventId, $val)
	{
		
		if($val == '2') {
			$status = 'win';
			$this->Session->write('Game.winner', $this->Auth->user('id'));
		}
		else {
			$status = 'lose';
			$this->Participant->save(
				array( 'Participant' => array(
						'event_id' => $eventId,
						'ip_adres' => $this->request->clientIp(),
						'user_id' => $this->Auth->user('id'), 
						'status' => $status
				))
			);
			
			$this->Session->write('Ticket.lose', TRUE);
		}
		
		if($this->request->is('ajax'))
		{
			$this->Session->write('Number.dice', $val);
			echo json_encode(array('status' => 'OK', 'number' => $val, 'eventId' => $eventId));
			exit();
		}
		
		throw new NotFoundException();
	}
	
	public function lose(){
		
		if(!$this->Session->read('Ticket.lose') || $this->Session->read('Ticket.lose') === FALSE)
			$this->redirect('/');
	
		$this->Session->delete('Ticket.lose');
	}
	
	public function getticket($eventId=NULL){
		
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		header("Pragma: no-cache");
		
		$this->set('title_for_layout', 'Je hebt gewonnen!');
		$event = $this->Event->find('first', array(
			'conditions' => array('Event.id' => $eventId)
		));
		
		if(empty($event)) {
			$this->redirect('/');
		}
		
		if($event['Event']['win'] != '1') {
			$this->redirect('/');
		}
		
		if($this->Session->read('Game.winner') != $this->Auth->user('id')) {
			$this->redirect('/');
		}
		
		$fullEvt = $this->Event->viewEvent($event['Event']['slug']);
		$evtSlug = $fullEvt['Country']['slug'].'/'.$fullEvt['City']['slug'].'/'.$fullEvt['Venue']['slug'].'/'.$fullEvt['Event']['slug'];
		
		if(!$this->Session->read('Number.dice') || $this->Session->read('Number.dice') != 2)
			$this->redirect(array('controller' => 'event', 'action'=>$evtSlug));
	
		if(!isset($eventId)) throw new NotFoundException();
		
		$this->set('event', $event);
	}
	
	public function accept($eventId=NULL){
		
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
		header("Pragma: no-cache");
	
		if(!isset($eventId)) throw new NotFoundException();
		
		$winnerRow = $this->Participant->find('first', array('conditions'=>array(
			'event_id'=>$eventId,'user_id'=>$this->Auth->user('id'),'status'=>'win'
		)));
		if(!empty($winnerRow))
			$this->redirect('/');
		
		if($this->Session->read('Game.winner') != $this->Auth->user('id')) {
			$this->redirect('/');
		}
		
		$this->Participant->save(array(
			'event_id'=>$eventId,'user_id'=>$this->Auth->user('id'),'ip_adres'=>$this->request->clientIp(),'status'=>'win'
		));
			
		$this->User->EventsUser->save(array(
			'event_id'=>$eventId,'user_id'=>$this->Auth->user('id')
		));
		
		$event = $this->Event->find('first', array('conditions'=>array(
			'Event.id'=>$eventId
		)));
		
		$fullEvt = $this->Event->viewEvent($event['Event']['slug']);
		$evtSlug = $fullEvt['Country']['slug'].'/'.$fullEvt['City']['slug'].'/'.$fullEvt['Venue']['slug'].'/'.$fullEvt['Event']['slug'];
		
		if($fullEvt['Event']['facebook'] != '') 
			$this->__setRsvpEvent("attending", $fullEvt['Event']['facebook']);
		
		$this->Session->delete('Number.dice');
		$this->Session->delete('Game.winner');
		
		$this->Session->write('Ticket.accept', TRUE);
		$this->redirect(array('controller'=>'events', 'action'=>'won', $event['Event']['id']));
		
	}
	
	public function won($eventId=NULL){
		
		if(!isset($eventId)) throw new NotFoundException();
		
		$event = $this->Event->find('first', array('conditions'=>array(
			'Event.id'=> $eventId

		)));
		if(empty($event)) throw new NotFoundException();
		
		$fullEvt = $this->Event->viewEvent($event['Event']['slug']);
		$evtSlug = 'event/'.$fullEvt['Country']['slug'].'/'.$fullEvt['City']['slug'].'/'.$fullEvt['Venue']['slug'].'/'.$fullEvt['Event']['slug'];
		if(!$this->Session->read('Ticket.accept') || $this->Session->read('Ticket.accept') === FALSE)
			$this->redirect(array('controller'=>'event', 'action'=>$fullEvt['Country']['slug'].'/'.$fullEvt['City']['slug'].'/'.$fullEvt['Venue']['slug'].'/'.$fullEvt['Event']['slug']));
			
		$this->set('event', $event);
		$this->set('fullEvt', $fullEvt);
		$this->set('slug', $evtSlug);
		$this->set('authUser', $this->Auth->user());

		$this->Session->delete('Ticket.accept');
	}
	
	public function cancel($eventId=NULL){
		
		if(!isset($eventId)) throw new NotFoundException();
		
		$this->Participant->save(array(
			'event_id'=>$eventId,'user_id'=>$this->Auth->user('id'),'ip_address'=>$this->request->clientIp(),'status'=>'lose'
		));
		
		$event = $this->Event->find('first', array(
			'conditions'=>array('Event.id'=>$eventId)
		));
		
		$fullEvt = $this->Event->viewEvent($event['Event']['slug']);
		$evtSlug = $fullEvt['Country']['slug'].'/'.$fullEvt['City']['slug'].'/'.$fullEvt['Venue']['slug'].'/'.$fullEvt['Event']['slug'];
		
		$this->Session->delete('Number.dice');
		$this->Session->setFlash('<div id="hide" class="alerts">
  <div class="alert-message error">
    <a class="close" href="#">×</a>
    <p><center><strong><a href="/win">Jammer, je hebt de tickets / gastenlijstplaatsen afgewezen.</strong> Klik hier voor meer winacties.</a></p>
  </center></div>
</div>');

		/** delete roll table record because cancel action */
		$this->Roll->deleteAll(array('Roll.event_id' => $event['Event']['id'], 'Roll.user_id' => $this->Auth->user('id')));
		
		
		$this->redirect(array('controller'=>'event', 'action'=>$evtSlug));
	}
	public function house() {
		
		
		$conditions = array(
				'or'=>array('Event.genre like'=>'%house%', 'Event.genre like'=>'%EDM%'),
				'Event.published' => true
			);
		$conditions['end_date >='] = date('Y-m-d H:i');
		
		
		$country='all-countries';
		$city='all-cities';
		$venue = 'all-venues';
		$category='all-categories';
		$tag='all-tags';
		$year = null;
		$month = null;
		$day = null;
		
		$this->set('country', $country);
		$this->set('city', $city);
		$this->set('venue', $venue);
		$this->set('category', $category);
		$this->set('current_tag', $tag);
		$this->set('year', $year);
		$this->set('month', $month);
		$this->set('day', $day);
		$this->set('isGiveaway', TRUE);
		
		/*
		 * paginate events using according to conditions
		 */
		$this->paginate['contain'] = array('Tag');
		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.lineup', 'Event.start_date',
			'Event.notes', 'Event.promoted', 'Event.ticketswap', 'Event.logo', 'Event.ticket', 'Event.win', 'Event.presale', 'Event.doorsale', 'Event.facebook', 'Venue.name', 'Venue.slug', 'City.name', 'City.slug',
			'Country.name', 'Country.slug', 'Category.name',
			'Category.slug');

			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = Venue.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				),
				array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'left',
					'conditions' => array('Category.id = Event.category_id')
				)
			);
		
		if(($promoted = Cache::read('evento_promoted')) === false) {
			$this->Event->contain(array('Tag'));
			$promoted = $this->Event->find('all',
				array('conditions'=>array('Event.promoted'=>1, 'Event.published'=>1),
				'joins' => $this->paginate['joins'], 'fields' => $this->paginate['fields']));
			Cache::write('evento_promoted', $promoted);
		}
		
		//this page displaying all list of giveaway events
		$this->set('promoted', $promoted);
		$this->set('events', $this->paginate('Event', $conditions));
		
		
		// print_r($this->paginate('Event', $conditions));
		// exit();
		
	}
	public function festivals() {
		
		
		$conditions = array(
				'or'=>array('Event.name like'=>'%festival%', 'Event.category_id'=>'1'),
				'Event.published' => true
			);
		$conditions['end_date >='] = date('Y-m-d H:i');
	
		
		$country='all-countries';
		$city='all-cities';
		$venue = 'all-venues';
		$category='all-categories';
		$tag='all-tags';
		$year = null;
		$month = null;
		$day = null;
		
		$this->set('country', $country);
		$this->set('city', $city);
		$this->set('venue', $venue);
		$this->set('category', $category);
		$this->set('current_tag', $tag);
		$this->set('year', $year);
		$this->set('month', $month);
		$this->set('day', $day);
		$this->set('isGiveaway', TRUE);
		
		/*
		 * paginate events using according to conditions
		 */
		$this->paginate['contain'] = array('Tag');
		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.lineup', 'Event.start_date',
			'Event.notes', 'Event.promoted', 'Event.ticketswap', 'Event.logo', 'Event.ticket', 'Event.win', 'Event.presale', 'Event.doorsale', 'Event.facebook', 'Venue.name', 'Venue.slug', 'City.name', 'City.slug',
			'Country.name', 'Country.slug', 'Category.name',
			'Category.slug');

			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = Venue.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				),
				array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'left',
					'conditions' => array('Category.id = Event.category_id')
				)
			);
		
		if(($promoted = Cache::read('evento_promoted')) === false) {
			$this->Event->contain(array('Tag'));
			$promoted = $this->Event->find('all',
				array('conditions'=>array('Event.promoted'=>1, 'Event.published'=>1),
				'joins' => $this->paginate['joins'], 'fields' => $this->paginate['fields']));
			Cache::write('evento_promoted', $promoted);
		}
		
		//this page displaying all list of giveaway events
		$this->set('promoted', $promoted);
		$this->set('events', $this->paginate('Event', $conditions));
		
		
		// print_r($this->paginate('Event', $conditions));
		// exit();
		
	}
	public function urban() {
		
		
		$conditions = array(
				'or'=>array('Event.genre like'=>'%urban%'),
				'Event.published' => true
			);
		$conditions['end_date >='] = date('Y-m-d H:i');
		
		
		$country='all-countries';
		$city='all-cities';
		$venue = 'all-venues';
		$category='all-categories';
		$tag='all-tags';
		$year = null;
		$month = null;
		$day = null;
		
		$this->set('country', $country);
		$this->set('city', $city);
		$this->set('venue', $venue);
		$this->set('category', $category);
		$this->set('current_tag', $tag);
		$this->set('year', $year);
		$this->set('month', $month);
		$this->set('day', $day);
		$this->set('isGiveaway', TRUE);
		
		/*
		 * paginate events using according to conditions
		 */
		$this->paginate['contain'] = array('Tag');
		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.lineup', 'Event.start_date',
			'Event.notes', 'Event.promoted', 'Event.ticketswap', 'Event.logo', 'Event.ticket', 'Event.win', 'Event.presale', 'Event.doorsale', 'Event.facebook', 'Venue.name', 'Venue.slug', 'City.name', 'City.slug',
			'Country.name', 'Country.slug', 'Category.name',
			'Category.slug');

			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = Venue.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				),
				array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'left',
					'conditions' => array('Category.id = Event.category_id')
				)
			);
		
		if(($promoted = Cache::read('evento_promoted')) === false) {
			$this->Event->contain(array('Tag'));
			$promoted = $this->Event->find('all',
				array('conditions'=>array('Event.promoted'=>1, 'Event.published'=>1),
				'joins' => $this->paginate['joins'], 'fields' => $this->paginate['fields']));
			Cache::write('evento_promoted', $promoted);
		}
		
		//this page displaying all list of giveaway events
		$this->set('promoted', $promoted);
		$this->set('events', $this->paginate('Event', $conditions));
		
		
		// print_r($this->paginate('Event', $conditions));
		// exit();
		
	}
	public function hardstyle() {
		
		
		$conditions = array(
				'or'=>array('Event.genre like'=>'%hardstyle%'),
				'Event.published' => true
			);
		$conditions['end_date >='] = date('Y-m-d H:i');
		
		
		$country='all-countries';
		$city='all-cities';
		$venue = 'all-venues';
		$category='all-categories';
		$tag='all-tags';
		$year = null;
		$month = null;
		$day = null;
		
		$this->set('country', $country);
		$this->set('city', $city);
		$this->set('venue', $venue);
		$this->set('category', $category);
		$this->set('current_tag', $tag);
		$this->set('year', $year);
		$this->set('month', $month);
		$this->set('day', $day);
		$this->set('isGiveaway', TRUE);
		
		/*
		 * paginate events using according to conditions
		 */
		$this->paginate['contain'] = array('Tag');
		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.lineup', 'Event.start_date',
			'Event.notes', 'Event.promoted', 'Event.ticketswap', 'Event.logo', 'Event.ticket', 'Event.win', 'Event.presale', 'Event.doorsale', 'Event.facebook', 'Venue.name', 'Venue.slug', 'City.name', 'City.slug',
			'Country.name', 'Country.slug', 'Category.name',
			'Category.slug');

			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = Venue.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				),
				array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'left',
					'conditions' => array('Category.id = Event.category_id')
				)
			);
		
		if(($promoted = Cache::read('evento_promoted')) === false) {
			$this->Event->contain(array('Tag'));
			$promoted = $this->Event->find('all',
				array('conditions'=>array('Event.promoted'=>1, 'Event.published'=>1),
				'joins' => $this->paginate['joins'], 'fields' => $this->paginate['fields']));
			Cache::write('evento_promoted', $promoted);
		}
		
		//this page displaying all list of giveaway events
		$this->set('promoted', $promoted);
		$this->set('events', $this->paginate('Event', $conditions));
		
		
		// print_r($this->paginate('Event', $conditions));
		// exit();
		
	}
	public function trance() {
		
		
		$conditions = array(
				'or'=>array('Event.genre like'=>'%trance%'),
				'Event.published' => true
			);
		$conditions['end_date >='] = date('Y-m-d H:i');
		
		
		$country='all-countries';
		$city='all-cities';
		$venue = 'all-venues';
		$category='all-categories';
		$tag='all-tags';
		$year = null;
		$month = null;
		$day = null;
		
		$this->set('country', $country);
		$this->set('city', $city);
		$this->set('venue', $venue);
		$this->set('category', $category);
		$this->set('current_tag', $tag);
		$this->set('year', $year);
		$this->set('month', $month);
		$this->set('day', $day);
		$this->set('isGiveaway', TRUE);
		
		/*
		 * paginate events using according to conditions
		 */
		$this->paginate['contain'] = array('Tag');
		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.lineup', 'Event.start_date',
			'Event.notes', 'Event.promoted', 'Event.ticketswap', 'Event.logo', 'Event.ticket', 'Event.win', 'Event.presale', 'Event.doorsale', 'Event.facebook', 'Venue.name', 'Venue.slug', 'City.name', 'City.slug',
			'Country.name', 'Country.slug', 'Category.name',
			'Category.slug');

			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = Venue.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				),
				array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'left',
					'conditions' => array('Category.id = Event.category_id')
				)
			);
		
		if(($promoted = Cache::read('evento_promoted')) === false) {
			$this->Event->contain(array('Tag'));
			$promoted = $this->Event->find('all',
				array('conditions'=>array('Event.promoted'=>1, 'Event.published'=>1),
				'joins' => $this->paginate['joins'], 'fields' => $this->paginate['fields']));
			Cache::write('evento_promoted', $promoted);
		}
		
		//this page displaying all list of giveaway events
		$this->set('promoted', $promoted);
		$this->set('events', $this->paginate('Event', $conditions));
		
		
		// print_r($this->paginate('Event', $conditions));
		// exit();
		
	}
	public function techno() {
		
		
		$conditions = array(
				'or'=>array('Event.genre like'=>'%techno%'),
				'Event.published' => true
			);
		$conditions['end_date >='] = date('Y-m-d H:i');
		
		
		$country='all-countries';
		$city='all-cities';
		$venue = 'all-venues';
		$category='all-categories';
		$tag='all-tags';
		$year = null;
		$month = null;
		$day = null;
		
		$this->set('country', $country);
		$this->set('city', $city);
		$this->set('venue', $venue);
		$this->set('category', $category);
		$this->set('current_tag', $tag);
		$this->set('year', $year);
		$this->set('month', $month);
		$this->set('day', $day);
		$this->set('isGiveaway', TRUE);
		
		/*
		 * paginate events using according to conditions
		 */
		$this->paginate['contain'] = array('Tag');
		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.lineup', 'Event.start_date',
			'Event.notes', 'Event.promoted', 'Event.ticketswap', 'Event.logo', 'Event.ticket', 'Event.win', 'Event.presale', 'Event.doorsale', 'Event.facebook', 'Venue.name', 'Venue.slug', 'City.name', 'City.slug',
			'Country.name', 'Country.slug', 'Category.name',
			'Category.slug');

			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = Venue.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				),
				array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'left',
					'conditions' => array('Category.id = Event.category_id')
				)
			);
		
		if(($promoted = Cache::read('evento_promoted')) === false) {
			$this->Event->contain(array('Tag'));
			$promoted = $this->Event->find('all',
				array('conditions'=>array('Event.promoted'=>1, 'Event.published'=>1),
				'joins' => $this->paginate['joins'], 'fields' => $this->paginate['fields']));
			Cache::write('evento_promoted', $promoted);
		}
		
		//this page displaying all list of giveaway events
		$this->set('promoted', $promoted);
		$this->set('events', $this->paginate('Event', $conditions));
		
		
		// print_r($this->paginate('Event', $conditions));
		// exit();
		
	}
	
	public function giveaway() {
		
		$conditions = array();
		$conditions['Event.win'] = '1';
		$conditions['end_date >='] = date('Y-m-d H:i');
		$conditions['win_start_date <='] = date('Y-m-d H:i');
		$conditions['win_end_date >='] = date('Y-m-d H:i');
		
		
		$country='all-countries';
		$city='all-cities';
		$venue = 'all-venues';
		$category='all-categories';
		$tag='all-tags';
		$year = null;
		$month = null;
		$day = null;
		
		$this->set('country', $country);
		$this->set('city', $city);
		$this->set('venue', $venue);
		$this->set('category', $category);
		$this->set('current_tag', $tag);
		$this->set('year', $year);
		$this->set('month', $month);
		$this->set('day', $day);
		$this->set('isGiveaway', TRUE);
		
		/*
		 * paginate events using according to conditions
		 */
		$this->paginate['contain'] = array('Tag');
		$this->paginate['fields'] = array('Event.id', 'Event.name', 'Event.slug', 'Event.lineup', 'Event.start_date',
			'Event.notes', 'Event.promoted', 'Event.ticketswap', 'Event.logo', 'Event.ticket', 'Event.win', 'Event.presale', 'Event.doorsale', 'Event.facebook', 'Venue.name', 'Venue.slug', 'City.name', 'City.slug',
			'Country.name', 'Country.slug', 'Category.name',
			'Category.slug');

			$this->paginate['joins'] = array(
				array(
					'table' => 'venues',
					'alias' => 'Venue',
					'type' => 'left',
					'conditions' => array('Venue.id = Event.venue_id')
				),
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = Venue.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				),
				array(
					'table' => 'categories',
					'alias' => 'Category',
					'type' => 'left',
					'conditions' => array('Category.id = Event.category_id')
				)
			);
		
		if(($promoted = Cache::read('evento_promoted')) === false) {
			$this->Event->contain(array('Tag'));
			$promoted = $this->Event->find('all',
				array('conditions'=>array('Event.promoted'=>1, 'Event.published'=>1),
				'joins' => $this->paginate['joins'], 'fields' => $this->paginate['fields']));
			Cache::write('evento_promoted', $promoted);
		}
		
		//this page displaying all list of giveaway events
		$this->set('promoted', $promoted);
		$this->set('events', $this->paginate('Event', $conditions));
		
		
		// print_r($this->paginate('Event', $conditions));
		// exit();
		
	}
	
	private function __setRsvpEvent($status, $dataFbId){
		
		$fbConfig = Configure::read('Facebook');
		$appId 			= $fbConfig['appId'];
		$app_secret	= $fbConfig['secret'];
		
		preg_match('!\d+!', $dataFbId, $matches);
		$eventFBId = join('', $matches);
		
		$access_token =  $facebook->getAccessToken();
		$test = $facebook->api("/" . $eventFBId . "/" .$status, "post", array("access_token" => $access_token));
		
		return $test['success'];
	}
	
}
?>