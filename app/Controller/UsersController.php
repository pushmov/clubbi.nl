<?php
/**
 * @copyright Copyright 2008 Evento
 */

App::uses('CakeEmail', 'Network/Email');
App::uses('Controller', 'Controller');
$dirname = dirname(dirname(__FILE__));
require_once("$dirname/vendors/Facebook/facebook.php");

class UsersController extends AppController {

	var $name = 'Users';
	var $components = array('RequestHandler');
	var $helpers = array('Html', 'Form', 'Time', 'Js', 'Text', 'Timeformat');
	var $uses = array('User', 'Event', 'EventsUser', 'Country', 'City', 'Participant', 'Credit');
	var $paginate = array('limit' => 30, 'order' => array('User.created' => 'desc'));
	
	
	  

	/**
	 * Overload beforeFilter and set some permissions.
	 */

	public function beforeFilter() {
		$this->Auth->allow('facebooklogin','facebook_connect','login','logout', 'view', 'organisation_register','recover',
			'code_login', 'activation', 'index', 'organisation_login');
//register toevoegen mochten we email register aanzetten
		return parent::beforeFilter();
	}
	function facebooklogin()
    {
		$fbConfig = Configure::read('Facebook');
		$appId 			= $fbConfig['appId'];
		$app_secret	= $fbConfig['secret'];
		$facebook = new Facebook(array(
		'appId'	=> $appId,
		'secret'	=> $app_secret,
		));
		
		
		 // If the user has not installed the app, redirect them to the Login Dialog
	
		$loginUrl = $facebook->getLoginUrl(array(
		'scope'	=> 'email, user_birthday, user_hometown, user_photos, rsvp_event',
		'redirect_uri'	=> FACEBOOK_BASE_URL.'users/facebook_connect',
		'display'=>'popup'
		));
		
		$this->redirect($loginUrl);
    }
	
	 function facebook_connect()
    {
	
	//print_r($_REQUEST);
	
	 if(isset($_REQUEST['error']))
     {
         if(isset($_REQUEST['error_reason']) && $_REQUEST['error_reason']=='user_denied')
         {
       echo "Sorry, Access Denied.";
	   die();
         }
     }
	
		$fbConfig = Configure::read('Facebook');
		$appId 			= $fbConfig['appId'];
		$app_secret	= $fbConfig['secret'];
		 
		$facebook = new Facebook(array(
		'appId'	=> $appId,
		'secret'	=> $app_secret,
		));
		$fb_user = $facebook->getUser();
		
		if ($fb_user){
		
		 $fb_user = $facebook->api('/me','GET');
		//print_r($fb_user);
		//echo $fb_user['id'];
		
		
		function getRandomString($length = 6) {
			$validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ+-*#&@!?0123456789";
			$validCharNumber = strlen($validCharacters);
		 
			$result = "";
		 
			for ($i = 0; $i < $length; $i++) {
				$index = mt_rand(0, $validCharNumber - 1);
				$result .= $validCharacters[$index];
			}
		 
			return $result;
		}
 
          $randPass=getRandomString();
				$mysql_date = date("Y/m/d", strtotime($fb_user['birthday']));
				$genderr = $fb_user['gender'];
				if($genderr == "male"){
					$genderr = "0";
				}
				if($genderr == "female"){
					$genderr = "1";
				}

		 
		   $data['User']= array(
					'username'      => $this->_checkUsername(strtolower(str_replace(' ', '', $fb_user['name']))),                             
					'password'      => $randPass, # Set random password
					'email'          => $fb_user['email'],
					'admin'			=> '0',
					'notification'	=> '1',
					'slug'			=>	Inflector::slug(strtolower(str_replace(' ', '', $fb_user['name']))),
					'gender'			=>	$genderr,
					'name'			=>	$fb_user['name'],
					'first_name'			=>	$fb_user['first_name'],
					'last_name'			=>	$fb_user['last_name'],
					'birthdate'			=>	$mysql_date,
					'active'		=> '1',
					'uid'			=> $fb_user['id'],
					'fbid'			=> $fb_user['id'],
					'photo'			=> $fb_user['id'].".jpg"
                );
				
			
				$this->User->recursive = -1;
				
				$userCheck = $this->User->find('first', array('conditions'=>array(
					'fbid'=>$fb_user['id'])));
					
			
		if($fb_user['id']==$userCheck['User']['fbid'])
		{
				$user = $this->User->read(null, $userCheck['User']['id']);
				
				$this->Session->write('Auth', $user);
				
					$img = file_get_contents('https://graph.facebook.com/'.$fb_user['id'].'/picture?width=140&height=140');
								
					$img_thumbs = file_get_contents('https://graph.facebook.com/'.$fb_user['id'].'/picture?width=16&height=16');

					$file = "img".DS."users".DS."".$fb_user['id'].".jpg";
					$file_thumbs = "img".DS."users".DS."small".DS."".$fb_user['id'].".jpg";

					file_put_contents($file, $img);
					file_put_contents($file_thumbs, $img_thumbs);
				
				
				$this->Auth->loggedIn();
		if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile/') !== false) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari/') == false) || ($_SERVER['HTTP_X_REQUESTED_WITH'] == "com.company.app")) {
		    echo '<script>window.location.replace("http://clubbie.nl/win");</script>';
		}
				echo "		<style>
						#BLOCKQUOTE_1 {
						    color: rgb(85, 85, 85);
						  
						    text-align: justify;
						
						  
						    background: rgb(242, 242, 242) none repeat scroll 0% 0% / auto padding-box border-box;
						    border: 2px solid rgb(85, 85, 85);
						    border-radius: 10px 10px 10px 10px;
						    font: normal normal normal normal 18.2000007629395px/23.6599998474121px Arial, Helvetica, sans-serif;
						    margin: 18.2000007629395px 20px;
						    outline: rgb(85, 85, 85) none 0px;
						    padding: 10px;
						}/*#BLOCKQUOTE_1*/

						#P_2 {
						    color: rgb(85, 85, 85);
						    height: 69px;
						    text-align: justify;
						  
						    border: 0px none rgb(85, 85, 85);
						    font: normal normal normal normal 18.2000007629395px/23.6599998474121px Arial, Helvetica, sans-serif;
						    margin: 0px;
						    outline: rgb(85, 85, 85) none 0px;
						}/*#P_2*/

				</style>
						<meta name=\"viewport\" content=\"width=device-width,initial-scale=1\">

						<blockquote id=\"BLOCKQUOTE_1\">
							<p id=\"P_2\">
								<b>Je bent succesvol ingelogd.</b> <br /> Sluit dit scherm om verder te gaan.
							</p>
						</blockquote>
						<script type=\"text/javascript\">
						window.close();
						</script>";  

				
		}else{
					 
					if($this->User->save($data, array('validate' => false))) {
						
								$userId = $this->User->getLastInsertID();
								
								//change slug to userId
								$this->User->id = $userId;
								$slugData['User'] = array('slug' => Inflector::slug($userId));
								$this->User->save( $slugData, array('validate' => false) );
								
								$user = $this->User->read(null, $userId);
						 
						 
						 		$img = file_get_contents('https://graph.facebook.com/'.$fb_user['id'].'/picture?width=75&height=75');
								$img_thumbs = file_get_contents('https://graph.facebook.com/'.$fb_user['id'].'/picture?width=16&height=16');
					
								$file = "img".DS."users".DS."".$fb_user['id'].".jpg";
								$file_thumbs = "img".DS."users".DS."small".DS."".$fb_user['id'].".jpg";
					
								file_put_contents($file, $img);
								file_put_contents($file_thumbs, $img_thumbs);
								
								
								/** default credits for wingame */
								$dataCredit['Credit'] = array(
									'user_id' => $userId
								);
								$this->Credit->save($dataCredit);
					
								/*$this->data['User']['id'] = $this->User->getLastInsertID();
								$this->data['User']['photo'] = $fb_user['id'].".jpg";
					
								$this->User->save($this->data['User']); */
						 
						 
						 
						 //$this->__sendWelcomeMessage($user);
						 
						 
//EMAIL STUREN
						 
				/*	 $email = new CakeEmail();
					$email->viewVars(array(
						'user' => $fb_user['name'] ,
						'pass' => $randPass
					));
					$email->from(Configure::read('evento_settings.systemEmail'));
					$email->to($fb_user['email']);
					$email->subject(__('Facebook registration'));
					$email->template('facebook_welcome');
					$email->emailFormat('both');
					$email->send(); 
						 */
						 			 
						 
						 //echo "<pre>";
						 //print_r($user);
						// echo "</pre>";
						 
						 
						 //echo $user['User']['slug'];
						 
						$this->Session->write('Auth', $user);
						$this->Auth->loggedIn();
								
						?>
						<script type="text/javascript"> 
						window.close();
						</script>
					<?php	 		
						
					}else{
								echo "Wrong function called";
						} 
		}
		//die('test');
		}
	}
	
	/**
	 * check if username exist, if exist return with incremental number trailing
	 *
	 * @access private
	 */
	private function _checkUsername($userName) {
		
		$users = $this->User->find('all', array('conditions'=>array(
			'User.username'=>$userName
		)));
		
		if(empty($users)) {
			return $userName;
		}
		
		return $userName . sizeof($users);
		
	}
	
	/**
	 * User login, if user is already logged in then redirect to events index.
	 *
	 * @access public
	 */

	public function login() {
		
		
		if($this->Auth->user()) $this->redirect(array('controller'=>'events', 'action'=>'index'));
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirect());
			}
			else {
				$this->Session->setFlash(__('Invalid username or password.'), 'default', array(), 'auth');
			}
		}
		$this->set('title_for_layout', __('users login'));
		$this->set('redirect', $this->Auth->redirect());
	}

	/**
	 * User login, if user is already logged in then redirect to events index.
	 *
	 * @access public
	 */

	public function organisation_login() {
		
		
		if($this->Auth->user()) $this->redirect(array('controller'=>'events', 'action'=>'index'));
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirect());
			}
			else {
				$this->Session->setFlash(__('Invalid username or password.'), 'default', array(), 'auth');
			}
		}
		$this->set('title_for_layout', __('Organisation login'));
		$this->set('redirect', $this->Auth->redirect());
	}


	/**
	 * User logout and redirect to homepage.
	 *
	 * @access public
	 */

	public function logout() {
		$this->redirect($this->Auth->logout());
	}

	/**
	 * User index displays a paginated list of users.
	 *
	 * @access public
	 */

	public function index() {
		$this->User->recursive = -1;
		$this->paginate['fields'] = array('username', 'slug', 'photo');
		$this->set('users', $this->paginate('User', array('active'=>true)));
		$this->set('title_for_layout', __('Users'));
		

		// user search
		if($this->request->data) {
			if(!$this->Auth->user('id')) throw new NotFoundException();
			$this->User->recursive = -1;
			$user = $this->User->find('first',
				array('conditions'=>array('or'=>array('email'=>$this->request->data['User']['search'],
					'username'=>$this->request->data['User']['search'])), 'fields'=>'slug'));
			if(!empty($user)) {
				$this->redirect(array('action'=>'view', $user['User']['slug']));
			}
			else {
				$this->User->invalidate('search');
			}
		}
	}

	/**
	 * User registration.
	 * First user will have admin permissions. If option is set users will need to confirm
	 * the provided email address.
	 *
	 * @access public
	 */

	public function register() {
		if(Configure::read('evento_settings.adminAddsUsers')==true) throw new NotFoundException();
		if($this->Auth->user()) $this->redirect(array('controller'=>'events','action'=>'index'));

		$useRecaptcha = false;
		if(Configure::read('evento_settings.recaptchaPublicKey')
		&& Configure::read('evento_settings.recaptchaPrivateKey')) {
			$recaptcha = $this->Components->load('Recaptcha.Recaptcha');
			$recaptcha->enabled = true;
			$recaptcha->initialize($this);
			$recaptcha->startup($this);
			$useRecaptcha = true;
		}

		if ($this->request->data) {
			if(!$useRecaptcha || ($useRecaptcha && $recaptcha->verify())) {
				$this->request->data['User']['recaptcha'] = 'correct';
			}
			if(0 == $this->User->find('count')) $this->request->data['User']['admin'] = 1;
			else $this->request->data['User']['admin'] = 0;

			if(Configure::read('evento_settings.validateEmails')==true) {
				$validationCode = sha1(rand().'-'.time());
				$this->request->data['User']['validation_code'] = $validationCode;
				$this->request->data['User']['validation_date'] = date('Y-m-d H-i-s');
				$this->request->data['User']['active'] = 0;
			}

			if($this->User->save($this->request->data)) {
			
				$userId = $this->User->getLastInsertID();
				$this->User->recursive = -1;
				$user = $this->User->find('first', array('conditions'=>array(
					'id'=>$this->User->getLastInsertID())));
				
				//update slug = is user id
				$this->User->id = $userId;
				$this->User->save(array('User' => array(
					'slug' => Inflector::slug($user['User']['id'] ))
				));
				
				if(Configure::read('evento_settings.validateEmails')==true) {
					$this->set('code',$user['User']['validation_code']);
					$email = new CakeEmail();
					$email->viewVars(array(
						'code' => $user['User']['validation_code'],
						'user' => $user['User']['username']
					));
					$email->from(Configure::read('evento_settings.systemEmail'));
					$email->to($user['User']['email']);
					$email->subject(__('Email confirmation'));
					$email->template('email_confirmation');
					$email->emailFormat('both');
					$email->send();
				}
				else {
					//$this->__sendWelcomeMessage($user);
					$this->Auth->login();
					if($user['User']['admin']) {
						$this->redirect(array('admin'=>true, 'controller'=>'settings',
							'action'=>'index'));
					}
					else {
						$this->redirect(array('controller'=>'users', 'action'=>'view',
							$user['User']['id']));
					}
				}
			}
		}
		$this->set('useRecaptcha', $useRecaptcha);
		$this->set('title_for_layout', __('Registration form'));
	}
/**
	 * User registration.
	 * First user will have admin permissions. If option is set users will need to confirm
	 * the provided email address.
	 *
	 * @access public
	 */

	public function organisation_register() {
		if(Configure::read('evento_settings.adminAddsUsers')==true) throw new NotFoundException();
		if($this->Auth->user()) $this->redirect(array('controller'=>'events','action'=>'index'));

		$useRecaptcha = false;
		if(Configure::read('evento_settings.recaptchaPublicKey')
		&& Configure::read('evento_settings.recaptchaPrivateKey')) {
			$recaptcha = $this->Components->load('Recaptcha.Recaptcha');
			$recaptcha->enabled = true;
			$recaptcha->initialize($this);
			$recaptcha->startup($this);
			$useRecaptcha = true;
		}

		if ($this->request->data) {
			if(!$useRecaptcha || ($useRecaptcha && $recaptcha->verify())) {
				$this->request->data['User']['recaptcha'] = 'correct';
			}
			if(0 == $this->User->find('count')) $this->request->data['User']['admin'] = 1;
			else $this->request->data['User']['admin'] = 0;

			if(Configure::read('evento_settings.validateEmails')==true) {
				$validationCode = sha1(rand().'-'.time());
				$this->request->data['User']['validation_code'] = $validationCode;
				$this->request->data['User']['validation_date'] = date('Y-m-d H-i-s');
				$this->request->data['User']['active'] = 0;
			}

			if($this->User->save($this->request->data)) {
			
				$userId = $this->User->getLastInsertID();
				$this->User->recursive = -1;
				$user = $this->User->find('first', array('conditions'=>array(
					'id'=>$this->User->getLastInsertID())));
				
				//update slug = is user id
				$this->User->id = $userId;
				$this->User->save(array('User' => array(
					'slug' => Inflector::slug($user['User']['id'] ))
				));
				
				if(Configure::read('evento_settings.validateEmails')==true) {
					$this->set('code',$user['User']['validation_code']);
					$email = new CakeEmail();
					$email->viewVars(array(
						'code' => $user['User']['validation_code'],
						'user' => $user['User']['username']
					));
					$email->from(Configure::read('evento_settings.systemEmail'));
					$email->to($user['User']['email']);
					$email->subject(__('Email confirmation'));
					$email->template('email_confirmation');
					$email->emailFormat('both');
					$email->send();
				}
				else {
					//$this->__sendWelcomeMessage($user);
					$this->Auth->login();
					if($user['User']['admin']) {
						$this->redirect(array('admin'=>true, 'controller'=>'settings',
							'action'=>'index'));
					}
					else {
						$this->redirect(array('controller'=>'users', 'action'=>'view',
							$user['User']['id']));
					}
				}
			}
		}
		$this->set('useRecaptcha', $useRecaptcha);
		$this->set('title_for_layout', __('Registration form'));
	}

	/**
	 * View user's profile
	 *
	 * @param string $user_slug
	 */

	public function view($user_slug= null, $mode = null) {
		if(!$user_slug) throw new NotFoundException();
		if(Configure::read('evento_settings.disableAttendees') == 1) {
			$mode = 'posted';
		}
		$user = $this->User->find('first', array('conditions'=>array('User.slug'=>$user_slug),
			'joins' => array(
				array(
					'table' => 'cities',
					'alias' => 'City',
					'type' => 'left',
					'conditions' => array('City.id = User.city_id')
				),
				array(
					'table' => 'countries',
					'alias' => 'Country',
					'type' => 'left',
					'conditions' => array('Country.id = City.country_id')
				)),
			'fields'=>array(
				'User.id', 'User.username', 'User.first_name', 'User.last_name', 'User.photo', 'User.created', 'User.web', 'User.slug', 'City.name',
				'City.slug', 'Country.name', 'Country.slug')
		));
		if(empty($user)) throw new NotFoundException();

		$this->paginate['joins'] = array(
			array(
				'table' => 'events',
				'alias' => 'Event',
				'type' => 'left',
				'conditions' => array('Event.id = EventsUser.event_id')
			),
			array(
				'table' => 'venues',
				'alias' => 'Venue',
				'type' => 'left',
				'conditions' => array('Venue.id = Event.venue_id')
			),
			array(
				'table' => 'cities',
				'alias' => 'City',
				'type' => 'left',
				'conditions' => array('City.id = Venue.city_id')
			),
			array(
				'table' => 'countries',
				'alias' => 'Country',
				'type' => 'left',
				'conditions' => array('Country.id = City.country_id')
			)
		);

		$model = 'EventsUser';
		$this->paginate['limit'] = 10;
		$this->paginate['fields'] = array('Event.name', 'Event.slug', 'Event.id', 'Event.start_date',
			'Event.end_date', 'Venue.name','Venue.slug', 'Venue.address',
			'Country.name', 'Country.slug', 'City.name', 'City.slug');

		$this->paginate['recursive'] = -1;
		$this->paginate['order'] = 'Event.start_date ASC';
		$conditions = array('Event.published' => true);

		if($mode == 'posted') {
			$model = 'Event';
			$conditions['Event.user_id'] = $user['User']['id'];
			array_shift($this->paginate['joins']);
		}
		else if($mode == 'past') {
			$conditions['EventsUser.user_id'] = $user['User']['id'];
			$conditions['Event.end_date <'] = date('Y-m-d');
		}
		else {
			$conditions['EventsUser.user_id'] = $user['User']['id'];
			$conditions['Event.end_date >='] = date('Y-m-d');
		}

		$user['Attendee'] = $this->paginate($model, $conditions);
		if($user['User']['id'] == $this->Auth->user('id')) {
				$homepage = true;
		}
		else {
			$homepage = false;
		}
		
		$joins = array(
			array(
				'table' => 'events',
				'alias' => 'Event',
				'type' => 'left',
				'conditions' => array('Event.id = Participant.event_id')
			),
			array(
				'table' => 'venues',
				'alias' => 'Venue',
				'type' => 'left',
				'conditions' => array('Venue.id = Event.venue_id')
			),
			array(
				'table' => 'cities',
				'alias' => 'City',
				'type' => 'left',
				'conditions' => array('City.id = Venue.city_id')
			),
			array(
				'table' => 'countries',
				'alias' => 'Country',
				'type' => 'left',
				'conditions' => array('Country.id = City.country_id')
			)
		);
		$fields = array('Event.*', 'City.*', 'Venue.*', 'Country.*', 'Participant.*');
		
		$userGames = $this->Participant->find('all', array('conditions'=>array(
			'Participant.user_id'=>$this->Auth->user('id')
		),
		'order'=>array('Participant.part_date ASC'),
		'group'=>array('Participant.event_id'),
		'joins'=>$joins,
		'fields'=>$fields
		));
		
		$eventFields = array('Participant.event_id');
		$myWinEvent = $this->Participant->find('all', array('conditions'=>array(
			'Participant.user_id'=>$this->Auth->user('id'), 'Participant.status'=>'win'
		),
		'fields'=>$eventFields));
		
		$arrayEventId = array();
		if(!empty($myWinEvent)) {
			foreach($myWinEvent as $evtId){
				$arrayEventId[] = $evtId['Participant']['event_id'];
			}
		}
		
		$newUserGames = array();
		$loseGames = array();
		
		if(!empty($userGames)) {
			$i = 0;
			foreach($userGames as $games) {
				
				$statusLose = 'lose';
				
				$count = (5 - $this->Participant->find('count', array('condition' => array(
					'user_id' => $this->Auth->user('id')
				))));
				
				if(!in_array($games['Participant']['event_id'], $arrayEventId)) {
					
					if(time() > strtotime($games['Participant']['part_date'] . "+24 hours")){
						if(time() > strtotime($games['Event']['win_end_date'])) {
							$loseGames[$i] = $games;
							$loseGames[$i]['Event']['status'] = $statusLose;
							$loseGames[$i]['Participant']['count'] = $count;
						} else {
							$newUserGames[$i] = $games;
							$newUserGames[$i]['Event']['status'] = $statusLose;
							$newUserGames[$i]['Participant']['count'] = $count;
						}
					} else {
						if(strtotime($games['Participant']['part_date'] . "+24 hours") < strtotime($games['Event']['win_end_date'])) {
							$newUserGames[$i] = $games;
							$newUserGames[$i]['Event']['status'] = $statusLose;
							$newUserGames[$i]['Participant']['count'] = $count;
						} else {
							$loseGames[$i] = $games;
							$loseGames[$i]['Event']['status'] = $statusLose;
							$loseGames[$i]['Participant']['count'] = $count;
						}
					}
					
				} else {
					$newUserGames[$i] = $games;
					$newUserGames[$i]['Event']['status'] = 'win';
					$newUserGames[$i]['Participant']['count'] = $count;
				}
				
				$i++;
			}
		}
		
		$this->set('user', $user);
		$this->set('homepage', $homepage);
		$this->set('title_for_layout', __(sprintf("%s's profile", ucfirst($user['User']['username']))));
		$this->set('mode', $mode);
		$this->set('userGames', array_merge($newUserGames, $loseGames));
	}

	/**
	 * Edit user's profile
	 *
	 * @access public
	 */

	public function edit($quick=FALSE, $eventId=NULL) {
		
		$quickmode = (isset($quick) && $quick=='quick') ? TRUE : FALSE;
		
		$this->User->contain('City');
		$user = $this->User->find('first', array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
		unset($user['User']['password']);
		
		if(!empty($this->request->data)) {
			if(isset($this->request->data['User']['delete_photo']) && $this->request->data['User']['delete_photo']) {
				$this->request->data['User']['filedata'] = '';
			}

			if(empty($this->request->data['User']['password'])) {
				unset($this->request->data['User']['password']);
			}
			$this->request->data['User']['id'] = $this->Auth->user('id');
			$this->request->data['User']['admin'] = $this->Auth->user('admin');
			$this->request->data['User']['username'] = $user['User']['username'];

			// user changed email and needs confirmation
			if(isset($this->request->data['User']['email'])) {
				if($user['User']['email']!=$this->request->data['User']['email']
					&& Configure::read('evento_settings.validateEmails')==true) {
						$this->request->data['User']['alter_email'] = $this->request->data['User']['email'];
						unset($this->request->data['User']['email']);
						$validationCode = sha1(rand() . '-' . time());
						$this->request->data['User']['validation_code'] = $validationCode;
						$this->request->data['User']['validation_date'] = date('Y-m-d H-i-s');
				}
			}

			if($this->User->save($this->request->data)) {
				$this->User->recursive = -1;
				$user = $this->User->find('first', array('conditions'=>array(
					'id'=>$this->Auth->user('id'))));

				// email confirmation
				if(isset($this->request->data['User']['alter_email']) &&
				Configure::read('evento_settings.validateEmails')==true) {
					$email = new CakeEmail();
					$email->viewVars(array(
						'code' => $user['User']['validation_code'],
						'user' => $user['User']['username']
					));
					$email->from(Configure::read('evento_settings.systemEmail'));
					$email->to($this->request->data['User']['alter_email']);
					$email->subject(__('Email confirmation'));
					$email->template('email_confirmation');
					$email->emailFormat('both');
					$email->send();

					$this->set('email_confirmation', $this->request->data['User']['alter_email']);
				}
				else {
					
					$user = $this->User->find('first', array('conditions'=>array(
						'id'=>$this->Auth->user('id'))));
					
					if($quickmode)
					{
						$event = $this->Event->find('first', array('conditions'=>array(
							'Event.id'=>$eventId)));
						$fullEvt = $this->Event->viewEvent($event['Event']['slug']);
						
						$evtSlug = $fullEvt['Country']['slug'].'/'.$fullEvt['City']['slug'].'/'.$fullEvt['Venue']['slug'].'/'.$fullEvt['Event']['slug'];
						$this->redirect(array('controller'=>'events', 'action'=>'win/'.$evtSlug));
					}
					else
						$this->redirect(array('action'=>'view', $user['User']['slug']));
				}
			}
			else {
				//could not save data, reset some variables
				if(isset($this->request->data['User']['alter_email'])) {
					$this->request->data['User']['email'] = $this->request->data['User']['alter_email'];
				}
				if(!isset($this->request->data['City']['country_id'])) {
					$this->request->data['City']['country_id'] = null;
				}
			}
		}
		else {
			$this->request->data = $user;
			unset($this->request->data['User']['id']);
		}
		
		$this->set('title_for_layout', __('Edit user profile'));
		$this->set('user', $user);
		$this->set('countries',$this->Country->find('countrylist'));
		$this->set('quickmode', $quickmode);

		
		$url = 'edit';
		if($quickmode)
		{
			$this->set('eventId', $eventId);
			$url = 'edit/quick/'.$eventId ;
		}
		
		$this->set('url', $url);

		
	}

	/**
	 * Delete user account after confirmation. It uses delete_user in users model
	 * to make sure we delete all user data.
	 *
	 * @access public
	 * @param string $confirmation
	 */

	public function delete_user($confirmation = null) {
		if($confirmation!=null) {
			$this->User->delete($this->Auth->user('id'), true);
			$this->redirect($this->Auth->logout());
		}
	}

	/**
	 * If user forgot the password we create a new url for him
	 * to access his account without password.
	 *
	 * @access public
	 */

	public function recover() {
		if(!empty($this->request->data)) {
			$email = $this->request->data['User']['email'];
			$this->User->recursive = -1;
			$user = $this->User->find('first', array('conditions'=>array('email'=>$email),
				'fields'=>array('id','username','email')));
			if(!empty($user)) {
				$validationCode = sha1(rand().'-'.time());
				$user['User']['validation_code'] = $validationCode;
				$user['User']['validation_date'] = date('Y-m-d H-i-s');
				if($this->User->save($user)) {
					$email = new CakeEmail();
					$email->viewVars(array(
						'code' => $validationCode,
						'user' => $user
					));
					$email->from(Configure::read('evento_settings.systemEmail'));
					$email->to($user['User']['email']);
					$email->subject(__('Password recovery'));
					$email->template('password_recovery');
					$email->emailFormat('both');
					$email->send();
				}
			}
			$this->set('recover', true);
		}
	}

	/**
	 * login a user without using password with the random code
	 * created with the recover action.
	 *
	 * The access code is valid for 2 hours only.
	 *
	 * @access public
	 * @param string $code
	 */

	public function code_login($code = null) {
		if(!$code) throw new NotFoundException();
		$this->User->recursive = -1;
		$user = $this->User->find('first', array('conditions'=>array('validation_code'=>$code)));
		if(!empty($user) &&
		$user['User']['validation_date']<date('Y-m-d H-i-s', strtotime('+2 hours'))) {
			$this->Auth->login($user['User']);
			$remove_code['User']['id'] = $user['User']['id'];
			$remove_code['User']['validation_code'] = null;
			$remove_code['User']['validation_date'] = null;
			$this->User->save($remove_code);
			$this->redirect(array('action'=>'edit'));
		}
		else{
			$this->set('code_login',false);
		}
	}

	/**
	 * confirm the email address is valid and activate the user account
	 *
	 * @param int $code
	 */

	public function activation($code = null) {
		if(!$code) throw new NotFoundException();
		$this->User->recursive = -1;
		$user = $this->User->find('first', array('conditions'=>array('validation_code'=>$code)));
		if(!empty($user)
		&& $user['User']['validation_date'] < date('Y-m-d H-i-s', strtotime('+24 hours'))) {
			if($user['User']['alter_email']) {
				$remove_code['User']['email'] = $user['User']['alter_email'];
				$remove_code['User']['alter_email'] = null;
			}
			$remove_code['User']['id'] = $user['User']['id'];
			$remove_code['User']['active'] = 1;
			$remove_code['User']['validation_code'] = null;
			$remove_code['User']['validation_date'] = null;

			if($this->User->save($remove_code)) {
				if(!$this->Auth->user('id')) {
					if(!$user['User']['active']) $this->__sendWelcomeMessage($user);
					$this->Auth->login($user['User']);
				}
			}
		}
		else{
			throw new NotFoundException();
		}
	}

	////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////  ADMIN
	////////////////////////////////////////////////////////////////

	/**
	 * admin index paginates a list of users
	 *
	 * @access public
	 */

	public function admin_index() {
		$this->set('title_for_layout', __('Manage users'));
		$this->Session->delete('Search.term');
		$this->paginate['fields'] = array('User.id', 'User.username', 'User.slug', 'User.photo', 'User.active');
		$countusers = $this->User->find('all');
		$this->set('countusers', $countusers);
		$this->set('users',$this->paginate('User'));
	}

	/**
	 * Admin delete users
	 *
	 * @access public
	 * @param int $userId
	 */

	public function admin_delete_user($userId = null) {
		if($userId === null) throw new NotFoundException();
		$this->User->recursive = -1;
		$user = $this->User->find('first', array('conditions'=>array('id'=>$userId), 'fields'=>array('id')));
		if(empty($user)) {
			throw new NotFoundException();
		}
		else {
			$this->User->delete($userId, true);

			$page = isset($this->request->params['named']['page'])? $this->request->params['named']['page']: null;
			$conditions = null;
			$action = 'index';
			if($this->Session->read('Search.term')) {
				$action = 'search';
				$term = $this->Session->read('Search.term');
				$conditions = array('or'=>array('User.email like'=>'%' . $term['Search']['user'] . '%',
					'User.username'=>$term['Search']['user'] . '%'));
			}
			if($page) {
				$count = $this->User->find('count', array('conditions'=>$conditions));
				$lastPage = ceil($count / $this->paginate['limit']);
				if($page > $lastPage) $page = $lastPage;
			}
			$this->redirect(array('action'=>$action, 'page' => $page));
		}
	}

	/**
	 * Admin can edit user data
	 *
	 * @access public
	 * @param int $userId
	 */

	public function admin_edit($userId = null) {
		if($userId === null) throw new NotFoundException();
		$this->User->recursive = -1;
		$user = $this->User->find('first', array('conditions'=>array('id'=>$userId),
			'fields'=>array('id', 'username', 'email', 'active', 'admin', 'photo', 'gender', 'birthdate', 'first_name', 'last_name')));

		if(empty($user)) {
			throw new NotFoundException();
		}
				$participants = $this->Participant->find('all', array(
			'conditions' => array('Participant.user_id' => $event['User']['id'])
		));
//----

//----
		$page = isset($this->request->params['named']['page'])? $this->request->params['named']['page']: null;
		if(!empty($this->request->data)) {
			$this->request->data['User']['id'] = $userId;
			if(!$this->request->data['User']['password']) {
				unset($this->request->data['User']['password']);
				unset($this->request->data['User']['password_confirm']);
			}
			if(isset($this->request->data['User']['delete_photo']) && $this->request->data['User']['delete_photo']) {
				$this->request->data['User']['filedata'] = '';
			}
			if($this->User->save($this->request->data)) {
				$this->redirect(array('action'=>'index', 'page'=>$page));
			}
		}
		$this->request->data = $user;
		$this->set('page', $page);
		$this->set('title_for_layout', __('Edit user'));
		$participants = $this->Participant->find('all', array(
			'conditions' => array('Participant.user_id' => $userId),
			'order' => array('Participant.status ASC')
		));
		
		$this->set('participants', $participants);


	}

	/**
	 * export users as plain text file
	 */

	public function admin_export() {
		Configure::write('debug', 0);
		$this->layout = 'export';
		$this->response->header("Cache-Control: private");
		$this->response->header("Content-Description: File Transfer");
		$this->response->header("Content-Disposition: attachment; filename=users.csv");
		$this->RequestHandler->respondAs('csv');
		$joins = array(
			array(
				'table' => 'cities',
				'alias' => 'City',
				'type' => 'left',
				'conditions' => array('City.id = User.city_id')
			),
			array(
				'table' => 'countries',
				'alias' => 'Country',
				'type' => 'left',
				'conditions' => array('Country.id = City.country_id')
			));
		$users = $this->User->find('all', array('fields'=>array('User.username', 'User.email',
			'User.web', 'City.name', 'Country.name'), 'joins'=>$joins));
		$this->set('users', $users);
	}

	/**
	 * allow admin to add new users
	 */

	public function admin_register() {		
		if(!empty($this->request->data)) {
			$this->request->data['User']['recaptcha'] = 'correct';
			$this->request->data['User']['admin'] = 0;
			$this->request->data['User']['active'] = 1;
			if($this->User->save($this->request->data)) {
			
				$userId = $this->User->getLastInsertID();
				$this->User->recursive = -1;
				$user = $this->User->find('first', array('conditions'=>array(
					'id'=>$this->User->getLastInsertID())));
				
				//update slug = is user id
				$this->User->id = $userId;
				$this->User->save(array('User' => array(
					'slug' => Inflector::slug($user['User']['id'] ))
				));
				$this->redirect(array('action'=>'index'));
			}
		}
		$this->set('title_for_layout', __('Add user'));
	}

	/**
	 * allow admin to search a user
	 */

	public function admin_search() {
		$this->set('title_for_layout', __('Search'));
		$data = $this->Session->read('Search.term');
		if(!empty($this->request->data) || !empty($data)) {
			if($this->request->data) {
				$this->Session->write('Search.term', $this->request->data);
			}
			else {
				$this->request->data = $data;
			}
			$conditions = array('or'=>array('User.email like'=>'%' . $this->request->data['Search']['user'] . '%',
				'User.username'=>$this->request->data['Search']['user'] . '%'));
			$this->set('users', $this->paginate('User', $conditions));
		}
		$this->render('admin_index');
	}

	/**
	 * bulk manage users
	 */

	public function admin_bulk() {
		if(empty($this->request->data)) throw new NotFoundException();
		$ids = array();
		foreach($this->request->data['User']['id'] as $key => $value) {
			if($value != 0) {
				$ids[] = $key;
			}
		}
		switch($this->request->data['User']['option']) {
			case 'activate':
				$this->User->bulkPublish($ids, 1);
				break;
			case 'deactivate':
				$this->User->bulkPublish($ids, 0);
				break;
			case 'delete':
				$this->User->bulkDelete($ids);
				break;
		}
		$this->redirect(array('action'=>'index'));
	}

	/**
	 * function to send a welcome message to new users
	 *
	 * @param array $user
	 */

	private function __sendWelcomeMessage($user) {
		$email = new CakeEmail();
		$email->viewVars(array(
			'user' => $user['User']['username']
		));
		$email->from(Configure::read('evento_settings.systemEmail'));
		$email->to($user['User']['email']);
		$email->subject(sprintf(__('Welcome to %s'), Configure::read('evento_settings.appName')));
		$email->template('welcome_message');
		$email->emailFormat('both');
		$email->send();
	}
}
?>