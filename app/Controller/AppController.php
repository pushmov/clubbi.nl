<?php
/* SVN FILE: $Id: app_controller.php 6311 2008-01-02 06:33:52Z phpnut $ */
/**
 * Short description for file.
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework <http://www.cakephp.org/>
 * Copyright 2005-2008, Cake Software Foundation, Inc.
 *								1785 E. Sahara Avenue, Suite 490-204
 *								Las Vegas, Nevada 89104
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright 2005-2008, Cake Software Foundation, Inc.
 * @link				http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package			cake
 * @subpackage		cake.app
 * @since			CakePHP(tm) v 0.2.9
 * @version			$Revision: 6311 $
 * @lastmodified	$Date: 2009-04-19 $
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 * Short description for class.
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		cake
 * @subpackage	cake.app
 */

class AppController extends Controller {

	var $uses = array('Settings', 'User');
	var $components = array('Auth', 'Session', 'Cookie');
	var $helpers = array('Html', 'Form', 'Js', 'Session', 'Text');
	public $theme = 'Default';

	/**
	 * beforeFilter overload
	 * set login and logout actions
	 */

	public function beforeFilter() {
		// load site settings
		$this->_loadSettings();

		// set language
		$languageCookie = $this->Cookie->read('language');
		if(!$languageCookie || !array_key_exists($languageCookie, $this->Settings->getLanguages())) {
			Configure::write('Config.language', Configure::read('evento_settings.language'));
		}
		else {
			Configure::write('evento_settings.language', $languageCookie);
			Configure::write('Config.language', $languageCookie);
			$this->Cookie->write('language', $languageCookie, true, '1 month');
		}

		// set time zone
		if(Configure::read('evento_settings.timeZone')) {
			date_default_timezone_set(Configure::read('evento_settings.timeZone'));
		}
		
		// set auth options
		$this->Auth->userScope = array('User.active' => true);
		$this->Auth->logoutRedirect = array('admin'=>false, 'controller'=>'events', 'action'=>'index');
		
		
		$referer = Controller::referer();
		$authError = (!preg_match('/win/i', $referer)) ? 'Please log in to continue' : 'Log in to play';
		
		$this->Auth->authError = __( $authError );
		$this->Auth->loginAction = array(
			'controller'=>'users',
			'action'=>'login',
			'admin'=>0,
			'plugin'=>null);

		// check user session
		$this->_checkSession();

		// set theme
		$this->theme = Configure::read('evento_settings.theme');

		// set default page title
		$this->set('title_for_layout', __(Configure::read('evento_settings.appSlogan')));
		
		Configure::load('facebook');
		return parent::beforeFilter();
	}

	/**
	 * check if user is authorized or admin
	 * and set auth permissions
	 */

	private function _checkSession() {
		$this->Auth->deny('*');
		
		if($this->Auth->user()) {

			// if user has been deleted or deactivated force the logout
			$this->User->recursive = -1;
			$user = $this->User->find('first', array('conditions'=>array('User.id'=>$this->Auth->user('id'),
				'User.active'=>true), 'fields'=>array('User.admin')));
			
			if(empty($user) || ($this->Auth->user('admin')==true && $user['User']['admin']==false)) {
				$this->redirect($this->Auth->logout());
			}
			
			if(isset($this->request->params['admin'])) {
				if(!$this->Auth->user('admin')) throw new NotFoundException();
				$this->layout = "admin_default";
			}
		}
	}

	/**
	 * load application settings
	 */

	private function _loadSettings() {
		$settings = $this->Settings->getSettings();
		Configure::write('evento_settings', $settings);
		if(Configure::read('evento_settings.recaptchaPublicKey')) {
			Configure::write('Recaptcha.publicKey', Configure::read('evento_settings.recaptchaPublicKey'));
			Configure::write('Recaptcha.privateKey', Configure::read('evento_settings.recaptchaPrivateKey'));
		}
	}
}
?>