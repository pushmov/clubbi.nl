<?php
/**
 * @copyright Copyright 2008 Evento
 */

App::uses('CakeEmail', 'Network/Email');
$dirname = dirname(dirname(__FILE__));
require_once("$dirname/vendors/Facebook/facebook.php");

class CreditsController extends AppController {

	var $name = 'Credits';
	var $uses = array('Event', 'Tag', 'EventsTag', 'Country', 'City', 'User', 'Settings', 'Category', 'Participant', 'Roll', 'Credit', 'Tasks', 'Credithistory');
	var $helpers = array('Html', 'Form', 'Calendar', 'Time', 'Text', 'Timeformat',
		'Bookmark', 'Js');
	var $components = array('RequestHandler', 'Calendar');
	var $paginate = array('limit' => 100, 'order' => array('Event.start_date' => 'ASC',
		'Event.id'=>'DESC'));

	/**
	 * Overload beforeFilter to set some permissions
	 */

	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function index(){
		
		$this->set('title_for_layout', __('Get Credit'));
		
		$fbConfig = Configure::read('Facebook');
		$appId 			= $fbConfig['appId'];
		$app_secret	= $fbConfig['secret'];
		
		$this->set('appId', $appId);
		$this->set('appSecret', $app_secret);
		
		
		$date = date('Y-m-d H:i:s');
		$tasks = $this->Tasks->find('all', array('conditions' => array(
			'start_date <= ' => $date,
			'end_date >= ' => $date
		)));
		
		
		$this->set('tasks', $tasks);
		$this->set('userId', $this->Auth->user('id'));
	}
	
	public function addCredit(){
		
		$userId = $this->request->data['userId'];
		$taskId = $this->request->data['taskId'];
		
		$taskRow = $this->Tasks->find('first', array('conditions' => array(
			'id' => $taskId
		)));
		
		$creditRow = $this->Credit->find('first', array('conditions' => array(
			'user_id' => $userId
		)));
		
		//validate if user already achieve this task :
		$test = $this->Credithistory->find('first', array('conditions' => array(
			'user_id' => $userId, 'task_id' => $taskId
		)));
		
		if(empty($test)) {
			
			$dateTime = date('Y-m-d H:i:s');
			$dayDuration = $taskRow['Tasks']['duration'];
			$creditAmount = $taskRow['Tasks']['credits'];
			
			$userCurrentCredit = $creditRow['Credit']['current_credits'];
			$userMaxCredits = $creditRow['Credit']['max_credits'];
			
			
			$dataAchievement = array('Credithistory' => array(
				'user_id' => $userId,
				'task_id' => $taskId,
				'date_achieve' => $dateTime,
				'date_expired' => date('Y-m-d H:i:s', strtotime($dateTime . "+$dayDuration days"))
			));
			
			// insert user achieved task
			$this->Credithistory->save($dataAchievement);
			
			$dataCredits = array('Credit' => array(
				'current_credits' => $userCurrentCredit + $creditAmount,
				'max_credits' => $userMaxCredits + $creditAmount
			));
			
			$this->Credit->id = $creditRow['Credit']['id'];
			$this->Credit->save($dataCredits);
			
			exit();
		}
		
		exit();
	}
	
	
}