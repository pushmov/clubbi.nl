<?php

class ResetcreditShell extends AppShell {
	
	var $uses = array('User', 'Credit');
	
	function main(){
		
		//reset user credit task every day 12pm
		//to do : 
		//seed users
		$usersList = $this->User->find('all');
		
		if(!empty($usersList)) {
			
			foreach($usersList as $user) {
				$defaultCredit = array();
				$userCredit = $this->Credit->find('first', array('conditions' => array(
					'user_id' => $user['User']['id']
				)));
				
				// echo var_dump(empty($userCredit));
				// echo '\n';
				
				if(empty($userCredit)) {
					$this->Credit->create();
					//insert default credit
					$defaultCredit['Credit'] = array(
						'user_id' => $user['User']['id']
					);
					$this->Credit->save( $defaultCredit );
					
					unset($defaultCredit);
				}
				
			}
		}
		
		// exit();
		
		$creditUserList = $this->Credit->find('all');
		
		if(!empty($creditUserList)) {
			
			foreach($creditUserList as $cUser) {
				
				if($cUser['Credit']['max_credits'] > $cUser['Credit']['current_credits']) {					
				
					$this->Credit->create();
					$maxCredit = $cUser['Credit']['max_credits'];
					
					$cUser['Credit']['current_credits'] = $maxCredit;
					
					$this->Credit->id = $cUser['Credit']['id'];
					$this->Credit->save($cUser);
					
					unset($cUser);
				}
			}
		}
		
	}
	
}