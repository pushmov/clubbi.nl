<?php

class ResettaskShell extends AppShell {
	
	var $uses = array('Credit', 'Credithistory');
	
	function main(){
		
		//every 1 min
		//to do : 
		//reset user's credit based on achievement expired date
		
		$joins = array(
			array(
				'table' => 'tasks',
				'alias' => 'Task',
				'type' => 'left',
				'conditions' => array('Task.id = Credithistory.task_id')
			)
		);
		
		$fields = array('Task.*', 'Credithistory.*');
		
		$expiredList = $this->Credithistory->find('all', array('conditions' => array(
			'Credithistory.date_expired <= ' => date('Y-m-d H:i:s')
		),
		'joins' => $joins,
		'fields' => $fields));
		
		
		if(!empty($expiredList)) {
			
			foreach($expiredList as $eList) {
				
				$rowCredit = $this->Credit->find('first', array('conditions' => array(
					'user_id' => $eList['Credithistory']['user_id']
				)));
				
				$maxCredits = $rowCredit['Credit']['max_credits'] - $eList['Task']['credits'];
				
				$this->Credit->create();
				$creditData = array('Credit' => array(
					'user_id' => $eList['Credithistory']['user_id'],
					'max_credits' => $maxCredits
				));
				
				//set current credits to max if current_credits >= max_credits
				if($rowCredit['Credit']['current_credits'] >= $maxCredits) {
					$creditData['current_credits'] = $maxCredits;
				}
				
				$this->Credit->id = $rowCredit['Credit']['id'];
				$this->Credit->save($creditData);
				unset($creditData);
				
			}
		}
		
	}
	
}