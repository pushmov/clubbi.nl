<p><?php echo sprintf(__('Welcome to %s'), Configure::read('evento_settings.appName')); ?></p>

<p><?php echo sprintf(__('You just signed up for a new account at %1$s with user name %2$s.'), $this->Html->link(Router::url('/', true), Router::url('/', true)), $user); ?></p>
...
<p><?php echo __("Beste voornaam"); ?> </p>
<p></p>
<p><?php echo __("Leuk dat je op Clubbie.nl een account hebt aangemaakt."); ?></p> 
<p><?php echo __("Op Clubbie.nl vind je naast een duidelijke party agenda ook een fantastische manier om kaarten en gastenlijstplaatsen voor jouw favoriete evenementen te winnen. Na het winactie spel: 'Roll the dice' gespeeld te hebben hoor je gelijk of je kaarten hebt gewonnen voor jou en een vriend(in). Probeer het gelijk uit en check onze winacties!"); ?> </p>
<p><?php echo __("Wij wensen jou onwijs veel plezier op onze website toe. Like ons ook op Facebook, Instagram en Twitter."); ?></p> 
<p></p>
<p><?php echo __("Groet,"); ?> </p>
<p><?php echo __("Team Clubbie.nl"); ?> </p>
