<p><?php echo __("Beste voornaam"); ?> </p>
<p></p>
<p><?php echo __("Gefeliciteerd je hebt gewonnen!"); ?></p> 
<p><?php echo __("Leuk dat je meegedaan hebt met de 'Roll the dice' winactie op Clubbie.nl! Jij hebt 2 kaarten voor ... in de ... op ... - ... gewonnen! Let op: de kaarten die je ontvangt van de organisatie zijn naamsgebonden, dus zorg ervoor dat je jouw identiteitsbewijs niet vergeet. De kaarten ontvang je in de week van het evenement."); ?> </p>
<p><?php echo __("Meer informatie over het evenement vind je natuurlijk op Clubbie.nl!"); ?> </p>
<p><?php echo __("Wij wensen jou en je vriend(in) onwijs veel plezier toe bij ..."); ?> </p>
<p></p>
<p><?php echo __("Groet,"); ?> </p>
<p><?php echo __("Team Clubbie.nl"); ?> </p>
