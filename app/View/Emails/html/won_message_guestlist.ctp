<p><?php echo __("Beste voornaam"); ?> </p>
<p></p>
<p><?php echo __("Gefeliciteerd je hebt gewonnen!"); ?></p> 
<p><?php echo __("Leuk dat je meegedaan hebt met de 'Roll the dice' winactie op Clubbie.nl! Jij hebt 2 gastenlijstplaatsen voor ... in de ... op ... - ... gewonnen! Let op: de gastenlijst sluit om ..., dus zorg dat je voor deze tijd binnen bent. Jouw naam staat +1 op de gastenlijst, dus je mag 1 vriend(in) meenemen. Vergeet je identiteitsbewijs niet."); ?> </p>
<p><?php echo __("Meer informatie over het evenement vind je natuurlijk op Clubbie.nl!"); ?> </p>
<p><?php echo __("Wij wensen jou en je vriend(in) onwijs veel plezier toe bij ..."); ?> </p>
<p></p>
<p><?php echo __("Groet,"); ?> </p>
<p><?php echo __("Team Clubbie.nl"); ?> </p>
