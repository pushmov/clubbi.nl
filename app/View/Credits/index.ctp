<div id="DIV_1">
	<img src="/img/header.png" id="IMG_2" alt='Clubbie, uitgaan en feesten in Amsterdam, Utrecht, Rotterdam.' />
</div>

<div class="container content">
	<?php echo $this->Session->flash(); ?>
	<div>
		<h3>Task Page</h3>


		<table class="table table-stripped"> 
			<thead>
				<tr>
					<th>Task Name</th>
					<th>Credit Increase</th>
					<th>Duration (day)</th>
					<th>Task End</th>
					<th>Action</th>
				</tr>
			</thead>
			
			<tbody>
			<?php if(!empty($tasks)) : ?>
			<?php foreach($tasks as $t) : ?>
				<tr>
					<td><?php echo $t['Tasks']['task']; ?></td>
					<td align="center"><?php echo $t['Tasks']['credits']; ?></td>
					<td align="center"><?php echo $t['Tasks']['duration']; ?></td>
					<td align="center"><?php echo date('M d, Y H:i:s', strtotime($t['Tasks']['end_date'])); ?></td>
					<td align="center">
						<div style="text-align:center">
					<?php 
					if($t['Tasks']['facebook_page_id'] != '') {
						preg_match('!\d+!', $t['Tasks']['facebook_page_id'], $matches);
						echo '<div class="fb-page" data-href="'.$t['Tasks']['facebook_page_id'].'" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"></div>';
					}
					?>
					</div>
					
					<?php if($t['Tasks']['facebook_page_id'] != '') : ?>
					<div id="fb-root"></div>
					<script>
						window.fbAsyncInit = function() {
							FB.init({appId: '<?php echo $appId; ?>', status: true, cookie: true, xfbml: true});
							FB.Event.subscribe('edge.create', function(response) {
								
								$.post('/credits/addCredit', { userId : '<?php echo $userId; ?>', taskId : '<?php echo $t['Tasks']['id']; ?>' })
									.done(function(d){
										
										console.log(d);
										
									});
								alert('You just liked '+response);
								
								
							});
						};
						(function(d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0];
							if (d.getElementById(id)) return;
							js = d.createElement(s); js.id = id;
							js.src = "//connect.facebook.net/en_EN/all.js#xfbml=1&appId=<?php echo $appId; ?>";
							fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));
					</script>
					<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
			<?php else : ?>
				<tr>
					<td colspan="6">No task available</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>


