<div class="container content">
		<h1>Adverteren op Clubbie.nl</h1>
<p>
Clubbie.nl biedt scherpe en effectieve manieren voor adverteren, dit zowel voor evenementen, clubs, bedrijven en merken. Voor meer informatie over de doelgroep en het bezoekersaantal, kunt u contact met ons opnemen.
<p>
<b>Uitlichting evenement</b><br>
Door het evenement uit te lichten in de party agenda, wordt het evenement 5 keer meer bekeken dan de concurrende evenementen. Daarbij wordt er meer informatie over het evenement weergegeven.
<br>
	<p>
	<b>Banners</b><br>
Op de homepage is er ruimte voor 2 small banners en 1 big banner. Dit is de ultieme plek om aandacht te krijgen voor het evenement. Dit komt omdat de bezoeker gelijk bij het laden van de pagina deze flyers te zien krijgt. Banners kunnen geplaatst worden aan de zijkant van de homepage en op de evenementpagina.<br>
<p>
<i>Voor meer mogelijkheden, informatie en/of voorbeelden kun je het best contact met ons opnemen.</i>
<p><b>
Mail: <a href="mailto:randy@clubbie.nl">randy(@)clubbie.nl</a></b>
</div>