<div class="container content">
<h1>Terms and Conditions</h1>
<p>De <b>Algemene voorwaarden voor bezoekers</b> geldt voor iedereen die de website <a target="_blank" href="http://clubbie.nl">www.Clubbie.nl</a> bezoekt.<br /><br />

<b>1. Beheer</b><br />
De website www.Clubbie.nl staat onder beheer van OnTribe. De contactgegevens zijn te vinden op de <a href="/pages/contact">contact pagina</a>.<br /><br />

<b>2. Cookies</b><br />
2a Clubbie.nl maakt gebruik van cookies om de functionaliteit
van bepaalde pagina's van de website te optimaliseren. Cookies zijn
kleine tekstbestanden die door een pagina van de website op de computer
van de bezoeker worden geplaatst. In zo'n cookie wordt informatie
opgeslagen zoals bepaalde voorkeuren van de bezoeker. Daardoor kan Clubbie.nl de bezoeker bij een volgend bezoek nog beter van
dienst zijn. Ook kan Clubbie.nl door cookies beter inzicht krijgen in de statistieken van de website.<br />
2b De bezoeker kan zelf bepalen hoe er met cookies
omgegaan moet worden. Hij kan zijn browser zo instellen dat die het
gebruik van cookies toestaat, niet toestaat of gedeeltelijk toestaat.
In dit laatste geval kan worden ingesteld welke websites cookies mogen
plaatsen. Bij alle overige websites wordt het dan verboden. Deze
mogelijkheid wordt door de meestgebruikte moderne browsers geboden.<br />
2c Cookies kunnen altijd van een computer worden verwijderd, ook weer via de browser.</p>
<br />
<p><b>3. Vragen</b><br />
Bezoekers kunnen met hun vragen over deze algemene voorwaarden terecht bij Clubbie.nl. De contactgegevens staan vermeld op de website die genoemd
wordt in lid 1 van deze algemene voorwaarden.<br /><br />

<b>4. Disclaimer</b><br />
Clubbie.nl is gerechtigd de inhoud van de algemene voorwaarden te wijzigen
zonder dat de bezoeker daarvan op de hoogte wordt gesteld. Het
doorvoeren van de wijziging op de website is daarvoor afdoende. </p><br />

<b>5. Actievoorwaarden</b><br />
Om te deelnemen aan de acties die Clubbie.nl verzorgt dient de gebruiker 18 jaar of ouder te zijn en zijn profielgegevens naar waarheid ingevuld te hebben. De gewonnen tickets en/of gastenlijstplaatsen zijn naamsgebonden. Het is niet toegestaan om deze te verkopen / weg te geven aan derden. Daarbij is het ook niet toegestaan om meer dan 1 account te beheren.<br /><br />

<b>6. Copyright</b><br />
De teksten, vormgeving, databestanden, party agenda en overige materialen berusten bij Clubbie.nl. De website wordt uitsluitend voor persoonlijk en niet commercieel gebruik ter beschikking gesteld. Dit betekent dat al het overig gebruik niet is toegestaan, tenzij daarvoor voorafgaande schriftelijke toestemming van de rechthebbende is verkregen.<p>

Clubbie.nl besteed zijn uiterste zorg aan de inhoud en vormgeving van de website. Clubbie.nl aanvaardt geen enkele aansprakelijkheid voor de juistheid en volledigheid hiervan dan wel voor de directe of indirecte gevolgen van handelen of nalaten op basis hiervan. Clubbie.nl behoudt zich het recht voor, op elk gewenst moment en zonder aankondiging, veranderingen in de site aan te brengen. Clubbie.nl verplicht zich niet de gegevens te up-daten. <p><br />
<b>Geen rechten van welke aard dan ook kunnen worden ontleend aan de inhoud van deze website.</b></p>
</div>