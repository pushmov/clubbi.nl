<?php 
echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js');
 ?>

<div class="container content" style="
    padding: 30px;text-align:center;background-color: white;
">
   <div class="login-form">
  <h1>Organisator login</h1>
        <div id="form-block">
           <div id="loginform">
<?php
  echo $this->Session->flash('auth');
  echo $this->Form->create('User', array('url'=>array('controller'=>'users', 'action' => 'login')
    , 'id'=>'UserLoginForm'));
  echo $this->Form->input('username', array('div'=>false, 'autofocus'=>true, 'autocomplete'=>'off'));
  echo $this->Form->input('password', array('div'=>false));
  echo $this->Html->link(__('Forgot your password?'), array('controller'=>'users', 'action'=>'recover'));
  echo $this->Form->end(array('label'=>__('Login')));
  
?> 
<br>
            <div class="clear"></div>
</div>

<span data-url="<?php echo $redirect; ?>" id="redir-page"></span>
</div>
</div>