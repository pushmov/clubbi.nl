<div class="container content">
	<div id="left_column_user">
		<div class="user-photo">
<?php echo $this->Html->image('users/'.$user['User']['photo'], array('style'=>'width:80px;height:80px;')); ?>
		</div>
		<div class="user-info">
			<?php
if(!$user['User']['first_name'] && !($user['User']['last_name'])) {
		echo "<h1>";
		echo ucfirst($user['User']['username']);
		echo "</h1>";
	}
	else {
		echo "<h1>";
		echo ucfirst($user['User']['first_name']);
		echo " ";
		echo ucfirst($user['User']['last_name']);
		echo "</h1>";
	}
	?>
<?php
	$memberDate = __d('cake', $this->Time->format('F', $user['User']['created'])) . ' ';
	$memberDate .= $this->Time->format('Y', $user['User']['created']) . '. ';

	echo sprintf(__('Member since %s'), $memberDate);

	if(isset($user['City']['name'])) {
		echo '<br />';
		echo $this->Html->link(ucfirst($user['City']['name'])
			, array('controller'=>'events', 'action'=>'index', $user['Country']['slug'], $user['City']['slug']));
		echo ', ';
		echo $this->Html->link(__d('countries', $user['Country']['name'])
			, array('controller'=>'events', 'action'=>'index', $user['Country']['slug']));
		echo '.';
	}

	if($user['User']['web']) {
		echo '<br />';
		echo $this->Html->link(__('Visit website'),
			'http://' . ereg_replace('^http://','', $user['User']['web']), array('rel'=>'nofollow'));
	}
?>
		</div>
		<div class="clear"></div>
		<div class="user-links">
<?php
	if($homepage) {
		echo $this->Html->link(__('Edit my profile'),array('controller'=>'users', 'action'=>'edit')
			, array('class'=>'editprofilebutton'));
	}
?>
		</div>
	</div>
	<div id="right_column_user">
		<div class="user-events">
<?php
	if(Configure::read('evento_settings.adminEvents') != 1
	|| Configure::read('evento_settings.disableAttendees') != 1) {
	if($homepage) {
		echo '<h2>' . __('My Profile') . '</h2>';
	}
	elseif(!$user['User']['first_name'] && !($user['User']['last_name'])) {
		echo "<h2>";
		echo ucfirst($user['User']['username']);
		echo "'s Profile</h2>";
	}
	else {
		echo "<h2>";
		echo ucfirst($user['User']['first_name']);
		echo "'s Profile</h2>";
	}
	?>

			<div id="user-events-browser">
<?php
	$selected = array('class'=>'selected');
	$upcoming = (!isset($mode) || !$mode)? $selected : null;
	$past = (isset($mode) && $mode == 'past')? $selected : null;
	$posted = (isset($mode) && $mode == 'posted')? $selected : null;

	if(Configure::read('evento_settings.disableAttendees') != 1) {
		echo $this->Html->link(__('Upcoming'), array('controller'=>'users', 'action'=>'view'
			, $user['User']['slug']), $upcoming);
		echo $this->Html->link(__('Past'), array('controller'=>'users', 'action'=>'view'
			, $user['User']['slug'], 'past'), $past);
	}

	if(Configure::read('evento_settings.adminEvents') != 1) {
		echo $this->Html->link(__('My Events'), array('controller'=>'users', 'action'=>'view'
			, $user['User']['slug'], 'posted'), $posted);
	}
?>
			</div>
<?php
	if(!empty($user['Attendee'])) {
		echo '<table id="user-events-table">';
		foreach($user['Attendee'] as $event) {
			echo '<tr>';
			echo '<td class="date">';
			echo $this->Timeformat->getFormattedDate($event['Event']['start_date'], false);
			echo '</td>';
			echo '<td>';
			echo ' <span>';
			echo $this->Html->link(ucfirst($event['Event']['name'])
				, array('controller'=>'events', 'action'=>'view', $event['Country']['slug']
				, $event['City']['slug'], $event['Venue']['slug'], $event['Event']['slug']));
			echo '</span> <br>';
			echo h($event['Venue']['address'] . ', ' . $event['City']['name'] 
				. '. ' . $event['Country']['name'] . '.');
			echo '</td>';
			echo "<td>";
			if($homepage && $posted) {
		echo $this->Html->link(__('Edit'), array('controller'=>'events', 'action'=>'edit', $event['Event']['id']));
}
			echo "</td>";
			echo '</tr>';
		}
		echo '</table>';
		echo $this->Element('paginator');
	} else {
		echo '<p class="empty-message user-profile-events">' . __('There are no events in this party agenda.') . '</p>';
	}
}
?><!--
Upcoming events in city id<br>

promoted Events<br>

Winacties<br>

Banners-->
		</div>
		<?php if(!empty($userGames) && $homepage) : ?>
		<div class="user-games">
			<?php
			echo '<h2>' . __('My Game Participation') . '</h2>';
			?>
			<table width="100%" border="1">
				<thead>
					<tr>
						<th align="left"><b><?php echo __('Event name');?></b></th>
						<th align="left"><?php echo __('Event date');?></th>
						<th align="left"><?php echo __('Event location');?></th>
						<th align="left"><?php echo __('Status');?></th>
						<th align="left"><?php echo __('Chance Left');?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($userGames as $games) : ?>
				<?php
				if(time() > strtotime($games['Participant']['part_date'] . "+24 hours")){
									if(time() > strtotime($games['Event']['win_end_date'])) {
										echo "<tr class='closed'>";
									} else {
										echo "<tr>";
									}
								}
?>
						<td align="left"><?php echo $this->Html->link(ucfirst($games['Event']['name']), array('controller'=>'events', 'action'=>'view'
							, $games['Country']['slug'], $games['City']['slug'], $games['Venue']['slug']
							, $games['Event']['slug'])) . ''; ?></td>
						<td align="left"><?php 
						$datummm = "";
      if($this->Time->format('w', $games['Event']['start_date']) == 0){
          $datummm .= "Zondag";
        }elseif($this->Time->format('w', $games['Event']['start_date']) == 1){
          $datummm .= "Maandag";
        }if($this->Time->format('w', $games['Event']['start_date']) == 2){
          $datummm .= "Dinsdag";
        }if($this->Time->format('w', $games['Event']['start_date']) == 3){
          $datummm .= "Woensdag";
        }if($this->Time->format('w', $games['Event']['start_date']) == 4){
          $datummm .= "Donderdag";
        }if($this->Time->format('w', $games['Event']['start_date']) == 5){
          $datummm .= "Vrijdag";
        }if($this->Time->format('w', $games['Event']['start_date']) == 6){
          $datummm .= "Zaterdag";
        } 


   $datummm .=  date(" j ",strtotime($games['Event']['start_date']));

         if($this->Time->format('n', $games['Event']['start_date']) == 1){
                    $datummm .= "januari";
                  } 
        if($this->Time->format('n', $games['Event']['start_date']) == 2){
                    $datummm .= "februari";
                  } 
        if($this->Time->format('n', $games['Event']['start_date']) == 3){
                    $datummm .= "maart";
                  } 
        if($this->Time->format('n', $games['Event']['start_date']) == 4){
                    $datummm .= "april";
                  }
        if($this->Time->format('n', $games['Event']['start_date']) == 5){
                    $datummm .= "mei";
                  }
        if($this->Time->format('n', $games['Event']['start_date']) == 6){
                    $datummm .= "juni";
                  }
        if($this->Time->format('n', $games['Event']['start_date']) == 7){
                    $datummm .= "juli";
                  }
        if($this->Time->format('n', $games['Event']['start_date']) == 8){
                    $datummm .= "augustus";
                  }
        if($this->Time->format('n', $games['Event']['start_date']) == 9){
                    $datummm .= "september";
                  }
           if($this->Time->format('n', $games['Event']['start_date']) == 10){
                              $datummm .= "oktober";
                            }
          if($this->Time->format('n', $games['Event']['start_date']) == 11){
                                        $datummm .= "november";
                                      }
                                      if($this->Time->format('n', $games['Event']['start_date']) == 12){
                                                  $datummm .= "december";
                                                }
      $datummm .=  date(" Y",strtotime($games['Event']['start_date'])); echo $datummm;?></td>
						<td align="left"><?php echo $games['Venue']['name'] . ', ' . $games['City']['name']; ?></td>
						<?php if($games['Event']['status'] == 'lose') : ?>
						<td style="color:red;" align="left"><?php echo ucfirst($games['Participant']['status']); ?></td>
						<?php endif; ?>
						<?php if($games['Event']['status'] == 'win') : ?>
						<td style="color:green;" align="left"><?php echo ucfirst($games['Participant']['status']); ?></td>
						<?php endif; ?>
						<td align="left"><?php echo $games['Participant']['count']; ?>
						<?php if($games['Event']['status'] == 'lose') : ?>
							<?php
								// if(time() > strtotime($games['Participant']['part_date'] . "+24 hours")){
									// if(time() > strtotime($games['Event']['win_end_date'])) {
										// echo __('Contest is closed');
									// } else {
										// echo $this->Html->link(__('Now'), array('controller'=>'events', 'action'=>'view'
							// , $games['Country']['slug'], $games['City']['slug'], $games['Venue']['slug']
							// , $games['Event']['slug'])) . '';
									// }
								// } else {
									
									// if(strtotime($games['Participant']['part_date'] . "+24 hours") < strtotime($games['Event']['win_end_date'])) {
						// $datummm1 = "";
      // if($this->Time->format('w', $games['Participant']['part_date'] . "+24 hours") == 0){
          // $datummm1 .= "Zondag";
        // }elseif($this->Time->format('w', $games['Participant']['part_date'] . "+24 hours") == 1){
          // $datummm1 .= "Maandag";
        // }if($this->Time->format('w', $games['Participant']['part_date'] . "+24 hours") == 2){
          // $datummm1 .= "Dinsdag";
        // }if($this->Time->format('w', $games['Participant']['part_date'] . "+24 hours") == 3){
          // $datummm1 .= "Woensdag";
        // }if($this->Time->format('w', $games['Participant']['part_date'] . "+24 hours") == 4){
          // $datummm1 .= "Donderdag";
        // }if($this->Time->format('w', $games['Participant']['part_date'] . "+24 hours") == 5){
          // $datummm1 .= "Vrijdag";
        // }if($this->Time->format('w', $games['Participant']['part_date'] . "+24 hours") == 6){
          // $datummm1 .= "Zaterdag";
        // } 


   // $datummm1 .=  date(" j ",strtotime($games['Participant']['part_date'] . "+24 hours"));

         // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 1){
                    // $datummm1 .= "januari";
                  // } 
        // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 2){
                    // $datummm1 .= "februari";
                  // } 
        // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 3){
                    // $datummm1 .= "maart";
                  // } 
        // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 4){
                    // $datummm1 .= "april";
                  // }
        // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 5){
                    // $datummm1 .= "mei";
                  // }
        // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 6){
                    // $datummm1 .= "juni";
                  // }
        // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 7){
                    // $datummm1 .= "juli";
                  // }
        // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 8){
                    // $datummm1 .= "augustus";
                  // }
        // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 9){
                    // $datummm1 .= "september";
                  // }
           // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 10){
                              // $datummm1 .= "oktober";
                            // }
          // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 11){
                                        // $datummm1 .= "november";
                                      // }
                                      // if($this->Time->format('n', $games['Participant']['part_date'] . "+24 hours") == 12){
                                                  // $datummm1 .= "december";
                                                // }
      // $datummm1 .=  date(" ",strtotime($games['Participant']['part_date'] . "+24 hours")); 
       // $datummm1 .= ' om';
         // $datummm1 .=  date(" H:i",strtotime($games['Participant']['part_date'] . "+24 hours")); echo $datummm1;
									// } else {
										// echo __('Contest is closed');
									// }
								// }
							?>
						<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<?php endif; ?>
		
	</div>
	<div class="clear"></div>
</div>