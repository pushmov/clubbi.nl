<div id="admin-content">
	<div class="admin-content-wrap form-wrap">
		<h1 class="form-box-title"><?php echo __('Edit user'); ?></h1>
		<div id="form-block" class="form-box">
<?php 
	echo $this->Form->create('User',array('url'=>array('action'=>'edit', $this->request->data['User']['id']
		, 'page'=> isset($this->request->params['named']['page'])?
			 $this->request->params['named']['page'] : null)));

	if($this->request->data['User']['photo']!='user_photo.jpg') {
		echo '<div id="user-photo-box">';
		echo $this->Html->image('users/'.$this->request->data['User']['photo'], array('alt'=>__('photo')));
		echo '<div id="photo-input">';
		echo $this->Form->input('delete_photo', array('type'=>'checkbox', 'div'=>false
			, 'label'=>__('delete photo'), 'class'=>'checkbox-input'));
		echo '</div><div class="clear"></div></div>';
	}

	echo $this->Form->input('username', array('label'=>__('Username'), 'autocomplete'=>'off'));
	echo $this->Form->input('first_name', array('label'=>__('Voornaam'), 'autocomplete'=>'off'));
	echo $this->Form->input('last_name', array('label'=>__('Achternaam'), 'autocomplete'=>'off'));
	echo $this->Form->input('email', array('label'=>__('Email'), 'autocomplete'=>'off'));
	echo $this->Form->input('gender', array('options'=>array('M','F'),'autocomplete'=>'off'));
	echo $this->Form->input('password', array('type'=>'password', 'label'=>__('New password')
		, 'required'=>false));
	echo $this->Form->input('password_confirm', array('type'=>'password'
		, 'label'=>__('New password confirmation')));
	echo $this->Form->input('active', array('class'=>'checkbox-input', 'type'=>'checkbox'
		, 'label'=>__('User account is active'), 'div'=>false));
	echo '<br/>';
	echo $this->Form->input('admin', array('class'=>'checkbox-input', 'type'=>'checkbox'
		, 'label'=>__('User is admin'), 'div'=>false));	
?>
<?php if($this->Session->read('Auth.User.admin') && isset($this->request->params['admin'])) : ?>
<div>
	<table class="table" border="1">
		<thead>
			<tr>
				<th>Evenement</th>
				<th>Date of participation</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($participants)) : ?>
			<?php foreach($participants as $p) : ?>
			<tr>
				<td><a href="http://clubbie.nl/admin/events/edit/<?php echo $p['Participant']['event_id']; ?>"><?php echo $p['Participant']['event_id']; ?></a></td> <!--Moet eventname worden-->
				<td><?php echo $p['Participant']['part_date']; ?></td>
				<td align="center"><?php echo $p['Participant']['status']; ?></td>
			</tr>
			<?php endforeach; ?>
			<?php else : ?>
			<tr>
				<td colspan="7">No data available</td>
			</tr>
			<?php endif; ?>
		</tbody>
	</table>
</div>
<?php endif; ?>
			<div class="submit">
<?php 
	echo $this->Form->submit(__('Save'), array('div'=>false));
	echo '<p class="backlink"> ';
	echo $this->Html->link(__('Cancel'), array('controller'=>'users', 'action'=>'index'
		, 'page'=> isset($this->request->params['named']['page'])?
			$this->request->params['named']['page'] : null)
		, array('class'=>'back-button'));
	echo '</p>';
	echo $this->Form->end();
?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>