<!DOCTYPE html>
<html>
<head>
	 <?php 
// for Facebook
if(!isset($meta_location)) {
    $meta_location = FULL_BASE_URL.$this->here;
} 
?>
	<meta name="description" property="og:description" lang="nl" content="<?php if($event['Event']['name']){ echo "Meer informatie over ". $event['Event']['name']." ".$this->Time->format('d', $event['Event']['start_date']). " ".$this->Time->format('F', $event['Event']['start_date']). " ".$this->Time->format('Y', $event['Event']['start_date']). " in ".$event['Venue']['name']. ", " .$event['City']['name'] ." op Clubbie. De party agenda voor meer ". $event['Event']['genre'] .  " feesten in " .$event['City']['name'] ."."; }else { echo "Uitgaan in jouw stad! Clubbie is de meest complete party agenda! Uitgaan in Amsterdam, Utrecht, Rotterdam en meer! De complete Uitagenda, alle evenementen!";} ?>">
	
	<meta name="keywords" content="Uitgaan, Uitagenda, Amsterdam, Rotterdam, Utrecht, Clubs, Evenementen, party agenda">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="content-type"  content="text/html;charset=UTF-8">
	<meta name="apple-mobile-web-app-title" content="Clubbie.nl">
	<meta property="og:title" content="<?php if($event['Event']['name'] && $event['Event']['win'] == 0){ echo $event['Event']['name']." in ".$event['City']['name']. " - ".$this->Time->format('d', $event['Event']['start_date']). " ".$this->Time->format('F', $event['Event']['start_date']). " ".$this->Time->format('Y', $event['Event']['start_date']). " | Party Agenda Clubbie.nl"; }?>
	<?php
	if(!$event['Event']['name']) { 
		echo "Clubbie.nl - Win festivals tickets!";
	} 
	?>
<?php if($event['Event']['name'] && $event['Event']['win'] == 1){ echo "Win 2 tickets voor ".$event['Event']['name']." in ".$event['City']['name']. " - ".$this->Time->format('d', $event['Event']['start_date']). " ".$this->Time->format('F', $event['Event']['start_date']). " ".$this->Time->format('Y', $event['Event']['start_date']). " op Clubbie.nl"; }?>
	" />
	<meta property="og:site_name" content="Clubbie.nl" />
	<meta property="og:image" content="<?php

if($event['Event']['logo']!='') {
  if(filter_var($event['Event']['logo'], FILTER_VALIDATE_URL)){ 
  echo $event['Event']['logo'];
}else{
echo "http://clubbie.nl/img/logos/small/";
 echo $event['Event']['logo'];
}
}
if(!$event['Event']['logo']) {
	echo "http://clubbie.nl/img/clubbiefb.png";
	}

?>" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="nl_NL" />
	<meta property="og:url" content="<?php echo $meta_location; ?>"/>
	 
	<meta name="apple-mobile-web-app-title" content="Clubbie.nl">
	<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
 <!-- ICONS -->
 
    <!-- iPad retina icon -->
    <link href="/img/apple-touch-icon-152x152.png"
          sizes="152x152"
          rel="apple-touch-icon">
 
    <!-- iPad retina icon (iOS < 7) -->
    <link href="/img/apple-touch-icon-144x144.png"
          sizes="144x144"
          rel="apple-touch-icon">
 
    <!-- iPad non-retina icon -->
    <link href="/img/apple-touch-icon-76x76.png"
          sizes="76x76"
          rel="apple-touch-icon">
 
    <!-- iPad non-retina icon (iOS < 7) -->
    <link href="/img/apple-touch-icon-72x72.png"
          sizes="72x72"
          rel="apple-touch-icon">
 
    <!-- iPhone 6 Plus icon -->
    <link href="/img/apple-touch-icon-180x180.png"
          sizes="120x120"
          rel="apple-touch-icon">
 
    <!-- iPhone retina icon (iOS < 7) -->
    <link href="/img/apple-touch-icon-114x114.png"
          sizes="114x114"
          rel="apple-touch-icon">
 
    <!-- iPhone non-retina icon (iOS < 7) -->
    <link href="/img/apple-touch-icon-57x57.png"
          sizes="57x57"
          rel="apple-touch-icon-">
           <!-- STARTUP IMAGES -->
 
    <!-- iPad retina portrait startup image -->
    <link href="/img/apple-touch-startup-image-1536x2008.png"
          media="(device-width: 768px) and (device-height: 1024px)
                 and (-webkit-device-pixel-ratio: 2)
                 and (orientation: portrait)"
          rel="apple-touch-startup-image">

 
    <!-- iPad non-retina portrait startup image -->
    <link href="/img/apple-touch-startup-image-768x1004.png"
          media="(device-width: 768px) and (device-height: 1024px)
                 and (-webkit-device-pixel-ratio: 1)
                 and (orientation: portrait)"
          rel="apple-touch-startup-image">
 
 
    <!-- iPhone 6 Plus portrait startup image -->
    <link href="/img/apple-touch-startup-image-1242x2148.png"
          media="(device-width: 414px) and (device-height: 736px)
                 and (-webkit-device-pixel-ratio: 3)
                 and (orientation: portrait)"
          rel="apple-touch-startup-image">
 
    <!-- iPhone 6 startup image -->
    <link href="/img/apple-touch-startup-image-750x1294.png"
          media="(device-width: 375px) and (device-height: 667px)
                 and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image">
 
    <!-- iPhone 5 startup image -->
    <link href="/img/apple-touch-startup-image-640x1096.png"
          media="(device-width: 320px) and (device-height: 568px)
                 and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image">
 
    <!-- iPhone < 5 retina startup image -->
    <link href="/img/apple-touch-startup-image-640x920.png"
          media="(device-width: 320px) and (device-height: 480px)
                 and (-webkit-device-pixel-ratio: 2)"
          rel="apple-touch-startup-image">
 
    <!-- iPhone < 5 non-retina startup image -->
    <link href="/img/apple-touch-startup-image-320x460.png"
          media="(device-width: 320px) and (device-height: 480px)
                 and (-webkit-device-pixel-ratio: 1)"
          rel="apple-touch-startup-image">
<?php
	echo $this->Html->charset();
	echo $this->Html->meta('icon');
 $homepage = "/";
            $currentpage = $_SERVER['REQUEST_URI'];
            if($homepage==$currentpage) {
echo "<title>Party agenda - Uitgaan in Nederland | Clubbie.nl</title>";
                 } else {
         echo '<title>' . $title_for_layout . ' | ' . Configure::read('evento_settings.appName') . '</title>';
     };
$this->Html->css('addtohomescreen', array('block' => 'css'));
	$this->Html->css('style', array('block' => 'css'));
	$this->Html->css('bootstrap.min', array('block' => 'css'));
	$this->Html->css('bootstrap-datepicker', array('block' => 'css'));
	$this->Html->css('font-awesome.min', array('block' => 'css'));
	
	// see default/elements/
	echo $this->Element('xml_feed_link');

	// Description meta tags. See View/Elements/meta_description.ctp
	echo $this->Element('meta_description');

	// CakePHP view blocks
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
?>

<?php if($currentpage != '/users/login') : ?>
<span id="redir" alt="<?php echo Router::url( $this->here, true ); ?> "></span>
<?php else : ?>
<span id="redir" alt="<?php echo $redirect; ?> "></span>
<?php endif; ?>
<script src="/js/addtohomescreen.min.js"></script>
<script>(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener("click",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;"href"in d&&(d.href.indexOf("http")||~d.href.indexOf(e.host))&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,"standalone")</script>
<!--<script>
addToHomescreen({
   skipFirstVisit: true,
   maxDisplayCount: 1
});
</script>-->

</head>
<body>
<div id="header">
	
	<div class="container">
		
		
				<ul>
							<li id="li_947c_0">		<?php
	echo $this->Element('search_form');
	?></li>
			<li>		<div id="div_947c_0">
<a href="/" id="a_947c_0"><img class="desktop" src="http://clubbie.nl/img/logo.png"><img class="app" src="http://clubbie.nl/img/logomob.png"></a>
		</div></li>
			<li id="li_947c_1">
<?php
if($this->Session->read('Auth.User')) { // registered user
	
	
	echo $this->Html->link(__('Logout'), array('admin'=>false, 'plugin'=>null, 'controller'=>'users', 'action'=>'logout'), array('class' => 'zocial facebook mobilelog' , 'style'=>'font-size: 13px'));
}
else { // anonymous user


?>
<button class="zocial facebook mobilelog"  id="loginfb"><?php echo __('Inloggen'); ?></button>
<?php
	//echo $this->Html->link(__('Login'), array('admin'=>false, 'plugin'=>null, 'controller'=>'users', 'action'=>'login'));

	if(Configure::read('evento_settings.adminAddsUsers')==false) {
		//echo $this->Html->link(__('Register'), array('admin'=>false, 'plugin'=>null, 'controller'=>'users', 'action'=>'register'));
	}
}
?>
</li>
</ul>


		<!-- BACKUP MENU
		<div class="logo">
<a href="/"><img src="http://clubbi.nl/img/logo.png" /></a>
		</div>-->
			</div><div class="clear"></div>
			<div class="container">
			<div id="menu" >
<div id="menu-container">
<a href="/">Party agenda</a>
<a href="/win">Winacties</a>
<a href="/festivals">Festivals</a>
<a href="/events/index/all-countries/all-cities/all-venues/all-categories/all-tags/2015/april/27"><font color="orange">Koningsdag</font></a>
<?php
if(Configure::read('evento_settings.adminEvents')==='0') { 
	echo "<span class='desktop'>";
	echo $this->Html->link(__('Add an event'), 
		array('plugin'=>null, 'controller'=>'events', 'action'=>'add'), 
		array('rel'=>'nofollow')); 
	echo "</span>";
}
if($this->Session->read('Auth.User')) { // registered user
	echo $this->Html->link(__('My Profile'),array('admin'=>false, 'plugin'=>null, 'controller'=>'users', 'action'=>'view',
		$this->Session->read('Auth.User.slug')));
}

		if($this->Session->read('Auth.User.admin')) {
		echo $this->Html->link(__('Admin'), array('plugin'=>null, 'controller'=>'events', 
			'action'=>'index', 'admin'=>true));
	} ?>
</div></div></div>
		<div class="clear"></div>
</div>
<div id="main">

<?php
	// htmlTop is the custom header added by admin
	echo '<div id="bannerTop">' . Configure::read('evento_settings.htmlTop') . '</div>';

	// main content is inserted here
	echo $this->fetch('content');

	// htmlBottom is the custom footer added by admin
	echo Configure::read('evento_settings.htmlBottom');
?>
</div>

<div id="footer">
	<div class="container">
		<div class="clear">
		<div class="left clear">
			<div class="block">
				<h4>Clubbie.nl</h4>
		<a href="/">Home</a><br>
		<a href="/about">Over ons</a><br>
		<a href="/pages/terms">Voorwaarden</a><br>
		<a href="/pages/links">Links</a><br>
		<a href="/pages/contact">Contact</a><br>
		</div>
		<div class="block">
			<h4>Sociaal</h4>
			<a target="_blank" href="http://facebook.com/Clubbie.nl">Facebook</a><br>
		<a target="_blank" href="http://instagram.com/Party_agenda">Instagram</a><br>
		<a target="_blank" href="http://twitter.com/officialclubbie">Twitter</a><br>
		</div>
		<!--<div class="block">
			<h4>TAAL</h4>
			<a href="/lang/ned"><img src="/flags/Netherlands.png"></a><br>
		<a href="/lang/eng"><img src="/flags/United-Kingdom.png"></a><br>
		<a href="/lang/spa"><img src="/flags/Spain.png"></a><br>
		</div>-->
	</div>
		<div class="right desktop">
			<div class="block">
				<h4>Partners</h4>
				<!--<img height="20px" src="/img/partners/air.png">-->
				<img id="img_947c_0" height="13px" src="/img/partners/ticketswap.png">
				<img id="img_947c_1" height="20px" src="/img/partners/clubjudge.png">
				<!--<img id="img_947c_2" height="20px" src="/img/partners/panama.png">-->
			</div>
			</div>
			
<?php
	/*
	// You can allow users to change they language settings by creating a link to the /lang/ page along with the
	// language code.
	// ex. http://example.com/lang/spa --> set language to Spanish
	// The language settings are stored in a cookie so when the user comes back it gets the page in his desired
	// language.
	// you can use the following code to build your language links, this links are created using the CakePHP HTML
	// helper and also add the nofollow attribute.

	// if current language is English show the link to change it to Italian
	if(Configure::read('evento_settings.language') == 'eng') {
		echo $this->Html->link('Italiano', array('controller'=>'settings', 'action'=>'lang', 'ita'),
			array('rel'=>'nofollow'));
	}
	else {
		echo $this->Html->link('English', array('controller'=>'settings', 'action'=>'lang', 'eng'),
			array('rel'=>'nofollow'));
	}
	*/
?>
<div class="clear"></div>
<?php
	$this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'
		, array('block' => 'scriptBottom'));
	$this->Html->script('evento', array('block' => 'scriptBottom'));
	$this->Html->script('zebra_datepicker', array('block' => 'scriptBottom'));
	$this->Html->script('dice', array('block' => 'scriptBottom'));

	echo $this->fetch('scriptBottom');

	// CakePHP SQL dump
	echo $this->element('sql_dump');
?>
    

<script>
$(document).ready(function(){
  $("#hide").click(function(){
    $("p").hide();
    $("#hide").hide();
  });
  $("#show").click(function(){
    $("p").show();
  });
});
</script>

	<script type="text/javascript" src="/banner-management/scripts/js/banner-tracking.js"></script>
</body>
</html>