<div id="DIV_1">
	<img src="/img/header.png" id="IMG_2" alt='Clubbie, uitgaan en feesten in Amsterdam, Utrecht, Rotterdam.' />
</div>

<div id="hide" class="alerts">
  <div class="alert-message win">
    <a class="close" href="#">×</a>
    <p><center><a href="/win"><img src="/img/winpromoted.png"> <strong>Kaarten winnen? 'Roll the dice' en win 2 tickets! <img src="/img/winpromoted.png"></a></center></p>
  </div>
</div>
<div class="container content">
	<?php echo $this->Session->flash(); ?>



<div id="banner-placeholder-4" class="banner-placeholder" style="width:100%;"></div>
	<div id="center_column" class="events-index">

    <table class="tg">
<?php
// show the promoted events at the top

	// show events filters
	echo $this->Element('index_filter');

	// load events
	if(!empty($events)) {
    $temp = array();
		foreach($events as $event) {
      //print_r($event);
      $datummm = "";
      if($this->Time->format('w', $event['Event']['start_date']) == 0){
          $datummm .= "Zondag";
        }elseif($this->Time->format('w', $event['Event']['start_date']) == 1){
          $datummm .= "Maandag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 2){
          $datummm .= "Dinsdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 3){
          $datummm .= "Woensdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 4){
          $datummm .= "Donderdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 5){
          $datummm .= "Vrijdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 6){
          $datummm .= "Zaterdag";
        } 


   $datummm .=  date(" j ",strtotime($event['Event']['start_date']));

         if($this->Time->format('n', $event['Event']['start_date']) == 1){
                    $datummm .= "januari";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 2){
                    $datummm .= "februari";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 3){
                    $datummm .= "maart";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 4){
                    $datummm .= "april";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 5){
                    $datummm .= "mei";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 6){
                    $datummm .= "juni";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 7){
                    $datummm .= "juli";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 8){
                    $datummm .= "augustus";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 9){
                    $datummm .= "september";
                  }
           if($this->Time->format('n', $event['Event']['start_date']) == 10){
                              $datummm .= "oktober";
                            }
          if($this->Time->format('n', $event['Event']['start_date']) == 11){
                                        $datummm .= "november";
                                      }
                                      if($this->Time->format('n', $event['Event']['start_date']) == 12){
                                                  $datummm .= "december";
                                                }
      $datummm .=  date(" Y",strtotime($event['Event']['start_date']));
      if(!in_array( $datummm, $temp)){
     
      

          echo '<tr>
            <th class="tg-031" colspan="4">'.$datummm.' </th>
          </tr>';
          echo '<tr>
                <th class="tg-031 left" colspan="1"></th>
                <th class="tg-031 left" colspan="1"></th>
                <th class="tg-031 left" colspan="1"></th>
                <th class="tg-031 left" colspan="1"></th>
              </tr>';
            $temp[] =   $datummm;
      }
   
			echo $this->Element('event_post', array('event'=>$event));
		}
echo "</table>";
		echo $this->Element('paginator');
	} else {
		echo '<p class="empty-message">'.__('There are no events.').'</p>';
	}
?>

	</div>
	<div id="right_column">
    <div class="desktop">
      <?php echo $this->Element('genres');?>
    </div>
	<!--	<h2><?php echo __('Zoek op naam'); ?></h2>
		<?php
	echo $this->Element('search_form');
	if(!empty($countries)) { 
?>-->
<div class="desktop">
<?php
	// calendar is inserted here
	echo $this->Calendar->calendar($year, $month, $data, $country, $city, $venue, $category
		, $current_tag, $weekStart, $day);
?>
		<div class="event-browser">
<?php
	$tomorrow_year = date('Y', strtotime('+1 day'));
	$tomorrow_month = strtolower(date('F', strtotime('+1 day')));
	$tomorrow_day = date('d', strtotime('+1 day'));
	echo $this->Html->link(__('Tomorrow'), array('controller'=>'events', 'action'=>'index', $country
		, $city, $venue, $category, $current_tag, $tomorrow_year, $tomorrow_month, $tomorrow_day));
	echo $this->Html->link(__('This week'),array('controller'=>'events', 'action'=>'index', $country
		, $city, $venue, $category, $current_tag, date('Y'), strtolower(date('F')),'week'));
	echo $this->Html->link(__('This month'), array('controller'=>'events', 'action'=>'index'
		, $country, $city, $venue, $category, $current_tag, date('Y'), strtolower(date('F'))));
?>
		</div>

     <div id="banner-placeholder-5" class="banner-placeholder" style="width:280px;height:350px;"></div>


  

    </div>
		<!--<div id="cities_column">
			<h2><?php echo __('Zoek op stad'); ?></h2>
			<?php echo $this->Element('cities_list'); ?>
		</div>-->
<!--<?php 
	}
	echo $this->Element('categories_block');
	echo $this->Element('toptags_block');
?>-->

	</div>
	<div class="clear"></div>
</div>
