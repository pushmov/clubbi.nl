<div id="admin-content">
  <div class="admin-content-wrap">
    <div class="header">
<?php

  echo $this->Form->create('Search', array('url'=>array('controller'=>'events', 'action'=>'search')));
  echo $this->Form->input('Search.term', array('class'=>'user-search', 'type'=>'text'
    , 'label'=>false, 'div'=>false, 'autocomplete'=>'off'
    , 'placeholder'=>ucfirst(strtolower(__('Event Name')))));
  echo $this->Form->submit(__('Search'), array('class'=>'user-search-button', 'div'=>false));
  echo $this->Form->end();
?>
    </div>
<?php
  if(!empty($events)) {
    echo $this->Form->create('Event', array('url'=>array('action'=>'bulk')));
    echo '<table id="admin">';
    echo '<thead>';
    echo '<tr>';
    echo '<th class="checkbox-column"><input type="checkbox" id="select-all"></th>';
    echo '<th class="name">' . __('Event') . '</th>';
    echo '<th>Win start date</th>';
        echo '<th>Win end date</th>';
    echo '<th class="icon">Winners</th>';
    echo '<th class="icon">Participants</th>';
    echo '<th class="icon"><i class="fa fa-pencil"></i></th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
		
    foreach($events as $event) {
      if($event['Event']['win'] == '1') {
      $isExpired  = (time() > strtotime($event['Event']['win_end_date']));
      }
      if($isExpired) {
       echo '<tr style="color:red;">';
      } 
else{
       echo '<tr>';
      }

      echo '<td class="checkbox-column">';
      echo $this->Form->input('Event.id.'.$event['Event']['id']
        , array('type'=>'checkbox', 'div'=>false, 'label'=>false, 'class'=>'checkbox'));
      echo '</td>';
      echo '<td class="name">';
      echo $this->Html->link(ucfirst($event['Event']['name'])
        , array('admin'=>0, 'controller'=>'events', 'action'=>'view'
        , $event['Country']['slug'], $event['City']['slug']
        , $event['Venue']['slug'], $event['Event']['slug']));
      echo '</td>';


      echo '<td>';
      $datummm = "";
      if($this->Time->format('w', $event['Event']['win_start_date']) == 0){
          $datummm .= "Zondag";
        }elseif($this->Time->format('w', $event['Event']['win_start_date']) == 1){
          $datummm .= "Maandag";
        }if($this->Time->format('w', $event['Event']['win_start_date']) == 2){
          $datummm .= "Dinsdag";
        }if($this->Time->format('w', $event['Event']['win_start_date']) == 3){
          $datummm .= "Woensdag";
        }if($this->Time->format('w', $event['Event']['win_start_date']) == 4){
          $datummm .= "Donderdag";
        }if($this->Time->format('w', $event['Event']['win_start_date']) == 5){
          $datummm .= "Vrijdag";
        }if($this->Time->format('w', $event['Event']['win_start_date']) == 6){
          $datummm .= "Zaterdag";
        } 


   $datummm .=  date(" j ",strtotime($event['Event']['win_start_date']));

         if($this->Time->format('n', $event['Event']['win_start_date']) == 1){
                    $datummm .= "januari";
                  } 
        if($this->Time->format('n', $event['Event']['win_start_date']) == 2){
                    $datummm .= "februari";
                  } 
        if($this->Time->format('n', $event['Event']['win_start_date']) == 3){
                    $datummm .= "maart";
                  } 
        if($this->Time->format('n', $event['Event']['win_start_date']) == 4){
                    $datummm .= "april";
                  }
        if($this->Time->format('n', $event['Event']['win_start_date']) == 5){
                    $datummm .= "mei";
                  }
        if($this->Time->format('n', $event['Event']['win_start_date']) == 6){
                    $datummm .= "juni";
                  }
        if($this->Time->format('n', $event['Event']['win_start_date']) == 7){
                    $datummm .= "juli";
                  }
        if($this->Time->format('n', $event['Event']['win_start_date']) == 8){
                    $datummm .= "augustus";
                  }
        if($this->Time->format('n', $event['Event']['win_start_date']) == 9){
                    $datummm .= "september";
                  }
           if($this->Time->format('n', $event['Event']['win_start_date']) == 10){
                              $datummm .= "oktober";
                            }
          if($this->Time->format('n', $event['Event']['win_start_date']) == 11){
                                        $datummm .= "november";
                                      }
                                      if($this->Time->format('n', $event['Event']['win_start_date']) == 12){
                                                  $datummm .= "december";
                                                }
      $datummm .=  date(" Y",strtotime($event['Event']['win_start_date']));
      if(!in_array( $datummm, $temp)){
        echo $datummm;
      }
    

      echo '</td>';
                echo "<td>";
$datummm = "";
      if($this->Time->format('w', $event['Event']['win_end_date']) == 0){
          $datummm .= "Zondag";
        }elseif($this->Time->format('w', $event['Event']['win_end_date']) == 1){
          $datummm .= "Maandag";
        }if($this->Time->format('w', $event['Event']['win_end_date']) == 2){
          $datummm .= "Dinsdag";
        }if($this->Time->format('w', $event['Event']['win_end_date']) == 3){
          $datummm .= "Woensdag";
        }if($this->Time->format('w', $event['Event']['win_end_date']) == 4){
          $datummm .= "Donderdag";
        }if($this->Time->format('w', $event['Event']['win_end_date']) == 5){
          $datummm .= "Vrijdag";
        }if($this->Time->format('w', $event['Event']['win_end_date']) == 6){
          $datummm .= "Zaterdag";
        } 


   $datummm .=  date(" j ",strtotime($event['Event']['win_end_date']));

         if($this->Time->format('n', $event['Event']['win_end_date']) == 1){
                    $datummm .= "januari";
                  } 
        if($this->Time->format('n', $event['Event']['win_end_date']) == 2){
                    $datummm .= "februari";
                  } 
        if($this->Time->format('n', $event['Event']['win_end_date']) == 3){
                    $datummm .= "maart";
                  } 
        if($this->Time->format('n', $event['Event']['win_end_date']) == 4){
                    $datummm .= "april";
                  }
        if($this->Time->format('n', $event['Event']['win_end_date']) == 5){
                    $datummm .= "mei";
                  }
        if($this->Time->format('n', $event['Event']['win_end_date']) == 6){
                    $datummm .= "juni";
                  }
        if($this->Time->format('n', $event['Event']['win_end_date']) == 7){
                    $datummm .= "juli";
                  }
        if($this->Time->format('n', $event['Event']['win_end_date']) == 8){
                    $datummm .= "augustus";
                  }
        if($this->Time->format('n', $event['Event']['win_end_date']) == 9){
                    $datummm .= "september";
                  }
           if($this->Time->format('n', $event['Event']['win_end_date']) == 10){
                              $datummm .= "oktober";
                            }
          if($this->Time->format('n', $event['Event']['win_end_date']) == 11){
                                        $datummm .= "november";
                                      }
                                      if($this->Time->format('n', $event['Event']['win_end_date']) == 12){
                                                  $datummm .= "december";
                                                }
      $datummm .=  date(" Y",strtotime($event['Event']['win_end_date']));
      if(!in_array( $datummm, $temp)){
        echo $datummm;
      }
    echo '</td>';
      echo '<td class="icon">';
      echo ($event['Count']['winner'] == '') ? 0 : $event['Count']['winner'];
      echo ' / '.$event['Event']['win_number_winners'].'';
      echo '</td>';
      echo '<td class="icon"> ';
      echo ($event['Count']['participant'] == '') ? 0 : $event['Count']['participant'];
      echo '</td>';
      echo '<td class="icon">';
      echo $this->Html->link('<i class="fa fa-pencil"></i>'
        , array('controller'=>'events', 'action'=>'edit', $event['Event']['id']
        , 'page'=>isset($this->request->params['named']['page'])?
          $this->request->params['named']['page'] : null)
        , array('escape'=>false));
      echo '</td>';
      echo '</tr>';
    }

    echo '</tbody>';
    echo '</table>';

    echo $this->Element('paginator');
  } else {
    echo '<div class="content-box"><p class="center empty">' . __('There are no events.') . '</p></div>';
  }
?>
  </div>
</div>