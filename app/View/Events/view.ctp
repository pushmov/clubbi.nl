<?php
  if($this->Session->read('Auth.User.id') == $event['Event']['user_id']) {


    if(Configure::read('evento_settings.disablePhotos') == false) {
      echo $this->Html->link(__('Manage gallery'), array('controller'=>'photos', 'action'=>'manage'
        , $event['Event']['slug']));
    }
  }
?>
<!-- Event photos
  <?php
  if(Configure::read('evento_settings.disablePhotos') == false && !empty($event['Photo'])) {
    echo '<div id="small-photos">';
    echo '<div class="container1"><div class="center_column" style="position: relative">';
    echo '<div id="photo-loader">';
    echo '<span>' . __('Loading...') . '</span>';
    echo '</div>';
    echo $this->Html->image('events/'.$event['Photo'][0]['Photo']['file'], array('id'=>'big-image'));
    echo '</div><div class="right_column" id="thumbnails-column"><div id="thumbnails">';
    foreach($event['Photo'] as $photo) {
      echo $this->Html->link($this->Html->image('events/small/'.$photo['Photo']['file']
        , array('alt'=>h($photo['Photo']['title']))), array('controller'=>'photos'
        , 'action'=>'view', $event['Event']['slug'], $photo['Photo']['id']), array('escape'=>false
        , 'data-photo'=> Router::url('/') . IMAGES_URL . 'events/' . $photo['Photo']['file']
        , 'class'=>'small-image'));
    }
    echo '</div>';
    if($photoPagination) {
      echo '<div id="thumbnails-paginator">';
      echo $this->Html->link('>', array('controller'=>'photos', 'action'=>'page', $event['Event']['id']
        , 1), array('id'=>'thumbs-next'));
      echo '</div>';
    }
    echo '</div><div class="clear"></div></div></div>';
  }
?>-->
<!-- Attendees
  <?php
  if(Configure::read('evento_settings.disableAttendees') == false 
  && ((isset($event['Attendees']) && !empty($event['Attendees'])) || $this->Session->read('Auth.User.id'))) {
?>
  <div class="block-attendees">
  <h2><?php echo __('Attendees'); ?></h2> 
  <div id="user-list" class="attendee-list">
<?php
    if(!empty($event['Attendees'])) {
      foreach($event['Attendees'] as $attendee) {
        if($this->Session->read('Auth.User.id') == $attendee['User']['id']) {
          $attendeesId = 'attendee-me';
        } else {
          $attendeesId = 'attendee';
        }
        echo '<div id="' . $attendeesId . '" class="attendee-user">';
        echo $this->Html->link($this->Html->image('users/' . $attendee['User']['photo'],
          array('alt'=>h($attendee['User']['username']))), array('controller'=>'users'
            , 'action'=>'view' , $attendee['User']['slug']), array('escape'=>false)) . ' ';
        echo '<span>';
        echo $this->Html->link($attendee['User']['username'], array('controller'=>'users'
          , 'action'=>'view', $attendee['User']['slug']));
        echo '<br> ';
        if($attendee['City']['name']) {
          echo $attendee['City']['name'] . ', ' . $attendee['Country']['name'] . '.';
        }
        echo '</span><div class="clear"></div>';
        echo '</div>';
      }
    } else {
      echo '<p class="empty-message">' . __('There are no attendees.') . '</p>';
    }
  }
?>-->
<!-- Repeated events & Event tags
  <?php 
  if(isset($repeatEvents) && !empty($repeatEvents)) {
?>
    <div class="block-repeat-events">
      <h2><?php echo __('This event repeats'); ?></h2>
<?php
  foreach($repeatEvents as $repeatEvent) {
    echo '<p>';
    $linkName = $this->Timeformat->getFormattedDate($repeatEvent['Event']['end_date']);
    echo $this->Html->link($linkName
      , array('action'=>'view', $repeatEvent['Venue']['City']['Country']['slug']
      , $repeatEvent['Venue']['City']['slug'], $repeatEvent['Venue']['slug']
      , $repeatEvent['Event']['slug']));
    echo '</p>';
  }
?>
    </div>
<?php
  }
?>
    <span class="event-tags">
<?php
  if(!empty($event['Tag'])) {
    sort($event['Tag']);

    $event_category = $event['Category']['slug'];
    if(!$event_category) {
      $event_category = 'all-categories';
    }

    $n = 1;
    foreach($event['Tag'] as $tag) {
      echo $this->Html->link($tag['Tag']['name'],
        array('controller'=>'events', 'action'=>'index',
          'all-countries',
          'all-cities', 'all-venues', 'all-categories',
          $tag['Tag']['slug']),array('rel'=>'tag'));
      $n++;
    }
  }
?>
    </span>
    </div>-->
     <?php echo $this->Session->flash(); ?>
<div id="DIV_11">
<div class="container text-center margin">
 <?php

if($event['Event']['logo']!='') {
  if(filter_var($event['Event']['logo'], FILTER_VALIDATE_URL)){ 
  echo ' <div><a data-mfp-src="' . $event['Event']['logo'] . '" class="image-link"><img class="eventflyer" src="' . $event['Event']['logo'] . '" alt="Flyer ' . $event['Event']['name'] . ' in ' . $event['Venue']['name'] . ', ' . $event['City']['name'] . '"></a></div>';
}else{

 echo ' <div style=""><a  data-mfp-src="' . Router::url('/') . IMAGES_URL . 'logos/big/' . $event['Event']['logo'] . '" class="image-link"><img class="eventflyer" src="' . Router::url('/') . IMAGES_URL . 'logos/small/' . $event['Event']['logo'] . '" alt="Flyer ' . $event['Event']['name'] . ' in ' . $event['Venue']['name'] . ', ' . $event['City']['name'] . '" ></a></div>';
}
}

?>

<h1 class="eventname"><?php echo h(ucfirst($event['Event']['name'])); ?></h1>
<h2 class="whenname"><?php
  echo $this->Html->link(ucfirst($event['Venue']['name']), array('action'=>'index'
    , $event['Country']['slug'], $event['City']['slug']
    , $event['Venue']['slug']), array('class'=>'whenname'));
?>, <?php 
  echo $this->Html->link(ucfirst($event['City']['name']), 
    array('controller'=>'events', 'action'=>'index', $event['Country']['slug'], $event['City']['slug']), array('class'=>'whenname')); 
?></h2>
<h3 class="whenname"><?php
           if($this->Time->format('w', $event['Event']['start_date']) == 0){
          echo "Zondag";
        }elseif($this->Time->format('w', $event['Event']['start_date']) == 1){
          echo "Maandag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 2){
          echo "Dinsdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 3){
          echo "Woensdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 4){
          echo "Donderdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 5){
          echo "Vrijdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 6){
          echo "Zaterdag";
        } echo ', '; echo date("d", strtotime($event['Event']['start_date']));echo ' ';
        if($this->Time->format('n', $event['Event']['start_date']) == 1){
                    echo "januari";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 2){
                    echo "februari";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 3){
                    echo "maart";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 4){
                    echo "april";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 5){
                    echo "mei";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 6){
                    echo "juni";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 7){
                    echo "juli";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 8){
                    echo "augustus";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 9){
                    echo "september";
                  }
           if($this->Time->format('n', $event['Event']['start_date']) == 10){
                              echo "oktober";
                            }
          if($this->Time->format('n', $event['Event']['start_date']) == 11){
                                        echo "november";
                                      }
                                      if($this->Time->format('n', $event['Event']['start_date']) == 12){
                                                  echo "december";
                                                }
        echo ',';
        echo ' ' . $this->Time->format($this->Timeformat->getTimeFormat(), $event['Event']['start_date']);
      
      echo ' -';
      
        echo ' ' . $this->Time->format($this->Timeformat->getTimeFormat(), $event['Event']['end_date']);
      ?></h3>
<br>
<?php 
if($event['Event']['genre']){
  echo '<h3 class="genrename">';
echo $event['Event']['genre'];
echo "</h3>";
}
?>
<div id="menu-outer">
  <div class="table">
 <ul id="venu-share">

<?php if($isWin) : ?>
      <li class="pulse2"><a style="width: auto;" href="<?php echo Router::url('win/' . $winURL); ?>" class="share-google btn-social-share"><img src="/img/winpromoted.png" alt=""><span class="share-font">WIN HIER TICKETS</span></a></li>
       <?php endif; ?>
       <?php
  if(Configure::read('evento_settings.disableAttendees') == false) {
    if($this->Session->read('Auth.User')) {
      $id = 'imgoing';
    } else {
      $id = 'imgoing-login';
    }

    if($event['Event']['end_date'] < date('Y-m-d')) {
      $button_text = 'I went';
      $cancel_text = 'I went to this event, click to cancel.';
    } else {
      $button_text = 'I\'m going';
      $cancel_text = 'I\'m going to this event, click to cancel.';
    }

    if($isAttendee) {
      echo "<li>";
      echo $this->Html->link(__('<i class="fa fa-user-times"></i> '.$cancel_text),
        array('controller'=>'events', 'action'=>'attendee', 
          $event['Event']['id'], 0), array('class'=>'btn-social-share desktop', 'style'=>'width:auto;background-color:rgb(216, 141, 3);','escape'=>false)).' ';
      echo "</li>";
    } else {
echo "<li>";
      echo $this->Html->link(__('<i class="fa fa-user-plus"></i> '.$button_text),
        array('controller'=>'events', 'action'=>'attendee', 
          $event['Event']['id'], 1), array('class'=>'btn-social-share desktop', 'style'=>'width:auto;background-color:green;','escape'=>false)).' ';
      echo "</li>";
    }
  }
?><li><a class="btn-social-share share-whatsapp app" href="whatsapp://send?text=Check <?php echo h(ucfirst($event['Event']['name'])); ?>: <?php echo Router::url( $this->here, true ); ?>"><i class="fa fa-fw fa-whatsapp"></i><span class="share-font">SHARE</span></a></li>
      <li><a onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo Router::url( $this->here, true ); ?>','Facebook share',
'width=400,height=200,scrollbars=yes,toolbar=yes,location=yes'); return false" class="desktop share-facebook btn-social-share" target="_BLANK" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo Router::url( $this->here, true ); ?>"><i class="fa fa-fw fa-facebook-official"></i><span class="share-font">SHARE</span></a></li>
      <li><a onclick="window.open('https://twitter.com/home?status=<?php echo Router::url( $this->here, true ); ?>','Twitter share',
'width=400,height=200,scrollbars=yes,toolbar=yes,location=yes'); return false" class="desktop share-twitter btn-social-share" target="_BLANK"  href="https://twitter.com/home?status=<?php echo Router::url( $this->here, true ); ?>"><i class="fa fa-fw fa-twitter"></i><span class="share-font">TWEET</span></a></li>
      <!--<li><a onclick="window.open('https://plus.google.com/share?url=<?php echo Router::url( $this->here, true ); ?>','Google+ share',
'width=400,height=200,scrollbars=yes,toolbar=yes,location=yes'); return false" class="desktop share-google btn-social-share" target="_BLANK"  href="https://plus.google.com/share?url=<?php echo Router::url( $this->here, true ); ?>"><i class="fa fa-fw fa-google-plus"></i><span class="share-font">SHARE</span></a></li>-->

          <?php
    if($this->Session->read('Auth.User.id') == $event['Event']['user_id']) {
      echo "<li>";
      echo '<a class="edit-event btn-social-share" href="';
    echo $this->Html->url(array('controller'=>'events', 'action'=>'edit', $event['Event']['id'])); 
    echo '"><i class="fa fa-pencil-square-o"></i><span class="share-font">EDIT</span></a></li>';
  }
  //array('class'=>''));
      ?>

      
      
     

    </ul><!--/.venu-share-->
</div>
</div>
</div>
</div>
<style type="text/css">
#DIV_11 {
  background-size: cover;
  background-image: url('<?php echo '/img/bg/background';echo mt_rand(1, 6); echo '.jpg';?>');
  height: <?php if($event['Event']['logo']!='') { echo "550px;";}else{ echo "385px;";} ?>
    text-align: center;
    left: 0px;
    top: 55px;
    width: 100%;
    z-index: -999998;
    align-self: stretch;
    perspective-origin: 638px 50px;
    transform-origin: 638px 50px;
    border: 0px none rgb(140, 128, 110);
    font: normal normal normal normal 12px/normal Arial;
    outline: rgb(140, 128, 110) none 0px;
    overflow: hidden;
}
/*#DIV_11*/


</style>


<div class="container content1">

  <div id="center_column">

<div class="boxer">


  <?php
    echo '
  <div class="box-row">
    <div class="box">Feest naam</div>
    <div class="box"> '.$event['Event']['name'].' </div>
  </div>';

    echo '
  <div class="box-row">
    <div class="box">Locatie</div>
    <div class="box"> '.$event['Venue']['name'].' </div>
  </div>';

    echo '
  <div class="box-row">
    <div class="box">Adres</div>
    <div class="box"> '.$event['Venue']['address'].' <br />'.$event['City']['name'].' </div>
  </div>'; ?>

  <div class="box-row">
    <div class="box">Datum</div>
    <div class="box"><?php


             if($this->Time->format('w', $event['Event']['start_date']) == 0){
            echo "Zondag";
          }elseif($this->Time->format('w', $event['Event']['start_date']) == 1){
            echo "Maandag";
          }if($this->Time->format('w', $event['Event']['start_date']) == 2){
            echo "Dinsdag";
          }if($this->Time->format('w', $event['Event']['start_date']) == 3){
            echo "Woensdag";
          }if($this->Time->format('w', $event['Event']['start_date']) == 4){
            echo "Donderdag";
          }if($this->Time->format('w', $event['Event']['start_date']) == 5){
            echo "Vrijdag";
          }if($this->Time->format('w', $event['Event']['start_date']) == 6){
            echo "Zaterdag";
          }   echo ' '; echo date("d", strtotime($event['Event']['start_date']));echo ' ';

          if($this->Time->format('n', $event['Event']['start_date']) == 1){
                      echo "januari";
                    } 
          if($this->Time->format('n', $event['Event']['start_date']) == 2){
                      echo "februari";
                    } 
          if($this->Time->format('n', $event['Event']['start_date']) == 3){
                      echo "maart";
                    } 
          if($this->Time->format('n', $event['Event']['start_date']) == 4){
                      echo "april";
                    }
          if($this->Time->format('n', $event['Event']['start_date']) == 5){
                      echo "mei";
                    }
          if($this->Time->format('n', $event['Event']['start_date']) == 6){
                      echo "juni";
                    }
          if($this->Time->format('n', $event['Event']['start_date']) == 7){
                      echo "juli";
                    }
          if($this->Time->format('n', $event['Event']['start_date']) == 8){
                      echo "augustus";
                    }
          if($this->Time->format('n', $event['Event']['start_date']) == 9){
                      echo "september";
                    }
             if($this->Time->format('n', $event['Event']['start_date']) == 10){
                                echo "oktober";
                              }
            if($this->Time->format('n', $event['Event']['start_date']) == 11){
                                          echo "november";
                                        }
                                        if($this->Time->format('n', $event['Event']['start_date']) == 12){
                                                    echo "december";
                                                  }



           ?> </div>
  </div>

  <div class="box-row">
    <div class="box">Begintijd</div>
    <div class="box">  <?php  echo $this->Time->format($this->Timeformat->getTimeFormat(), $event['Event']['start_date']);?></div>
  </div>

  <div class="box-row">
    <div class="box">Eindtijd</div>
    <div class="box">  <?php echo  $this->Time->format($this->Timeformat->getTimeFormat(), $event['Event']['end_date']);?></div>
  </div>


  <br />

    
  <div class="box-row">
    <?php   if($event['Event']['presale'] || $event['Event']['doorsale']){ echo '<div class="box">Prijs</div>';}?>
    <div class="box"> <?php   if($event['Event']['presale']){
    echo '<i>Voorverkoop</i>  &euro;'.preg_replace("/[^0-9.,]*/", "", $event['Event']['presale']).''; } ?>
    <?php 
    if($event['Event']['ticket']){
        echo ' (<a href="'.$event['Event']['ticket'].'">Koop <i class="icon fa fa-external-link"></i> </a>)';

    }
    ?>
  <?php   if($event['Event']['doorsale']){
      echo ' | <i>Deurverkoop</i> &euro;'.preg_replace("/[^0-9.,]*/", "", $event['Event']['doorsale']).' '; } ?></div>
  </div>

    <?php 
  if($event['Event']['facebook']){
    $str1 = preg_replace("/[^0-9.,]*/", "", $event['Event']['facebook']);
    $int1 = filter_var($str1, FILTER_SANITIZE_NUMBER_INT);
      echo '
    <div class="box-row">
      <div class="box">Facebook</div>
      <div class="box"> <a target="_BLANK" href="https://facebook.com/events/'.$int1.'"><i class="icon fa fa-facebook-official"></i></a></div>
    </div>';

  }
  ?>
  <?php 
  if($event['Event']['facebook'] && $event['Event']['ticketswap'] == 1){
      echo '
    <div class="box-row">
      <div class="box">Ticketswap</div>
      <div class="box"> <a href="https://www.ticketswap.nl/widget/dff138323c/facebook-event/'.$event['Event']['facebook'].'" onclick="window.open(\'https://www.ticketswap.nl/widget/dff138323c/facebook-event/'.$event['Event']['facebook'].'\', \'HTML-wijzer\', \'width=640,height=400,location=yes,scrollbars=no\'); return false"><i class="icon fa fa-external-link"></i></a> </div>
    </div>';

  }

  ?>


    <?php 
  if($event['Event']['web']){
    $str = preg_replace('#^https?://#', '', $event['Event']['web']);
      echo '
    <div class="box-row">
      <div class="box">Website</div>
      <div class="box"> <a target="_BLANK" href="http://'.$str.'"><i class="icon fa fa-external-link"></i></a> </div>
    </div>';

  }
  ?>
  <?php 
  if($event['Event']['guid']){
    $str = preg_replace("/[^0-9.,]*/", "", $event['Event']['guid']);
    $int = filter_var($str, FILTER_SANITIZE_NUMBER_INT);
      echo '
    <div class="box-row">
      <div class="box">Judge op Clubjudge</div>
      <div class="box"> <a target="_BLANK" href="http://clubjudge.com/events/'.$int.'"><i class="icon fa fa-external-link"></i></a> </div>
    </div>';

  }
  ?>


  <div class="box-row">
    <div class="box"></div>
    <div class="box"></div>
  </div>

</div>

<br />

<?php 
if($event['Event']['lineup']){
  echo '<b>Line-up:</b> ';
  echo "<br>";
echo $event['Event']['lineup'];}
echo "<br>";
?>
    <?php 
    echo "<br>";
if($event['Event']['notes']){
  echo '<b>Meer informatie over ';
  echo $event['Event']['name'];
  echo ":</b><br>";
echo $event['Event']['notes'];
echo "<br>";
}
?>
</div>

  <div id="right_column">

                  <?php
    if($this->Session->read('Auth.User.admin')) {
    echo '<a class="edit-event btn-social-share" href="/admin/events/edit/';
if($event['Event']['id']){
echo $event['Event']['id'];
       echo '"><i class="fa fa-pencil-square-o"></i><span class="share-font">EVENT</span></a>';
   }
   echo " ";
   echo '<a class="edit-event btn-social-share" href="/admin/venues/edit/';
if($event['Venue']['id']){
echo $event['Venue']['id'];
    echo '"><i class="fa fa-pencil-square-o"></i><span class="share-font">VENUE</span></a>';
   }
}
       ?>

<div id="banner-placeholder-6" class="banner-placeholder"></div>



    <div class="desktop">
<?php echo h($event['Venue']['address']); ?>, <?php echo h($event['City']['name']); ?><br>
<?php echo $this->Element('google_maps'); ?>
</div>
  </div>
  <div class="clear"></div>
   <?php if(!empty($relatedEvents)) : ?>
<!-- related events ingebouwd -->
<br>
    <h3><b>Vergelijkbare events : </b></h3>
    <div class="wrapperevents">
      <div class="longcontent">
    <ul class="body-product">
      <?php foreach($relatedEvents as $event) : ?>
      <li class="hover-effect"><a href="<?php echo Router::url('/event/' . $event['Country']['slug'] . '/' . $event['City']['slug'] . '/' . $event['Venue']['slug'] . '/' . $event['Event']['slug']);?>">
<div class="element-first">
<?php

if($event['Event']['logo']!='') {
  if(filter_var($event['Event']['logo'], FILTER_VALIDATE_URL)){ 
  echo '<img width="125px" height="125px" src="';
  echo $event['Event']['logo'];
  echo '">';

} else {

 echo '<img width="125px" height="125px" src="' . Router::url('/') . IMAGES_URL . 'logos/small/' . $event['Event']['logo'] . '">';
}
}

?>
<?php
if(!$event['Event']['logo']) {
  echo '<img width="125px" height="125px" src="/img/clubbieevent.png">';
}
?>

  </div>
  <div class="big-text"><?php echo $this->Text->truncate($event['Event']['name'],13); ?><span class="small-text-2"><?php echo date("d", strtotime($event['Event']['start_date']));echo ' ';echo date("M", strtotime($event['Event']['start_date'])); ?></span></div>
          <div class="big-text-2"><?php echo $this->Text->truncate($event['Venue']['name'],18); ?><br><?php echo $event['City']['name']; ?></div>
        </a></li>
      <?php endforeach; ?>
    </ul>
  </div>
    </div>
  <div class="clear"></div>
  <?php endif; ?>

  <?php
  if(Configure::read('evento_settings.disableComments') == false
  && (isset($event['Comment']) && !empty($event['Comment']) || $this->Session->read('Auth.User.id'))) { 
?>
  <div class="block-comments">
    <h2><?php echo __('Comments'); ?></h2>
<?php
  foreach($event['Comment'] as $comment) {
    echo $this->Element('comment_post', array('comment' => $comment));
  }
  echo $this->Element('comment_form');
}
    ?>
</div>
<script type="text/javascript">if(typeof wabtn4fg==="undefined"){wabtn4fg=1;h=document.head||document.getElementsByTagName("head")[0],s=document.createElement("script");s.type="text/javascript";s.src="http://clubbi.nl/js/whatsapp-button.js";h.appendChild(s);}</script>