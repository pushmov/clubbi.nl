<div class="container margin">
		
		<form action="<?php echo Router::url('play/' . $eventId); ?>" method="post" >
		<div class="content" style="">
				<?php
			if($eventDetail['logo']!='') {
				if(filter_var($eventDetail['logo'], FILTER_VALIDATE_URL)){ 
				echo ' <div style=""><a  data-mfp-src="' . $eventDetail['logo'] . '" class="image-link" alt="view image"><img style="max-width:140px;max-height:140px;float:right;width:auto;height:auto;cursor: zoom-in;" src="' . $eventDetail['logo'] . '" alt="venu-left-image" ></a></div>';
			}else{

			 echo ' <div style=""><a  data-mfp-src="' . Router::url('/') . IMAGES_URL . 'logos/big/' . $eventDetail['logo'] . '" class="image-link" alt="view image"><img style="max-width:140px;float:right;max-height:140px;width:auto;height:auto;cursor: zoom-in;" src="' . Router::url('/') . IMAGES_URL . 'logos/big/' . $eventDetail['logo'] . '" alt="venu-left-image" ></a></div>';
			}
			}
		?>
			<h1>Spelregels</h1>
			<p>Gooi 2 & win 2 <?php if($eventDetail['win_price_tickets'] == 1){ echo "kaarten";}?><?php if($eventDetail['win_price_guestlist'] == 1){ echo "gastenlijstplekken";}?><?php if($eventDetail['win_price_tickets'] == 0 && $eventDetail['win_price_guestlist'] == 0){echo "kaarten / gastenlijstplekken";}?> voor <b><?php echo h(ucfirst($eventDetail['name'])); echo "</b> op <b>";
			if($this->Time->format('w', $eventDetail['start_date']) == 0){
          echo "zondag";
      }elseif($this->Time->format('w', $eventDetail['start_date']) == 1){
          echo "maandag";
      }if($this->Time->format('w', $eventDetail['start_date']) == 2){
          echo "dinsdag";
      }if($this->Time->format('w', $eventDetail['start_date']) == 3){
          echo "woensdag";
      }if($this->Time->format('w', $eventDetail['start_date']) == 4){
          echo "donderdag";
        }if($this->Time->format('w', $eventDetail['start_date']) == 5){
          echo "vrijdag";
        }if($this->Time->format('w', $eventDetail['start_date']) == 6){
          echo "zaterdag";
        } echo ' '; echo date("d", strtotime($eventDetail['start_date']));echo ' ';echo date("F", strtotime($eventDetail['start_date']));
        echo "</b> van ";
        echo ' ' . $this->Time->format($this->Timeformat->getTimeFormat(), $eventDetail['start_date']);
      
      echo ' -';
      
        echo ' ' . $this->Time->format($this->Timeformat->getTimeFormat(), $eventDetail['end_date']);
      ?>
		!<br><br><b>
				Vereist:<br>
				- Je bent minimaal <?php if($eventDetail['id'] == 910) { echo '16 jaar';} else {echo "18 jaar";}?>
<br>
				- Geboortedatum, voor- en achternaam moet geldig en aantoonbaar op identiteitsbewijs zijn<br>
				- Je kan 1 keer in de 24 uur meedoen aan deze winactie</b><br>
<br><b>Het spel</b><br>
				Als je <font color="green">het getal 2</font> gooit, dan heb je 2 <?php if($eventDetail['win_price_tickets'] == 1){ echo "kaarten";}?><?php if($eventDetail['win_price_guestlist'] == 1){ echo "gastenlijstplekken";}?><?php if($eventDetail['win_price_tickets'] == 0 && $eventDetail['win_price_guestlist'] == 0){echo "kaarten / gastenlijstplekken";}?> gewonnen en ontvang je een bevestigingsmail. Als je <font color="red">een ander getal dan 2</font> hebt gegooit, dan heb je verloren. Je kan na 24 uur weer deelnemen aan de desbetreffende winactie.
<br><br>
				Deze <?php if($eventDetail['win_price_tickets'] == 1){ echo "kaarten";}?><?php if($eventDetail['win_price_guestlist'] == 1){ echo "gastenlijstplekken";}?><?php if($eventDetail['win_price_tickets'] == 0 && $eventDetail['win_price_guestlist'] == 0){echo "kaarten / gastenlijstplekken";}?> zijn naamsgebonden en kunnen niet doorverkocht / weggeven worden.
			</p><br>
			<div style="text-align:center">
				<input class="btn btn-continue" name="tos" type="submit" value="Ik accepteer spelregels en voorwaarden"/>
			</div>
		</div>
		</form>
		
	</div>
</div>