<div class="container text-center margin">
	<div class="losepage" style="text-align:center;padding:30px 0">
		
		<h3>You Lose..</h3>
		<p>Next time better, you can't play for 24 hours</p>
		
		<button onclick="window.location='/'" class="btn btn-primary">Return to Home page</button>
		
	</div>
</div>