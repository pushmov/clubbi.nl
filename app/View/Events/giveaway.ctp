<div id="DIV_1">
	<img src="/img/header.png" id="IMG_2" alt='Clubbie, uitgaan en feesten in Amsterdam, Utrecht, Rotterdam.' />
</div>

<div class="container content">
	<?php echo $this->Session->flash(); ?>
	<div id="center_column" class="events-index" style="width:100%">
    <div class="rowwin">
       <div class="span3">
        <img height="50px" src="/img/flat-party.png"><br>
      3. Feesten maar!
    </div>
      
     <div class="span3">
      <img height="50px" src="/img/flat-dice.png"><br>
      2. 'Roll the dice' en gooi 2
    </div>
<div class="span3">
 <img height="50px" src="/img/flat-events.png"><br>
       1. Kies een winactie
    </div>
  </div>
    <table class="tg">
     
<?php
	// show events filters
	//echo $this->Element('index_filter');

	 // load events
  if(!empty($events)) {
    $temp = array();
    foreach($events as $event) {
      //print_r($event);
      $datummm = "";
      if($this->Time->format('w', $event['Event']['start_date']) == 0){
          $datummm .= "Zondag";
        }elseif($this->Time->format('w', $event['Event']['start_date']) == 1){
          $datummm .= "Maandag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 2){
          $datummm .= "Dinsdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 3){
          $datummm .= "Woensdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 4){
          $datummm .= "Donderdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 5){
          $datummm .= "Vrijdag";
        }if($this->Time->format('w', $event['Event']['start_date']) == 6){
          $datummm .= "Zaterdag";
        } 


   $datummm .=  date(" j ",strtotime($event['Event']['start_date']));

         if($this->Time->format('n', $event['Event']['start_date']) == 1){
                    $datummm .= "januari";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 2){
                    $datummm .= "februari";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 3){
                    $datummm .= "maart";
                  } 
        if($this->Time->format('n', $event['Event']['start_date']) == 4){
                    $datummm .= "april";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 5){
                    $datummm .= "mei";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 6){
                    $datummm .= "juni";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 7){
                    $datummm .= "juli";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 8){
                    $datummm .= "augustus";
                  }
        if($this->Time->format('n', $event['Event']['start_date']) == 9){
                    $datummm .= "september";
                  }
           if($this->Time->format('n', $event['Event']['start_date']) == 10){
                              $datummm .= "oktober";
                            }
          if($this->Time->format('n', $event['Event']['start_date']) == 11){
                                        $datummm .= "november";
                                      }
                                      if($this->Time->format('n', $event['Event']['start_date']) == 12){
                                                  $datummm .= "december";
                                                }
      $datummm .=  date(" Y",strtotime($event['Event']['start_date']));
      if(!in_array( $datummm, $temp)){
     
      

          echo '<tr>
            <th class="tg-031" colspan="4">'.$datummm.' </th>
          </tr>';
          echo '<tr>
                <th class="tg-031 left" colspan="1"></th>
                <th class="tg-031 left" colspan="1"></th>
                <th class="tg-031 left" colspan="1"></th>
              </tr>';
            $temp[] =   $datummm;
      }
   
      echo $this->Element('win_post', array('event'=>$event));
    }
echo "</table>";
    echo $this->Element('paginator');
  } else {
    echo '<p class="empty-message">'.__('Unfortunately there are no giveaways at the moment. Please check tomorrow.').'</p>';
  }
?>


	</div>
	<div class="clear"></div>
	
</div>