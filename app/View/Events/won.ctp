<div class="container text-center margin content">
	<div class="losepage" style="text-align:center;">
		
		<h3>Jij hebt gewonnen!</h3>
		<?php
   // print_r ($fullEvt);
		//EMAIL STUREN
						 
				$email = new CakeEmail();
		//			$email->viewVars(array(
		//				'user' => $user['first_name'] ,
		//				'event' => $event['name']
		//			));
     //   print_r($authUser);
          $tijd12 = "";
        if($this->Time->format('w', $event['Event']['start_date']) == 0){
                 $tijd12 .= "zondag";
               }elseif($this->Time->format('w', $event['Event']['start_date']) == 1){
                 $tijd12 .=  "maandag";
               }if($this->Time->format('w', $event['Event']['start_date']) == 2){
                 $tijd12 .=  "dinsdag";
               }if($this->Time->format('w', $event['Event']['start_date']) == 3){
                 $tijd12 .=  "woensdag";
               }if($this->Time->format('w', $event['Event']['start_date']) == 4){
                 $tijd12 .=  "donderdag";
               }if($this->Time->format('w', $event['Event']['start_date']) == 5){
                 $tijd12 .=  "vrijdag";
               }if($this->Time->format('w', $event['Event']['start_date']) == 6){
                 $tijd12 .=  "zaterdag";
               } $tijd12 .=  ' ';
               $tijd12 .=  date("d", strtotime($event['Event']['start_date']));
               $tijd12 .=  '-';
               $tijd12 .=  date("m", strtotime($event['Event']['start_date']));
               $tijd12 .=  '-';
               $tijd12 .=  date("Y", strtotime($event['Event']['start_date']));
               $tijd12 .=  ' om ';
               $tijd12 .=  $this->Time->format($this->Timeformat->getTimeFormat(), $event['Event']['start_date']);
              $guestlisttime .= $this->Time->format($this->Timeformat->getTimeFormat(), $event['Event']['win_guestlisttime']);

					$email->from(Configure::read('evento_settings.systemEmail'));
					$email->to($authUser['email']); //Dit moet gebruikers mail adres worden
					$email->subject(__('Gefeliciteerd '.$authUser['first_name'].'! Je hebt tickets gewonnen'));
					//$email->template('won_message_tickets'); // Dit is het bericht zodra Tickets worden weggegeven
					$email->emailFormat('html');
          if($event['Event']['win_price_tickets'] == 1){
					$email->send('<p>Beste '.$authUser['first_name'].',</p>
<p></p>
<p>Gefeliciteerd je hebt gewonnen!</p> 
<p>Leuk dat je meegedaan hebt met de \'Roll the dice\' winactie op Clubbie.nl! Jij hebt 2 kaarten gewonnen voor <b>'.$event['Event']['name'].'</b> in de  <b>'.$fullEvt['Venue']['name'].'</b>  op  '.$tijd12.'!  Let op: de kaarten die je ontvangt van de organisatie zijn naamsgebonden, dus zorg ervoor dat je jouw identiteitsbewijs niet vergeet. De kaarten ontvang je in de week van het evenement.</p>
<p>Mocht je onverhoopt niet kunnen komen, meld dit dan gelijk bij ons via info@clubbie.nl.</p>
<p>Meer informatie over het evenement vind je natuurlijk op Clubbie.nl!</p>
<p>Wij wensen jou en je vriend(in) onwijs veel plezier toe.</p>
<p></p>
<p>Groet,</p>
<p>Team Clubbie.nl</p>'); 
        }else {

                    $email->send('<p>Beste '.$authUser['first_name'].',</p>
          <p></p>
          <p>Gefeliciteerd je hebt gewonnen!</p> 
          <p>Leuk dat je meegedaan hebt met de \'Roll the dice\' winactie op Clubbie.nl! Jij hebt 2 gastenlijstplekken gewonnen voor <b>'.$event['Event']['name'].'</b> in de  <b>'.$fullEvt['Venue']['name'].'</b>  op  '.$tijd12.'! Let op: de gastenlijstplekken zijn naamsgebonden,  zorg ervoor dat je jouw identiteitsbewijs niet vergeet. De gastenlijst sluit om '.$guestlisttime.' uur, dus zorg dat je op tijd binnen bent.</p>
          <p>Mocht je onverhoopt niet kunnen komen, meld dit dan gelijk bij ons via info@clubbie.nl.</p>
          <p>Meer informatie over het evenement vind je natuurlijk op Clubbie.nl!</p>
          <p>Wij wensen jou en je vriend(in) onwijs veel plezier toe.</p>
          <p></p>
          <p>Groet,</p>
          <p>Team Clubbie.nl</p>');
        }
					?>
					<p>Gefeliciteerd jij hebt 2 
            <?php if($event['Event']['win_price_tickets'] == 1){ echo "kaarten";}
            elseif($event['Event']['win_price_guestlist'] == 1){ echo "gastenlijstplekken";}
            else{echo "kaarten / gastenlijstplekken";}?> 
            gewonnen. <br>Je hebt zojuist een email van ons ontvangen. Mocht je deze niet kunnen vinden, <b>controleer dan ook je SPAM box</b>.<br>Wij wensen jou onwijs veel plezier toe!</p>

					<div id="menu-outer">
  <div class="table">
    <p><b>Nodig nu jouw vrienden uit om mee te gaan of om ook deel te nemen!</b></p>
 <ul id="venu-share">
<li><a class="btn-social-share share-whatsapp app" href="whatsapp://send?text=Ik heb kaarten gewonnen voor <?php echo $event['Event']['name']; ?> op http://clubbie.nl/win! Doe ook mee, dan gaan we samen!"><i class="fa fa-fw fa-whatsapp"></i><span class="share-font">SHARE</span></a></li>
      <li><a onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=http://clubbie.nl/win','Facebook share',
'width=400,height=200,scrollbars=yes,toolbar=yes,location=yes'); return false" class="desktop share-facebook btn-social-share" target="_BLANK" href="https://www.facebook.com/sharer/sharer.php?u=http://clubbie.nl/win"><i class="fa fa-fw fa-facebook-official"></i><span class="share-font">SHARE</span></a></li>
      <li><a onclick="window.open('https://twitter.com/home?status=http://clubbie.nl/win','Twitter share',
'width=400,height=200,scrollbars=yes,toolbar=yes,location=yes'); return false" class="desktop share-twitter btn-social-share" target="_BLANK"  href="https://twitter.com/home?status=http://clubbie.nl/win"><i class="fa fa-fw fa-twitter"></i><span class="share-font">TWEET</span></a></li>

    </ul><!--/.venu-share-->
</div>
</div><br><br>
		<button onclick="window.location='<?php echo Router::url('/'.$slug); ?>'" class="btn btn-continue">Ga terug naar de event pagina</button>
		
		
	</div>
</div>