	
	<?php
		
		$month = '';
		$year = '';
		$day = '';
		
		if($user['User']['birthdate'] != '') {
			$seg = explode(' ', $user['User']['birthdate']);
			$date = explode('-', $seg[0]);
			
			$month = $date[1];
			$year = $date[0];
			$day = $date[2];
		}
		
	?>
	
	<div class="input datetime">
		<label for="UserBirthdateMonth">Geboortedatum</label>
		<select id="UserBirthdateDay" autocomplete="off" name="data[User][birthdate][day]">
			<?php for($i=1;$i<=31;$i++) : ?>
			<option value="<?php echo sprintf('%02s', $i); ?>" <?php if((int)$day==$i) echo "selected";?>><?php echo $i; ?></option>
			<?php endfor; ?>
		</select>-
		<select id="UserBirthdateMonth" autocomplete="off" name="data[User][birthdate][month]">
			<?php for($m=1;$m<=12;$m++) : ?>
			<option value="<?php echo sprintf('%02s', $m); ?>" <?php if((int)$month==$m) echo "selected";?>><?php echo date('F', mktime( 0, 0, 0, $m + 1, 0, 0, 0 )); ?></option>
			<?php endfor; ?>
		</select>-
		<select id="UserBirthdateYear" autocomplete="off" name="data[User][birthdate][year]">
			<?php for($y=1940;$y<=1996;$y++) : ?>
			<option <?php if($year == $y) echo "selected";?> value="<?php echo $y?>"><?php echo $y; ?></option>
			<?php endfor; ?>
		</select>
	</div>