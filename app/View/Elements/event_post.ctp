<?php
        if($event['Event']['promoted']) { 
echo "<tr class='promoted'>";
} else {
	echo "<tr>";
}
?>
<?php

/*	if($event['Event']['logo'] != '') {
	$event['Event']['logo'] =	str_replace("original","square_160",$event['Event']['logo']);
		$logo = $this->Html->image( $event['Event']['logo'], array('class'=>'photo'));
	} else {
		$logo = '';
	}

	echo $this->Html->link($logo
		, array('controller'=>'events', 'action'=>'view', $event['Country']['slug']
		, $event['City']['slug']
		, $event['Venue']['slug'], $event['Event']['slug'])
		, array('escape'=>false, 'class'=>'event-index-logo'));

	if($event['Event']['promoted']) {
		echo '<div class="promoted-bar">' . __('Promoted') . '</div>';
	}*/
?>
	<?php
        if($event['Event']['promoted'] && $event['Event']['logo']!='') { 
        	if(filter_var($event['Event']['logo'], FILTER_VALIDATE_URL)){ 
            echo '<th class="tg-031epromoted left paddingleft">';
             echo $this->Html->image(''.$event['Event']['logo'], array('alt'=>'Flyer '.$event['Event']['name'], 'url' => array('controller' => 'events', 'action' => 'view', 
                    $event['Country']['slug'], $event['City']['slug'], 
                    $event['Venue']['slug'], $event['Event']['slug']),'class'=>'desktop','style' => 'width: 100px;height: 100px;float: left;margin-right: 5px;')); 
}else {

  echo '<th class="tg-031epromoted left paddingleft">';
echo $this->Html->image(''. Router::url('/') . IMAGES_URL . 'logos/small/' . $event['Event']['logo'],array('alt'=>'Flyer '.$event['Event']['name'], 'url' => array('controller' => 'events', 'action' => 'view', 
                    $event['Country']['slug'], $event['City']['slug'], 
                    $event['Venue']['slug'], $event['Event']['slug']),'class'=>'desktop','style' => 'width: 100px;height: 100px;float: left;margin-right: 5px;')); 
}

        } 
        ?>
        <?php
 if($event['Event']['promoted'] && !$event['Event']['logo']) { 
 echo '<th class="tg-031epromoted left paddingleft">';
  echo $this->Html->image('http://clubbie.nl/img/clubbieevent.png', array('alt'=>'Clubbie.nl', 'url' => array('controller' => 'events', 'action' => 'view', 
                    $event['Country']['slug'], $event['City']['slug'], 
                    $event['Venue']['slug'], $event['Event']['slug']),'class'=>'desktop','style' => 'width: 100px;height: 100px;float: left;margin-right: 5px;')); 
 }
 ?>
 <?php  
 if($event['Event']['promoted'] == 0) {
        	echo '<th class="tg-031e left paddingleft">';
        }
        ?>
        <?php
         if($event['Event']['promoted']) {
	echo $this->Html->link($this->Text->truncate(ucfirst($event['Event']['name']), 80)
		, array('controller'=>'events', 'action'=>'view'
		, $event['Country']['slug'], $event['City']['slug']
		, $event['Venue']['slug'], $event['Event']['slug']), array('style'=>'font-size:15px;font-weight:bold;','class'=>'url summary'));
} else {
	echo $this->Html->link($this->Text->truncate(ucfirst($event['Event']['name']), 80)
		, array('controller'=>'events', 'action'=>'view'
		, $event['Country']['slug'], $event['City']['slug']
		, $event['Venue']['slug'], $event['Event']['slug']), array('class'=>'url summary mobilebold'));
}
?>
<?php if($event['Event']['promoted'] && $event['Event']['lineup']) {
  echo "<br><br>";
  echo "<b>Line-up: </b>";
	echo $this->Text->truncate($event['Event']['lineup'], 100, array('exact'=>false, 'html'=>true));
	} ?>	
	</th>
		<?php
        if($event['Event']['promoted']) { 
            echo '<th class="tg-031epromoted left">';
        } else {
        	echo '<th class="tg-031e left">';
        }
        ?>
			<?php
	//echo $this->Html->link($event['Category']['name'], array('action'=>'index'
	//	, $country, $city,$venue, $event['Category']['slug'], $current_tag, $year, $month, $day)
	//	, array('class'=>'category'));
	//echo '. ';


echo $this->Html->link(ucfirst($event['Venue']['name']), array('action'=>'index'
		, $event['Country']['slug'], $event['City']['slug'], $event['Venue']['slug']
		, $category, $current_tag, $year, $month, $day), array('class'=>'fn org')) . '';


	//$venue .= $this->Html->link(__d('countries', $event['Country']['name']), array('action'=>'index'
	//	, $event['Country']['slug'], 'all-cities', 'all-venues', $category, $current_tag, $year, $month, $day)
	//	, array('class'=>'country-name')) . '.';



?>
			</th>
	<?php
        if($event['Event']['promoted']) { 
            echo '<th class="tg-031epromoted left">';
        } else {
        	echo '<th class="tg-031e left">';
        }
        ?>
			
<?php

echo $this->Html->link(ucfirst($event['City']['name'])
	, array('action'=>'index', $event['Country']['slug'], $event['City']['slug'], 'all-venues'
	, $category, $current_tag, $year, $month, $day), array('escape'=>false, 'class'=>'locality'));

/*	$indexDay = ($day)? sprintf('%02d', $day) : '01';
	$indexMonth = ($month)? date('m', strtotime($month)) : date('m');
	if($year) {
		$indexYear = $year;
	}
	else {
		$indexYear = date('Y');
		$indexDay = date('d');
	}
	$selectedDate = strtotime($indexYear . '-' . $indexMonth . '-' . $indexDay);

	$indexDate = strtotime($event['Event']['start_date']);
	if(date('Y-m-d', $indexDate) < date('Y-m-d', $selectedDate)){
		$indexDate = strtotime(date('Y-m-d', $selectedDate));
	}

	echo $this->Timeformat->getFormattedDate($indexDate);*/
?>
	</th>		
	<?php
        if($event['Event']['promoted'] == 1) { 
            echo '<th class="tg-031epromoted left">';
        } else {
        	echo '<th class="tg-031e left">';
        }
        ?>
        <?php
        $today = date('Y/m/d H:i');
if($event['Event']['win'] == 1 && $today >= $event['Event']['win_start_date']) { 
echo $this->Html->image('/img/win.png', array('url' => array('controller' => 'events', 'action' => 'view', 
                    $event['Country']['slug'], $event['City']['slug'], 
                    $event['Venue']['slug'], $event['Event']['slug']))); 
        }
       //echo $today;
        ?>

		</th>
		</tr>