<?php
if($event['Event']['promoted']) {
echo "<tr class='promoted'>";
}
if(!$event['Event']['promoted']) {
echo "<tr>";
}
?>
<?php

/*	if($event['Event']['logo'] != '') {
	$event['Event']['logo'] =	str_replace("original","square_160",$event['Event']['logo']);
		$logo = $this->Html->image( $event['Event']['logo'], array('class'=>'photo'));
	} else {
		$logo = '';
	}

	echo $this->Html->link($logo
		, array('controller'=>'events', 'action'=>'view', $event['Country']['slug']
		, $event['City']['slug']
		, $event['Venue']['slug'], $event['Event']['slug'])
		, array('escape'=>false, 'class'=>'event-index-logo'));

	if($event['Event']['promoted']) {
		echo '<div class="promoted-bar">' . __('Promoted') . '</div>';
	}*/
?>
	<?php
        if($event['Event']['logo']!='') { 
        	if(filter_var($event['Event']['logo'], FILTER_VALIDATE_URL)){ 
            echo '<th class="tg-031epromoted left">';
             echo $this->Html->image(''.$event['Event']['logo'], array('alt'=>'Flyer '.$event['Event']['name'], 'url' => array('controller' => 'events', 'action' => 'view', 
                    $event['Country']['slug'], $event['City']['slug'], 
                    $event['Venue']['slug'], $event['Event']['slug']),'class'=>'desktop','style' => 'width: 100px;height: 100px;float: left;margin-right: 5px;')); 
}else {

  echo '<th class="tg-031epromoted left">';
echo $this->Html->image(''. Router::url('/') . IMAGES_URL . 'logos/small/' . $event['Event']['logo'],array('alt'=>'Flyer '.$event['Event']['name'], 'url' => array('controller' => 'events', 'action' => 'view', 
                    $event['Country']['slug'], $event['City']['slug'], 
                    $event['Venue']['slug'], $event['Event']['slug']),'class'=>'desktop','style' => 'width: 100px;height: 100px;float: left;margin-right: 5px;')); 
}

        } 
        ?>
        <?php
 if(!$event['Event']['logo']) { 
 echo '<th class="tg-031epromoted left">';
  echo $this->Html->image('http://clubbie.nl/img/clubbieevent.png', array('alt'=>'Clubbie.nl', 'url' => array('controller' => 'events', 'action' => 'view', 
                    $event['Country']['slug'], $event['City']['slug'], 
                    $event['Venue']['slug'], $event['Event']['slug']),'class'=>'desktop','style' => 'width: 100px;height: 100px;float: left;margin-right: 5px;')); 
 }
 ?>
        <?php
	echo $this->Html->link($this->Text->truncate(ucfirst($event['Event']['name']), 80)
		, array('controller'=>'events', 'action'=>'view'
		, $event['Country']['slug'], $event['City']['slug']
		, $event['Venue']['slug'], $event['Event']['slug']), array('style'=>'font-size:15px;font-weight:bold;','class'=>'url summary'));
	echo "<br>";
echo $this->Html->link(ucfirst($event['Venue']['name']), array('action'=>'index'
		, $event['Country']['slug'], $event['City']['slug'], $event['Venue']['slug']
		, $category, $current_tag, $year, $month, $day), array('class'=>'fn org')) . ', ';
echo $this->Html->link(ucfirst($event['City']['name'])
	, array('action'=>'index', $event['Country']['slug'], $event['City']['slug'], 'all-venues'
	, $category, $current_tag, $year, $month, $day), array('escape'=>false, 'class'=>'locality'));

?>
<?php if($event['Event']['lineup']) {
  echo "<br><br>";
  echo "<b>Line-up: </b>";
	echo $this->Text->truncate($event['Event']['lineup'], 100, array('exact'=>false, 'html'=>true));
	} ?>	
	</th>
		<?php
            echo '<th class="tg-031epromoted left">';
        ?>
			<?php
	//echo $this->Html->link($event['Category']['name'], array('action'=>'index'
	//	, $country, $city,$venue, $event['Category']['slug'], $current_tag, $year, $month, $day)
	//	, array('class'=>'category'));
	//echo '. ';


	//$venue .= $this->Html->link(__d('countries', $event['Country']['name']), array('action'=>'index'
	//	, $event['Country']['slug'], 'all-cities', 'all-venues', $category, $current_tag, $year, $month, $day)
	//	, array('class'=>'country-name')) . '.';



?>
			</th>
	<?php
	if($event['Event']['promoted']) {
            echo '<th class="tg-031epromoted left">';
        }
        if(!$event['Event']['promoted']) {
            echo '<th class="tg-031epromoted left">';
        }
        ?>
        <?php
       echo '<div class="playbuttonpromo center desktop">';
echo $this->Html->link($this->Html->image("/img/winpromoted.png", array()).' Roll the dice!', array('controller'=>'events', 'action'=>'win'
		, $event['Country']['slug'], $event['City']['slug']
		, $event['Venue']['slug'], $event['Event']['slug']), array('escape'=>false)); 
echo "</div>";
        ?>

        <?php
       echo '<div class="playbuttonpromo center app">';
echo $this->Html->link($this->Html->image("/img/winpromoted.png", array()).' Roll!', array('controller'=>'events', 'action'=>'win'
		, $event['Country']['slug'], $event['City']['slug']
		, $event['Venue']['slug'], $event['Event']['slug']), array('escape'=>false)); 
echo "</div>";
        ?>
		</th>
		</tr>