<?php
/**
 * meta description for the events listings page (aka homepage)
 * generate the description content based on the app slogan and page number
 */
if($this->request->params['controller']=='events' && $this->request->params['action']=='index') {
?>
<meta name="description" content="<?php 
echo h(__(Configure::read('evento_settings.appSlogan'))); 
if($this->Paginator->current() > 1) {
	echo ' - ' . __('page') . ' ' . $this->Paginator->current();
}
?>" />
<?php
}
/**
 * meta description and keywords for the event view page
 * generate the description content based on the event description and the keywords meta
 * based on the event name, city, country and tags.
 */
else if($this->request->params['controller']=='events' && $this->request->params['action']=='view') {
?>
<meta name="description" content="<?php echo str_replace('\n',' ',h($this->Text->truncate($event['Event']['notes'], 150,array('ending'=>'...', 'exact'=>false))));
?>" />
<meta name="keywords" content="<?php 
		$meta_str = $event['Event']['name'].', '.h($event['City']['name']).', '.__($event['Country']['name']);
		$meta_str .= ', ' . $event['Category']['name'];
	  	if(!empty($event['Tag'])) $meta_str.= ",".implode(',',Set::extract('/Tag/name',$event));
		echo h($meta_str);
       ?>" />
<?php 
	}
?>