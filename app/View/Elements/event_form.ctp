<?php
if(!isset($page)) $page = null;
echo $this->Html->script( 
'http://maps.googleapis.com/maps/api/js?key='.Configure::read('evento_settings.googleMapKey') . '&amp;sensor=false', array('inline'=>false));

if($this->request->params['action']=='add' || $this->request->params['action']=='admin_add') {
	echo $this->Form->create('Event', array('type'=>'file', 'id'=>'EventAddForm', 'class'=>'event-form', 'novalidate'=>true));
}
else {
	echo $this->Form->create('Event', array('url'=>array('action'=>'edit', 'page'=>$page), 'type'=>'file', 
		'id'=>'EventAddForm', 'class'=>'event-form', 'novalidate'=>true));
}
echo $this->Form->hidden('Event.id');
echo $this->Form->hidden('Venue.lat');
echo $this->Form->hidden('Venue.lng');
if(!Configure::read('evento_settings.adminVenues')) echo $this->Form->hidden('Event.venue_id');

echo $this->Form->input('Event.name',array('label'=>__('Event Name'), 'autofocus'=>true, 'autocomplete'=>'off'));

echo '<div id="form-logo-box">';
if(isset($this->request->data['Event']['logo']) && !empty($this->request->data['Event']['logo'])) {
	$this->request->data['Event']['logo'] = 'logos/small/'.$this->request->data['Event']['logo'];
	echo $this->Html->image($this->request->data['Event']['logo'], array('alt'=>__('logo')));
}
else {
	echo $this->Html->image('default_logo.jpg');
};
echo '<div id="logo-input">';
echo $this->Form->input('filedata', array('type'=>'file', 'label'=>__('Event flyer')));
echo '</div><div class="clear"></div></div>';
if(isset($this->request->data['Event']['id']) && !empty($this->request->data['Event']['id']) &&
	!empty($this->request->data['Event']['logo'])) {
		echo '<div id="delete-logo-block">';
		echo $this->Form->input('delete_logo', array('type'=>'checkbox', 'div'=>false,
			'label'=>__('Delete event flyer'), 'class'=>'checkbox-input'));
		echo '</div>';
}
if($this->Session->read('Auth.User.admin') && isset($this->request->params['admin'])) {
	echo '<a target="_blank" href="';
	echo "http://clubbie.nl/user/";
	echo $event['Event']['user_id'];
	echo '"><label for="EventOwner">Click here for event owner</label></a>';
	echo "</br>";
}
echo $this->Form->input('Event.genre',array('label'=>__('Genres')));
echo $this->Form->input('Event.presale',array('label'=>__('Voorverkoop')));
echo $this->Form->input('Event.ticket',array('label'=>__('Ticket link')));
echo $this->Form->input('Event.doorsale',array('label'=>__('Deurverkoop')));
echo $this->Form->input('Event.lineup',array('label'=>__('Line-up')));
echo $this->Form->input('Event.facebook',array('label'=>__('Facebook event url')));
if($this->Session->read('Auth.User.admin') && isset($this->request->params['admin'])) {
  echo $this->Form->input('ticketswap', array('type'=>'checkbox', 'label'=>__('Ticketswap is enabled')));
}
echo $this->Form->input('Event.notes',array('label'=>__('Description')));
if($this->Session->read('Auth.User.admin') && isset($this->request->params['admin'])) {
echo $this->Form->input('Event.category_id', array('type'=>'select', 'options'=>$categories, 'empty'=>true,
	'label'=>__('Category')));
}

echo '<div class="input text">';
if(!Configure::read('evento_settings.adminVenues')) {

echo '<div id="venue-name-div">';
echo $this->Form->input('Venue.name', array('label'=>__('Venue Name'), 'id'=>'VenueName', 'div'=>false, 'autocomplete'=>'off'));
?><div id="VenueName_autoComplete" data-url="<?php echo Router::url(array('admin'=>false, 'controller'=>'venues', 'action'=>'autoComplete')); ?>" class="auto_complete"></div>
<?php
echo '<div id="venue-name-block"><div id="venue-data"></div><a href="#" id="reset-venue">'.__('Change Venue').'</a></div>';

echo '</div>';
echo '</div>';

$venue_form_display = 'none';

if((!isset($this->request->data['Event']['venue_id']) || !$this->request->data['Event']['venue_id']) && (isset($this->request->data['Venue']['name']) ||
 	isset($this->request->data['Venue']['City']['name']) || isset($this->request->data['Venue']['City']['country_id']) ||
 	isset($this->request->data['Venue']['address']))) {
		$venue_form_display = 'block';
}
else if(isset($this->request->data['Event']['venue_id']) && $this->request->data['Event']['venue_id']) {
	echo '<input type="hidden" id="hiddenVenueName" value="'.h(str_replace('"','\"',h(str_replace('"','\"',
		$this->request->data['Venue']['name'])))).'"/>';
	echo '<input type="hidden" id="hiddenCityName" value="'
	.h(
		str_replace('"','\"',$this->request->data['Venue']['name'])
	).'"/>';
	echo '<input type="hidden" id="hiddenCountryName" value="'.h(str_replace('"','\"',
		__d('countries', $this->request->data['Venue']['City']['Country']['name']))).'"/>';
	echo '<input type="hidden" id="hiddenVenueAddress" value="'.h(str_replace('"','\"',$this->request->data['Venue']['address'])).'"/>';
}

echo '<div id="venue-form" style="display:'.$venue_form_display.'">';
if(!Configure::read('evento_settings.country_id')) {
	echo $this->Form->input('City.country_id', array('options'=>$countries, 'empty'=>true,
		'label'=>__('Country')));
}
else {
	echo $this->Form->hidden('City.country_name', array('value'=>$country_name));
	echo $this->Form->hidden('City.country_id', array('value'=>Configure::read('evento_settings.country_id')));
}

if(!Configure::read('evento_settings.city_name')) {
	echo '<div class="input">';
	echo $this->Form->input('City.name', array('label'=>__('City'), 'div'=>false, 'id'=>'CityName'));
	?>
		<div id="CityName_autoComplete" data-url="<?php echo Router::url(array('admin'=>false, 'controller'=>'cities', 'action'=>'autoComplete')); ?>" data-rfield="country" class="auto_complete"></div>
	<?php
	echo '</div>';
}
else {
	echo $this->Form->hidden('City.name', array('value'=>$default_city, array('autocomplete'=>'off')));
}

echo $this->Form->input('Venue.address', array('autocomplete'=>'off'));

$latlng = '';
if(isset($this->request->data['Event']['lat']) && isset($this->request->data['Event']['lng'])) { 
	$latlng = ' data-lat="'.$this->request->data['Event']['lat'].'" data-lng="'.$this->request->data['Event']['lng'].'"';
}

//google map
echo '<div class="input"><div id="map" class="map-edit" style="display:block;"'. $latlng .'></div></div>';


}
else {
	echo $this->Form->input('Event.venue_id', array('options'=>$venues));
}

echo '</div>';
echo '<div id="start_date_input">';
echo $this->Form->input('start_date',array('dateFormat'=>'DMY',
							'timeFormat'=>Configure::read('evento_settings.timeFormat'),
							'minYear' => date('Y'),
							'maxYear' => date('Y')+5,
							'separator' => ""
							));
echo '</div>';
echo $this->Form->input('end_date_check', array('type'=>'checkbox', 'id'=>'end_date_check', 'label'=>__('Add end date'), 'checked'=>isset($end_date_checked)));
echo '<div id="end_date_input">';
echo $this->Form->input('end_date',array(  'dateFormat'=>'DMY',
							'timeFormat'=>Configure::read('evento_settings.timeFormat'),
							'minYear' => date('Y'),
							'maxYear' => date('Y')+5,
							'separator' => "",
							'class'=>'EndDate'
							));
echo '</div>';

//echo $this->Element('event_repeat_form');


echo $this->Form->input('web', array('label'=>__('Website'), 'autocomplete'=>'off'));

//echo '<div class="input text">';
//if($this->Session->read('Auth.User.admin') && isset($this->request->params['admin'])) {
//echo $this->Form->input('tags', array('id'=>'Tags', 'div'=>false, 'autocomplete'=>'off', 'label'=>__('Tags') 
//. ' <span class="muted">' . __('comma separated') . '</span>'));
//}

?>
<!--	<div id="Tags_autoComplete" data-url="<?php echo Router::url(array('admin'=>false, 'controller'=>'tags', 'action'=>'autoComplete')); ?>" class="auto_complete"></div>-->

<?php
//echo '</div>';
if($this->Session->read('Auth.User.admin') && isset($this->request->params['admin'])) {
	if(isset($this->request->data['Event']['win'])) {
		echo $this->Form->input('Event.win', array('type'=>'checkbox', 'id'=>'Win', 'label'=>__('Add giveaway'), 
			'checked' => ($this->request->data['Event']['win'] == '1') ? 'checked' : ''));
	}
	else {
		echo $this->Form->input('Event.win', array('type'=>'checkbox', 'id'=>'Win', 'label'=>__('Add giveaway'), 
			'checked' => ''));
	}
	echo "<div id='giveawayz' style='display:none'>";
echo "<label>Does the winner get tickets or guestlistspots?</label>";
	if(isset($this->request->data['Event']['win_price_tickets'])) {
		echo $this->Form->input('Event.win_price_tickets', array('type'=>'checkbox', 'id'=>'Win_price_tickets', 'label'=>__('Tickets'), 
			'checked' => ($this->request->data['Event']['win_price_tickets'] == '1') ? 'checked' : ''));
	}
	else {
		echo $this->Form->input('Event.win_price_tickets', array('type'=>'checkbox', 'id'=>'Win_price_tickets', 'label'=>__('Tickets'), 
			'checked' => ''));
	}
	if(isset($this->request->data['Event']['win_price_guestlist'])) {
		echo $this->Form->input('Event.win_price_guestlist', array('type'=>'checkbox', 'id'=>'Win_price_guestlist', 'label'=>__('Guestlist'), 
			'checked' => ($this->request->data['Event']['win_price_guestlist'] == '1') ? 'checked' : ''));
	}
	else {
		echo $this->Form->input('Event.win_price_guestlist', array('type'=>'checkbox', 'id'=>'Win_price_guestlist', 'label'=>__('Guestlist'), 
			'checked' => ''));
	}
	if(isset($this->request->data['Event']['win_guestlisttime'])) {

		echo $this->Form->input('Event.win_guestlisttime',array('id'=>'Win_guestlisttime','label'=>__('Guestlist close time'),'timeFormat'=>Configure::read('evento_settings.timeFormat'),
							'separator' => "",
							));
	}
	else {
		echo $this->Form->input('Event.win_guestlisttime', array('id'=>'Win_guestlisttime', 'label'=>__('Guestlist close time')));
	}
	if(isset($this->request->data['Event']['win_start_date'])) {
		echo $this->Form->input('Event.win_start_date', array('type'=>'text', 'class'=>'datepicker', 'id'=>'Win_start_date', 'label'=>__('Start date'), 
			'value' => str_replace(' 00:00:00', '', $this->request->data['Event']['win_start_date'])));
	}
	else {
		echo $this->Form->input('Event.win_start_date', array('type'=>'text', 'class'=>'datepicker', 'id'=>'Win_start_date', 'label'=>__('Start date')));
	}
	
	if(isset($this->request->data['Event']['win_end_date'])) {
		echo $this->Form->input('Event.win_end_date', array('type'=>'text', 'class'=>'datepicker', 'id'=>'Win_end_date', 'label'=>__('End date'), 
			'value' => str_replace(' 00:00:00', '', $this->request->data['Event']['win_end_date'])));
	} 
	else {
		echo $this->Form->input('Event.win_end_date', array('type'=>'text', 'class'=>'datepicker', 'id'=>'Win_end_date', 'label'=>__('End date')));
	}
	
	if(isset($this->request->data['Event']['win_number_winners'])) {
		echo $this->Form->input('Event.win_number_winners', array('type'=>'text', 'id'=>'Win_number_winners', 'label'=>__('Number of winners'), 
			'value' => $this->request->data['Event']['win_number_winners']));
	}
	else {
		echo $this->Form->input('Event.win_number_winners', array('type'=>'text', 'id'=>'Win_number_winners', 'label'=>__('Number of winners')));
	}

	if(isset($this->request->data['Event']['win_chance'])) {
		echo $this->Form->input('Event.win_chance', array('type'=>'text', 'id'=>'Win_chance', 'label'=>__('Win chance'), 
			'value' => $this->request->data['Event']['win_chance']));
	}
	else {
		echo $this->Form->input('Event.win_chance', array('type'=>'text', 'id'=>'Win_chance', 'label'=>__('Win chance')));
	}
	echo "</div>";
  echo $this->Form->input('published', array('type'=>'checkbox', 'label'=>__('Event is published')));
}

if(isset($useRecaptcha) && $useRecaptcha) {
	echo $this->Recaptcha->display();
	echo $this->Form->error('recaptcha');
}
?>

<?php if($this->Session->read('Auth.User.admin') && isset($this->request->params['admin'])) : ?>

<div>
	<?php
	echo "Participants: ";
	echo sizeof($participants);
echo "<br>";
echo "Winners: ";
echo sizeof($partWon);
echo "<br><br>";
?>
	<table class="table" border="1">
		<thead>
			<tr>
				<th>Photo</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Birthdate</th>
				<th>Gender</th>
				<th>Participation</th>
				<th>E-mail</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($participants)) : ?>
			<?php foreach($participants as $p) : ?>
			<tr>
				<td><?php echo $this->Html->image('users/small/'.$p['User']['photo']
				, array('alt'=>h($user['User']['username']))) . ' ';?></td>
				<td><?php echo $p['User']['first_name']; ?></td>
				<td><?php echo $p['User']['last_name']; ?></td>
				<td><?php echo date('Y-m-d', strtotime($p['User']['birthdate'])); ?></td>
				<td align="center"><?php echo ($p['User']['gender'] == '1') ? 'F' : 'M';?></td>
				<td><?php echo $p['Participant']['part_date']; ?></td>
				<td><?php echo $p['User']['email']; ?></td>
				<td align="center"><?php echo $p['Participant']['status']; ?></td>
			</tr>
			<?php endforeach; ?>
			<?php else : ?>
			<tr>
				<td colspan="7">No data available</td>
			</tr>
			<?php endif; ?>
		</tbody>
	</table>
</div>
<?php endif; ?>

<div id="submit" class="submit">
<?php
echo $this->Form->submit(__('Save'), array('div'=>false, 'id'=>'submit-button'));
if($this->request->params['action']=='edit') {
	$link = array('controller'=>'events', 'action'=>'view',
		$event['Venue']['City']['Country']['slug'], $event['Venue']['City']['slug'],
		$event['Venue']['slug'], $event['Event']['slug']);
}
else {
	$link = array('controller'=>'events', 'page'=>$page,
  	'action'=>($this->Session->read('Search.term'))? 'search':'index');
}
echo $this->Html->link(__('Cancel'), $link, array('class'=>'back-button'));
echo "</p>";
?>
<div class="clear"></div>
</div>
<?php
echo $this->Form->end();
?>

