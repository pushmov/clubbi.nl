<?php
/*
 If events are being filtered by city or tag show the info-line and allow the user to clear
 individual filters.
*/

if(
	$country!='all-countries'
	|| $city!='all-cities'
	|| $category!='all-categories'
	|| $current_tag!='all-tags'
	|| $year || $month || $day
	) {
		echo '<div id="filter-block">';
	}

if($country!='all-countries') {
	echo '<div class="info-line">';
	echo '<span>'.sprintf(__('Events in %s'),h(__d('countries', $country_name))).'</span>';
	echo $this->Html->link('x',array('action'=>'index', 'all-countries', 'all-cities', 'all-venues', $category,
		$current_tag, $year, $month, $day));
	echo '<div class="clear"></div>';
	echo '</div>';
}

if($city!='all-cities') {
	echo '<div class="info-line">';
	echo '<span>'.sprintf(__('Events in %s'), h($city_name)).'</span>';
	echo $this->Html->link('x',array('action'=>'index', $country,
		'all-cities', 'all-venues', $category, $current_tag, $year, $month, $day));
	echo '<div class="clear"></div>';
	echo '</div>';
}

if($venue!='all-venues') {
	echo '<div class="info-line">';
	echo '<span>'.sprintf(__('Events at %s'), h($venue_name)).'</span>';
	echo $this->Html->link('x',array('action'=>'index', $country,
		$city, 'all-venues', $category, $current_tag, $year, $month, $day));
	echo '<div class="clear"></div>';
	echo '</div>';
}

if($category!='all-categories') {
	echo "<div class=\"info-line\">";
	echo '<span>'.sprintf(__("Events in category %s"),h($category_name)).'</span>';
	echo $this->Html->link('x', array('action'=>'index', $country, $city, $venue, 'all-categories',
		$current_tag, $year, $month, $day));
	echo '<div class="clear"></div>';
	echo '</div>';
}

if($current_tag!='all-tags') {
	echo '<div class="info-line">';
	echo '<span>'.sprintf(__("Events tagged %s"), h($tag_name)).'</span>';
	echo $this->Html->link('x',array('action'=>'index', $country, $city, $venue, $category, 'all-tags', $year,
		$month, $day));
	echo '<div class="clear"></div>';
	echo '</div>';
}

if($year or $month or $day) {
	$str = '';
	if($month) {
		$month_num = array_search($month, $this->Calendar->month_list)+1;
		$month = __(ucfirst($month));
		if(!$day && ($month_num == date('n') && $year == date('Y'))) $str = __('Events this month');
		if($day) {
			if($day == 'week') $str = __('Events this week');
			elseif($day == date('d', time()) && $month_num == date('n') && $year == date('Y')) {
				$str = __('Events today');
			}
			elseif($day == date('d', strtotime('+1 day')) && $month_num == date('n') && $year == date('Y')) {
				$str = __('Events tomorrow');
			}
		}
	}

	if($str=='') {
		if($day) {
			$str = sprintf(__('Events on %1$s %2$s %3$s'), __d('cake', $month), $day, $year);
		} else {
			$str = sprintf(__('Events in %1$s %2$s'), __d('cake', $month), $year);
		}
		
	}

	echo '<div class="info-line">';
	echo '<span>'.$str.'</span>';
	echo $this->Html->link('x',array('action'=>'index', $country, $city, $venue, $category, 'all-tags'));
	echo '<div class="clear"></div>';
	echo '</div>';
}

if(
	$country!='all-countries'
	|| $city!='all-cities'
	|| $category!='all-categories'
	|| $current_tag!='all-tags'
	|| $year || $month || $day
	) {
		echo '</div>';
	}
?>